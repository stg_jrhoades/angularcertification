# Basic JS & TS

## Closure
`Closures` are functions defined within another function or bundled together if you will.
    
    function A(v: string) {
        function B() {
            // Does things to params
        }
        
        // Eventually calls B() to change data consistently
    }


## Arrow Functions
    Filter: create a new array based on values that return true

        const newArray = array.filter((item) => { item.value > 10 });

        newArray would contain all items that have property of value that is above 10

    Map: creates a new arry based on changes done during the callback function
        const newArray = array.map((item) => { 
            item.value = item.value + value2;
            return item;
        })

    Reduce: runs a function to reduce the array to a single output value

        const result = array.reduce((result, currentValue) => { result * currentValue });

        result would be all the elements in the array times together
        ex: 6 = [1, 2, 3].reduce((result, currentValue) => { result * currentValue });

        Other trick would be building a sentence
        ex: "Hello World" = ["Hello", "world"].reduce((sentence, currentWord) => { `${sentence} ${currentWord}`});


## Destructuring
This is the feature in which you take an object and deconstruct into it's part
    
    const { name, birthday, gender } = personObject;

## Spread Operator
This is a feature that lets you empty or spread your array within another array or on it's own. This is primarily used to update values on a unmutable variable design pattern
    
    const newArray = [...oldArray, newValue];

## Promises
`Promises` let you run code asynchronous which helps when doing time-consuming (database calls, api calls, intense cpu heavy tasks, etc.) tasks as Javascript is a single threaded language. There are 2 methods to handle Promises. Aysnc & Await or Then, Catch, Finally

    Async & Await
        async function somethingTimeConsuming() {
            return results;
        }

        const result = await somethingTimeConsuming();

    Callback functions
        promiseFunction
            .then((resp) => { return resp }) // This runs on a success or resolve
            .catch((error) => { handleError(error) }) // This run if an error is thrown
            .finally(() => { // This runs after either then or catch })

## Blocked scope variable `Let`

`let` keyword scopes variables into the function where they are specified vs the `var` keyword which declares the variable globally

    let a = "Hello World"

    if (something true) {
        let a = 10;

        console.log(a) // return 10
    }

    console.log(a) // return Hello World



# Angular Specific


## Validation
There is plenty ways to validate fields in Angular some good libraries or just applying good logic during the submission process.  Due to there no validation outside the type of data it is required I did not put time into any specific validation on this project.

        
    
