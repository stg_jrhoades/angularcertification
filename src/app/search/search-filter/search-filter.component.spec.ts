import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchFilterComponent } from './search-filter.component';
import {RouterTestingModule} from "@angular/router/testing";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SearchService} from "../search.service";
import {ActivatedRoute, Router} from "@angular/router";
import {of} from "rxjs";

class MockSearchService {}

// Tests with no query params
describe('SearchFilterComponent', () => {
  let component: SearchFilterComponent;
  let fixture: ComponentFixture<SearchFilterComponent>;
  let router: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchFilterComponent ],
      imports: [
        RouterTestingModule.withRoutes([]),
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [
        { provide: SearchService, useClass: MockSearchService }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFilterComponent);
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should have initialized values", () => {
    expect(component.searchQuery.value).toEqual("")
    expect(component.year.value).toEqual(0)
  });

  it("should update the url with new values", () => {
    const spy = spyOn(router, "navigate").and.stub();
    component.searchQuery.setValue("movie");
    component.year.setValue(2001);

    component.setParams();
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy.calls.allArgs()[0][1].queryParams).toEqual({query: "movie", year: 2001});
  });
});

// Test with query params
describe('SearchFilterComponent - with query params', () => {
  let component: SearchFilterComponent;
  let fixture: ComponentFixture<SearchFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchFilterComponent ],
      imports: [
        RouterTestingModule.withRoutes([]),
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [
        { provide: SearchService, useClass: MockSearchService },
        { provide: ActivatedRoute, useValue: { queryParams: of({  query: "test", year: 2000 }) }}
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should have initialized values", () => {
    expect(component.searchQuery.value).toEqual("test")
    expect(component.year.value).toEqual(2000)
  });
});
