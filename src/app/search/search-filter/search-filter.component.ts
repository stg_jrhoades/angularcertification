import { Component, OnInit } from '@angular/core';
import {FormControl} from "@angular/forms";
import {SearchService} from "../search.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.css']
})
export class SearchFilterComponent implements OnInit {
  constructor(private searchService: SearchService, private route: ActivatedRoute, private router: Router) { }

  // Form data
  searchQuery = new FormControl("");
  year = new FormControl(0);
  setParams = () => {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { query: this.searchQuery.value, year: this.year.value },
      queryParamsHandling: "merge"
    })
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(route => {
      if (route.query) this.searchQuery.setValue(route.query)
      if (route.year) this.year.setValue(route.year)
    });
  }

}
