import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchResultsComponent } from './search-results.component';
import {RouterTestingModule} from "@angular/router/testing";
import {SearchService} from "../search.service";
import {Observable, of} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";

const testPage = 1;
const testParams = { query: "test", year: 2000 };
const testData = [
  {
    "adult": false,
    "backdrop_path": "/sXfQGa7juaTpZ5IF17Xa6hI53nx.jpg",
    "belongs_to_collection": {
      "id": 19163,
      "name": "The Land Before Time Collection",
      "poster_path": "/n1bjdBVThBezxR6nEf2dy43sTtV.jpg",
      "backdrop_path": "/alkvR9vTtuZEmd5ygsayOfxYOMa.jpg"
    },
    "budget": 12500000,
    "genres": [
      {
        "id": 10751,
        "name": "Family"
      },
      {
        "id": 16,
        "name": "Animation"
      },
      {
        "id": 12,
        "name": "Adventure"
      }
    ],
    "homepage": "https://www.uphe.com/movies/the-land-before-time",
    "id": 12144,
    "imdb_id": "tt0095489",
    "original_language": "en",
    "original_title": "The Land Before Time",
    "overview": "An orphaned brontosaurus named Littlefoot sets off in search of the legendary Great Valley. A land of lush vegetation where the dinosaurs can thrive and live in peace. Along the way he meets four other young dinosaurs, each one a different species, and they encounter several obstacles as they learn to work together in order to survive.",
    "popularity": 13.547,
    "poster_path": "/wSC2wxFj3ZgrIHlIaePeuHN4igo.jpg",
    "production_companies": [
      {
        "id": 6422,
        "logo_path": null,
        "name": "Sullivan Bluth Studios",
        "origin_country": "US"
      },
      {
        "id": 108270,
        "logo_path": "/eDWGeg5tdVNcI8aBaaJ21bEBtFa.png",
        "name": "Lucasfilm Animation",
        "origin_country": "US"
      },
      {
        "id": 33,
        "logo_path": "/8lvHyhjr8oUKOOy2dKXoALWKdp0.png",
        "name": "Universal Pictures",
        "origin_country": "US"
      },
      {
        "id": 20448,
        "logo_path": null,
        "name": "U-Drive Productions",
        "origin_country": "US"
      },
      {
        "id": 56,
        "logo_path": "/cEaxANEisCqeEoRvODv2dO1I0iI.png",
        "name": "Amblin Entertainment",
        "origin_country": "US"
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "IE",
        "name": "Ireland"
      },
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "1988-11-18",
    "revenue": 84460846,
    "runtime": 69,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      }
    ],
    "status": "Released",
    "tagline": "A new adventure is born.",
    "title": "The Land Before Time",
    "video": false,
    "vote_average": 7.1,
    "vote_count": 1988
  },
  {
    "adult": false,
    "backdrop_path": "/8pJZhr2wZnaMUkISc3agk9rjrKv.jpg",
    "belongs_to_collection": {
      "id": 19163,
      "name": "The Land Before Time Collection",
      "poster_path": "/n1bjdBVThBezxR6nEf2dy43sTtV.jpg",
      "backdrop_path": "/alkvR9vTtuZEmd5ygsayOfxYOMa.jpg"
    },
    "budget": 0,
    "genres": [
      {
        "id": 16,
        "name": "Animation"
      },
      {
        "id": 12,
        "name": "Adventure"
      },
      {
        "id": 10751,
        "name": "Family"
      }
    ],
    "homepage": "",
    "id": 19004,
    "imdb_id": "tt0113596",
    "original_language": "en",
    "original_title": "The Land Before Time III: The Time of the Great Giving",
    "overview": "Littlefoot and his young friends Cera, Ducky and Spike are being harassed by a group of older dinosaurs led by Hyp. When a meteorite strikes nearby, it causes a drought. The resulting scarcity of resources creates animosity among the different species in the Great Valley. Littlefoot and his crew resolve to venture forth to find a solution - as does Hyp and his gang. Will the groups learn to cooperate, or will their rivalry destroy them?",
    "popularity": 12.238,
    "poster_path": "/abcJUBDHi6hxibSgm2bxYK1wAZK.jpg",
    "production_companies": [
      {
        "id": 4285,
        "logo_path": null,
        "name": "Universal Cartoon Studios",
        "origin_country": ""
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "1995-03-16",
    "revenue": 0,
    "runtime": 71,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      }
    ],
    "status": "Released",
    "tagline": "",
    "title": "The Land Before Time III: The Time of the Great Giving",
    "video": false,
    "vote_average": 6.2,
    "vote_count": 301
  },
  {
    "adult": false,
    "backdrop_path": "/jWEYrAfXIPjmprktyKSvrkXVW8.jpg",
    "belongs_to_collection": {
      "id": 19163,
      "name": "The Land Before Time Collection",
      "poster_path": "/n1bjdBVThBezxR6nEf2dy43sTtV.jpg",
      "backdrop_path": "/alkvR9vTtuZEmd5ygsayOfxYOMa.jpg"
    },
    "budget": 0,
    "genres": [
      {
        "id": 10751,
        "name": "Family"
      },
      {
        "id": 16,
        "name": "Animation"
      },
      {
        "id": 12,
        "name": "Adventure"
      }
    ],
    "homepage": "",
    "id": 19164,
    "imdb_id": "tt0116817",
    "original_language": "en",
    "original_title": "The Land Before Time IV: Journey Through the Mists",
    "overview": "Littlefoot and his dinosaur pals go in search of a medicinal flower that will cure his ailing grandfather.",
    "popularity": 12.985,
    "poster_path": "/sqBhPAvSwPCZq4Kg79CvxGrU7nj.jpg",
    "production_companies": [
      {
        "id": 4285,
        "logo_path": null,
        "name": "Universal Cartoon Studios",
        "origin_country": ""
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "1996-11-06",
    "revenue": 0,
    "runtime": 74,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      }
    ],
    "status": "Released",
    "tagline": "",
    "title": "The Land Before Time IV: Journey Through the Mists",
    "video": false,
    "vote_average": 6.2,
    "vote_count": 267
  },
  {
    "adult": false,
    "backdrop_path": "/7RyHsO4yDXtBv1zUU3mTpHeQ0d5.jpg",
    "belongs_to_collection": {
      "id": 86311,
      "name": "The Avengers Collection",
      "poster_path": "/5k6WG11oWrXipYD8BAkmMJU4Men.jpg",
      "backdrop_path": "/zuW6fOiusv4X9nnW3paHGfXcSll.jpg"
    },
    "budget": 356000000,
    "genres": [
      {
        "id": 12,
        "name": "Adventure"
      },
      {
        "id": 878,
        "name": "Science Fiction"
      },
      {
        "id": 28,
        "name": "Action"
      }
    ],
    "homepage": "https://www.marvel.com/movies/avengers-endgame",
    "id": 299534,
    "imdb_id": "tt4154796",
    "original_language": "en",
    "original_title": "Avengers: Endgame",
    "overview": "After the devastating events of Avengers: Infinity War, the universe is in ruins due to the efforts of the Mad Titan, Thanos. With the help of remaining allies, the Avengers must assemble once more in order to undo Thanos' actions and restore order to the universe once and for all, no matter what consequences may be in store.",
    "popularity": 214.037,
    "poster_path": "/or06FN3Dka5tukK1e9sl16pB3iy.jpg",
    "production_companies": [
      {
        "id": 420,
        "logo_path": "/hUzeosd33nzE5MCNsZxCGEKTXaQ.png",
        "name": "Marvel Studios",
        "origin_country": "US"
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "2019-04-24",
    "revenue": 2797800564,
    "runtime": 181,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      },
      {
        "english_name": "Japanese",
        "iso_639_1": "ja",
        "name": "日本語"
      },
      {
        "english_name": "Xhosa",
        "iso_639_1": "xh",
        "name": ""
      }
    ],
    "status": "Released",
    "tagline": "Part of the journey is the end.",
    "title": "Avengers: Endgame",
    "video": false,
    "vote_average": 8.3,
    "vote_count": 18845
  },
  {
    "adult": false,
    "backdrop_path": "/nNmJRkg8wWnRmzQDe2FwKbPIsJV.jpg",
    "belongs_to_collection": {
      "id": 86311,
      "name": "The Avengers Collection",
      "poster_path": "/5k6WG11oWrXipYD8BAkmMJU4Men.jpg",
      "backdrop_path": "/zuW6fOiusv4X9nnW3paHGfXcSll.jpg"
    },
    "budget": 220000000,
    "genres": [
      {
        "id": 878,
        "name": "Science Fiction"
      },
      {
        "id": 28,
        "name": "Action"
      },
      {
        "id": 12,
        "name": "Adventure"
      }
    ],
    "homepage": "http://marvel.com/avengers_movie/",
    "id": 24428,
    "imdb_id": "tt0848228",
    "original_language": "en",
    "original_title": "The Avengers",
    "overview": "When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!",
    "popularity": 132.046,
    "poster_path": "/RYMX2wcKCBAr24UyPD7xwmjaTn.jpg",
    "production_companies": [
      {
        "id": 420,
        "logo_path": "/hUzeosd33nzE5MCNsZxCGEKTXaQ.png",
        "name": "Marvel Studios",
        "origin_country": "US"
      },
      {
        "id": 4,
        "logo_path": "/fycMZt242LVjagMByZOLUGbCvv3.png",
        "name": "Paramount",
        "origin_country": "US"
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "2012-04-25",
    "revenue": 1518815515,
    "runtime": 143,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      },
      {
        "english_name": "Hindi",
        "iso_639_1": "hi",
        "name": "हिन्दी"
      },
      {
        "english_name": "Russian",
        "iso_639_1": "ru",
        "name": "Pусский"
      }
    ],
    "status": "Released",
    "tagline": "Some assembly required.",
    "title": "The Avengers",
    "video": false,
    "vote_average": 7.7,
    "vote_count": 25214
  },
  {
    "adult": false,
    "backdrop_path": "/lmZFxXgJE3vgrciwuDib0N8CfQo.jpg",
    "belongs_to_collection": {
      "id": 86311,
      "name": "The Avengers Collection",
      "poster_path": "/5k6WG11oWrXipYD8BAkmMJU4Men.jpg",
      "backdrop_path": "/zuW6fOiusv4X9nnW3paHGfXcSll.jpg"
    },
    "budget": 300000000,
    "genres": [
      {
        "id": 12,
        "name": "Adventure"
      },
      {
        "id": 28,
        "name": "Action"
      },
      {
        "id": 878,
        "name": "Science Fiction"
      }
    ],
    "homepage": "https://www.marvel.com/movies/avengers-infinity-war",
    "id": 299536,
    "imdb_id": "tt4154756",
    "original_language": "en",
    "original_title": "Avengers: Infinity War",
    "overview": "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.",
    "popularity": 250.329,
    "poster_path": "/7WsyChQLEftFiDOVTGkv3hFpyyt.jpg",
    "production_companies": [
      {
        "id": 420,
        "logo_path": "/hUzeosd33nzE5MCNsZxCGEKTXaQ.png",
        "name": "Marvel Studios",
        "origin_country": "US"
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "2018-04-25",
    "revenue": 2046239637,
    "runtime": 149,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      },
      {
        "english_name": "Xhosa",
        "iso_639_1": "xh",
        "name": ""
      }
    ],
    "status": "Released",
    "tagline": "An entire universe. Once and for all.",
    "title": "Avengers: Infinity War",
    "video": false,
    "vote_average": 8.3,
    "vote_count": 22465
  },
  {
    "adult": false,
    "backdrop_path": "/zqkmTXzjkAgXmEWLRsY4UpTWCeo.jpg",
    "belongs_to_collection": {
      "id": 10,
      "name": "Star Wars Collection",
      "poster_path": "/tdQzRSk4PXX6hzjLcQWHafYtZTI.jpg",
      "backdrop_path": "/d8duYyyC9J5T825Hg7grmaabfxQ.jpg"
    },
    "budget": 11000000,
    "genres": [
      {
        "id": 12,
        "name": "Adventure"
      },
      {
        "id": 28,
        "name": "Action"
      },
      {
        "id": 878,
        "name": "Science Fiction"
      }
    ],
    "homepage": "http://www.starwars.com/films/star-wars-episode-iv-a-new-hope",
    "id": 11,
    "imdb_id": "tt0076759",
    "original_language": "en",
    "original_title": "Star Wars",
    "overview": "Princess Leia is captured and held hostage by the evil Imperial forces in their effort to take over the galactic Empire. Venturesome Luke Skywalker and dashing captain Han Solo team together with the loveable robot duo R2-D2 and C-3PO to rescue the beautiful princess and restore peace and justice in the Empire.",
    "popularity": 57.799,
    "poster_path": "/6FfCtAuVAW8XJjZ7eWeLibRLWTw.jpg",
    "production_companies": [
      {
        "id": 1,
        "logo_path": "/o86DbpburjxrqAzEDhXZcyE8pDb.png",
        "name": "Lucasfilm Ltd.",
        "origin_country": "US"
      },
      {
        "id": 25,
        "logo_path": "/qZCc1lty5FzX30aOCVRBLzaVmcp.png",
        "name": "20th Century Fox",
        "origin_country": "US"
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "1977-05-25",
    "revenue": 775398007,
    "runtime": 121,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      }
    ],
    "status": "Released",
    "tagline": "A long time ago in a galaxy far, far away...",
    "title": "Star Wars",
    "video": false,
    "vote_average": 8.2,
    "vote_count": 15885
  },
  {
    "adult": false,
    "backdrop_path": "/jn52me8AagfNt7r84SgQbV0R9ZG.jpg",
    "belongs_to_collection": {
      "id": 10,
      "name": "Star Wars Collection",
      "poster_path": "/tdQzRSk4PXX6hzjLcQWHafYtZTI.jpg",
      "backdrop_path": "/d8duYyyC9J5T825Hg7grmaabfxQ.jpg"
    },
    "budget": 250000000,
    "genres": [
      {
        "id": 28,
        "name": "Action"
      },
      {
        "id": 12,
        "name": "Adventure"
      },
      {
        "id": 878,
        "name": "Science Fiction"
      }
    ],
    "homepage": "https://www.starwars.com/films/star-wars-episode-ix-the-rise-of-skywalker",
    "id": 181812,
    "imdb_id": "tt2527338",
    "original_language": "en",
    "original_title": "Star Wars: The Rise of Skywalker",
    "overview": "The surviving Resistance faces the First Order once again as the journey of Rey, Finn and Poe Dameron continues. With the power and knowledge of generations behind them, the final battle begins.",
    "popularity": 128.547,
    "poster_path": "/db32LaOibwEliAmSL2jjDF6oDdj.jpg",
    "production_companies": [
      {
        "id": 1,
        "logo_path": "/o86DbpburjxrqAzEDhXZcyE8pDb.png",
        "name": "Lucasfilm Ltd.",
        "origin_country": "US"
      },
      {
        "id": 11461,
        "logo_path": "/p9FoEt5shEKRWRKVIlvFaEmRnun.png",
        "name": "Bad Robot",
        "origin_country": "US"
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "2019-12-18",
    "revenue": 1074144248,
    "runtime": 142,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      }
    ],
    "status": "Released",
    "tagline": "Every generation has a legend",
    "title": "Star Wars: The Rise of Skywalker",
    "video": false,
    "vote_average": 6.5,
    "vote_count": 6973
  }
];
const testPaginator = [testData[0]];
let testIsSuccess = false;

class MockSearchService {
  search = (query: string, year: number) => {
    expect(query).toEqual(testParams.query);
    expect(year).toEqual(testParams.year);
    if (testIsSuccess) {
      return new Observable(observer => {
        observer.next({
          page: 1,
          result: testData,
          total_pages: 1,
          total_results: 20
        });
        observer.complete();
      })
    }

    return new Observable(observer => {
      observer.next({
        page: 0,
        result: [],
        total_pages: 0,
        total_results: 0
      });
      observer.complete();
    })
  }
  paginator = (index: number, query: string, year: number) => {
    expect(index).toEqual(2);
    expect(query).toEqual(testParams.query);
    expect(year).toEqual(testParams.year);
    return new Observable(observer => {
      observer.next({
        page: 2,
        result: testPaginator,
        total_pages: 2,
        total_results: 40
      });
      observer.complete();
    })
  }
}

describe('SearchResultsComponent', () => {
  let component: SearchResultsComponent;
  let fixture: ComponentFixture<SearchResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchResultsComponent ],
      imports: [ RouterTestingModule.withRoutes([]) ],
      providers: [
        { provide: SearchService, useClass: MockSearchService }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should set hasData to false", () => {
    expect(component.hasData).toBeFalsy();
  });

  it("should NOT set provided data to datasource", () => {
    const testDataSource = new MatTableDataSource();
    expect(component.datasource.data).toEqual(testDataSource.data);
  });

  it("should NOT set message", () => {
    expect(component.message).toEqual("");
  });
});

describe('SearchResultsComponent - with query params', () => {
  let component: SearchResultsComponent;
  let fixture: ComponentFixture<SearchResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchResultsComponent ],
      imports: [ RouterTestingModule.withRoutes([]) ],
      providers: [
        { provide: SearchService, useClass: MockSearchService },
        { provide: ActivatedRoute, useValue: { queryParams: of({  query: testParams.query, year: testParams.year }) }}
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    testIsSuccess = false; // This is to manage the return of the mock service
    fixture = TestBed.createComponent(SearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should set hasData to true", () => {
    expect(component.hasData).toBeTruthy();
  });

  it("should have data returned from mock service", () => {
    testIsSuccess = true;
    component.query = testParams.query;
    component.year = testParams.year;
    component.setDatasource();

    expect(component.itemCount).toEqual(20);
  });

  it("should set message to 'No movies found'", () => {
    expect(component.message).toEqual("No movies found");
  });

  it("should NOT set message", () => {
    testIsSuccess = true;
    component.query = testParams.query;
    component.year = testParams.year;
    component.setDatasource();

    expect(component.message).toEqual("");
  });

  it("should run paginator service", () => {
      component.changePage({pageSize: 20, pageIndex: 1, previousPageIndex: 0, length: 20});
      expect(component.pageIndex).toEqual(2);
  });

  it("should run navigate function", () => {
    const router = TestBed.inject(Router);
    const spy = spyOn(router, "navigate").and.stub();

    component.goToDetails(1);
    expect(spy).toHaveBeenCalledOnceWith(["/details/movie/1"]);
  });
});
