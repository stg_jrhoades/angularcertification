import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {QueryData, SearchService} from "../search.service";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit, AfterViewInit {
  constructor(public searchService: SearchService, private route: ActivatedRoute, private router: Router) { }

  loading: boolean = false
  hasData: boolean = false;
  message!: string;

  // Table Data
  displayColumns: string[] = [ "title", "release_date", "vote_average"];
  data: any = {};
  datasource = new MatTableDataSource();


  // Query data
  query!: string;
  year!: number;

  @ViewChild(MatPaginator, { read: true }) paginator!: MatPaginator;
  pageIndex!: number;
  itemCount!: number;


  setDatasource = () => {
    this.route.queryParams.subscribe(route => {
      this.query = route.query;
      this.year = route.year;
      if (route.query) {
        this.hasData = true;
        this.searchService.search(route.query, route.year).subscribe((data: QueryData) => {
          if (data.total_results === 0) {
            this.message = "No movies found";
          } else {
            this.message = '';
          }

          this.datasource.data = data.results;
          this.datasource.paginator = this.paginator;

          this.itemCount = data.total_results;
        })
      } else {
        this.hasData = false
        this.datasource = new MatTableDataSource();
        this.message = '';
      }
    })
  }

  changePage(event: PageEvent) {
    this.loading = true;
    this.pageIndex = event.pageIndex + 1;
    this.searchService.paginator(this.pageIndex, this.query, this.year).subscribe((data: QueryData) => {
      this.datasource.data = data.results;
      this.loading = false;
    })
  }

  goToDetails = (id: number) => {
    this.router.navigate([`/details/movie/${id}`]);
  }

  ngOnInit(): void {
    this.setDatasource()
  }

  ngAfterViewInit(): void {
    this.setDatasource()
  }
}
