import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {QueryData, SearchService} from "../search.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator, PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-person-search',
  templateUrl: './person-search.component.html',
  styleUrls: ['./person-search.component.css']
})
export class PersonSearchComponent implements OnInit, AfterViewInit {
  constructor(private searchService: SearchService, private route: ActivatedRoute, private router: Router) { }

  // Search functionality
  // #Forms - using the ReactiveForm FormControl you are able to create a pretty useful form variable
  searchQuery = new FormControl("");
  setParams = () => {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { query: this.searchQuery.value },
      queryParamsHandling: "merge"
    })
  }

  // Table data
  loading: boolean = false;
  hasData: boolean = false;
  message!: string;

  @ViewChild(MatPaginator, { read: true }) paginator!: MatPaginator;
  displayColumns: string[] = [ "name", "gender", "popularity"];
  datasource = new MatTableDataSource();
  pageIndex!: number;
  itemCount!: number;

  setDataSource = () => {
    this.route.queryParams.subscribe(route => {
      this.searchQuery.setValue(route.query)
      if (this.searchQuery.value) {
        this.hasData = true;
        this.searchService.personSearch(route.query).subscribe((data: QueryData) => {
          if (data.total_results === 0) {
            this.message = "No person found";
          } else {
            this.message = '';
          }

          this.datasource.data = data.results;
          this.datasource.paginator = this.paginator;

          this.itemCount = data.total_results;
        })
      } else {
        this.hasData = false
        this.datasource = new MatTableDataSource();
        this.message = '';
      }
    })
  }

  changePage(event: PageEvent) {
    this.loading = true;
    this.pageIndex = event.pageIndex + 1;
    this.searchService.personPaginator(this.pageIndex, this.searchQuery.value).subscribe((data: QueryData) => {
      this.datasource.data = data.results;
      this.loading = false;
    })
  }

  goToDetails = (id: number) => {
    this.router.navigate([`/details/person/${id}`]);
  }


  // #Lifecycle hooks ngOnInit - this function runs when the component is initialized on the system.
  ngOnInit(): void {
    this.setDataSource()
  }

  // #Lifecycle hooks ngAfterViewInit - this function runs when the component is rendered
  ngAfterViewInit() {
    this.setDataSource()
  }
}
