import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonSearchComponent } from './person-search.component';
import {RouterTestingModule} from "@angular/router/testing";
import {SearchService} from "../search.service";
import {Observable, of} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatTableDataSource} from "@angular/material/table";

const testData = [
  {
    "adult": false,
    "also_known_as": [
      "Laura Dawn Bailey",
      "Elle Deets",
      "Laura Bailey Willingham"
    ],
    "biography": "Laura Dawn Bailey is an American voice actress.",
    "birthday": "1981-05-28",
    "deathday": null,
    "gender": 1,
    "homepage": "https://www.laurabaileyvo.com",
    "id": 75037,
    "imdb_id": "nm1154161",
    "known_for_department": "Acting",
    "name": "Laura Bailey",
    "place_of_birth": "Biloxi, Mississippi, USA",
    "popularity": 8.081,
    "profile_path": "/geVQ9DnYTU66xULx4W4jmBh68w3.jpg"
  },
  {
    "adult": false,
    "also_known_as": [],
    "biography": "Marisha Ray is an American voice actress, host, and producer. She is best known for her roles as Keyleth and Beauregard from Critical Role. In video games, Ray has voiced Margaret in Persona 4 Arena Ultimax and Persona Q, Laura Arseid in The Legend of Heroes: Trails of Cold Steel series, and Miranda in Metal Gear: Survive.",
    "birthday": "1989-05-10",
    "deathday": null,
    "gender": 1,
    "homepage": "http://www.marisharay.com/",
    "id": 1485055,
    "imdb_id": null,
    "known_for_department": "Acting",
    "name": "Marisha Ray",
    "place_of_birth": "Louisville, Kentucky, USA",
    "popularity": 2.988,
    "profile_path": "/pLP67Y4iV6vQVaV1O2K7FNmvXJX.jpg"
  },
  {
    "adult": false,
    "also_known_as": [],
    "biography": "",
    "birthday": "1981-08-03",
    "deathday": null,
    "gender": 2,
    "homepage": null,
    "id": 1221888,
    "imdb_id": "nm0992184",
    "known_for_department": "Acting",
    "name": "Travis Willingham",
    "place_of_birth": " Dallas, Texas, USA",
    "popularity": 2.188,
    "profile_path": "/g94CyOOGf8zcRwjtzX2N7csC4D5.jpg"
  },
  {
    "adult": false,
    "also_known_as": [
      "Matthew Christopher Miller",
      "Matt Mercer"
    ],
    "biography": "Matthew Mercer was born on June 29, 1982 as Matthew Christopher Miller. He is an actor and producer, known for Resident Evil: Damnation (2012), Monsters University (2013) and Resident Evil 6 (2012).",
    "birthday": "1982-06-29",
    "deathday": null,
    "gender": 2,
    "homepage": null,
    "id": 556275,
    "imdb_id": "nm2233310",
    "known_for_department": "Acting",
    "name": "Matthew Mercer",
    "place_of_birth": null,
    "popularity": 3.4,
    "profile_path": "/fe1S317253vfMuNuIDjhp9vtNSs.jpg"
  },
  {
    "adult": false,
    "also_known_as": [
      "Дэйзи Ридли",
      "黛西·蕾德莉",
      "เดซี ริดลีย์",
      "デイジー・リドリー",
      "데이지 리들리",
      "ديزي ريدلي",
      "Ντέιζι Ρίντλεϊ",
      "Daisy Jazz Isobel Ridley"
    ],
    "biography": "Daisy Jazz Isobel Ridley (born 10 April 1992) is an English actress. Ridley was born in Westminster, London.\n\nHer great-uncle was actor and playwright Arnold Ridley, of Dad's Army fame. She attended Tring Park School for the Performing Arts in Hertfordshire, graduating in 2010 at the age of 18.\n\nRidley has appeared in the television programmes such as Toast of London, Youngers, Silent Witness, Mr Selfridge and Casualty. She also appeared in the short film Blue Season, which was entered into the Sci-Fi-London 48-Hour Film Challenge. She played the lead in film three of Lifesaver, an interactive film which was nominated for a BAFTA British Academy Award. She has also appeared in the music video for Wiley's song \"Lights On\".\n\nIn April 2014, it was announced that she will play Rey, one of the lead characters in Star Wars: The Force Awakens. She was chosen for the film in February 2014. The casting of Ridley has generally been seen as a deliberate move from director J. J. Abrams, as he wishes to repeat the casting of relatively unknown actors for the lead roles like George Lucas did with the first Star Wars film in 1977.\n\nAfter receiving several competing offers, Ridley switched from her original talent agency Jonathan Arun to the United Talent Agency in August 2014. Following a two-month stint with UTA, Ridley signed on with the Creative Artists Agency in October 2014.",
    "birthday": "1992-04-10",
    "deathday": null,
    "gender": 1,
    "homepage": "http://daisyridley.us/",
    "id": 1315036,
    "imdb_id": "nm5397459",
    "known_for_department": "Acting",
    "name": "Daisy Ridley",
    "place_of_birth": "Westminster, London, England, UK",
    "popularity": 13.661,
    "profile_path": "/n8kBnNOi9VmELHJy3FdZjrSN9zT.jpg"
  },
  {
    "adult": false,
    "also_known_as": [
      "كريس إيفانز",
      "크리스 에반스",
      "Крис Эванс",
      "คริส อีแวนส์",
      "克里斯·埃文斯",
      "クリス・エヴァンス",
      "Кріс Еванс",
      "Christopher Robert \"Chris\" Evans",
      "Christopher Robert Evans",
      "Κρις Έβανς",
      "Κρίστοφερ Ρόμπερτ Έβανς",
      "Cap",
      "کریس اوانز"
    ],
    "biography": "An American actor. Evans is known for his superhero roles as the Marvel Comics characters Steve Rogers in the Marvel Cinematic Universe and the Human Torch in Fantastic Four (2005) and its 2007 sequel. Evans began his career on the 2000 television series Opposite Sex. Besides his superhero films, he has appeared in such films as Not Another Teen Movie (2001), Sunshine (2007), Scott Pilgrim vs. the World (2010), Snowpiercer (2013), and Gifted (2017). In 2014, he made his directorial debut with the drama film Before We Go, in which he also starred. Evans made his Broadway debut in a 2018 production of Lobby Hero.\n\nCourtesy Wikipedia®",
    "birthday": "1981-06-13",
    "deathday": null,
    "gender": 2,
    "homepage": null,
    "id": 16828,
    "imdb_id": "nm0262635",
    "known_for_department": "Acting",
    "name": "Chris Evans",
    "place_of_birth": "Sudbury, Massachusetts, USA",
    "popularity": 13.499,
    "profile_path": "/3bOGNsHlrswhyW79uvIHH1V43JI.jpg"
  },
  {
    "adult": false,
    "also_known_as": [
      "Скарлетт Йоганссон",
      "Скарлетт Йоханссон",
      "스칼릿 조핸슨",
      "سكارليت جوهانسون",
      "史嘉蕾·喬韓森",
      "สการ์เลตต์ โจแฮนส์สัน",
      "スカーレット・ヨハンソン",
      "斯嘉丽·约翰逊",
      "스칼렛 요한슨",
      "Σκάρλετ Τζοχάνσον",
      "اسکارلت جوهانسون"
    ],
    "biography": "Scarlett Johansson, born November 22, 1984, is an American actress, model and singer. She made her film debut in North (1994) and was later nominated for the Independent Spirit Award for Best Female Lead for her performance in Manny & Lo (1996), garnering further acclaim and prominence with roles in The Horse Whisperer (1998) and Ghost World (2001). She shifted to adult roles with her performances in Girl with a Pearl Earring (2003) and Sofia Coppola's Lost in Translation (2003), for which she won a BAFTA award for Best Actress in a Leading Role; both films earned her Golden Globe Award nominations as well.\n\nA role in A Love Song for Bobby Long (2004) earned Johansson her third Golden Globe for Best Actress nomination. Johansson garnered another Golden Globe nomination for Best Supporting Actress with her role in Woody Allen's Match Point (2005). She has played the Marvel comic book character Black Widow/Natasha Romanoff in Iron Man 2 (2010), The Avengers (2012), and Captain America: The Winter Soldier (2014), Avengers: Age of Ultron (2015), Captain America: Civil War (2016), Avengers: Infinity War (2018), Avengers: Endgame (2019), and Black Widow (2020). The 2010 Broadway revival of Arthur Miller's A View From the Bridge won Johansson the Tony Award for Best Performance by a Featured Actress in a Play. As a singer, Johansson has released two albums, Anywhere I Lay My Head and Break Up.\n\nJohansson was nominated for two Academy Awards in 2020 for her work in Marriage Story (2019), and Jojo Rabbit (2019).  Johansson was born in New York City. Her father, Karsten Johansson, is a Danish-born architect, and her paternal grandfather, Ejner Johansson, was a screenwriter and director. Her mother, Melanie Sloan, a producer, comes from an Ashkenazi Jewish family from the Bronx. Johansson has an older sister, Vanessa, who is an actress; an older brother, Adrian; a twin brother, Hunter (who appeared in the film Manny & Lo with Scarlett); and a half-brother, Christian, from her father's re-marriage.",
    "birthday": "1984-11-22",
    "deathday": null,
    "gender": 1,
    "homepage": null,
    "id": 1245,
    "imdb_id": "nm0424060",
    "known_for_department": "Acting",
    "name": "Scarlett Johansson",
    "place_of_birth": "New York City, New York, USA",
    "popularity": 38.072,
    "profile_path": "/6NsMbJXRlDZuDzatN2akFdGuTvx.jpg"
  },
  {
    "adult": false,
    "also_known_as": [
      "Thomas Jacob Black",
      "Τόμας Τζέικομπ \"Τζακ\" Μπλακ",
      "Τόμας Τζέικομπ Μπλακ",
      "Τζακ Μπλακ",
      "잭 블랙",
      "Джек Блек"
    ],
    "biography": "Thomas Jacob \"Jack\" Black (born August 28, 1969) is an American actor, comedian, singer, songwriter, and YouTuber. He makes up one half of the comedy and satirical rock duo Tenacious D. The group has two albums as well as a television series and a film. His acting career is extensive, starring primarily as bumbling, cocky, but internally self-conscious outsiders in comedy films. He was a member of the Frat Pack, a group of comedians who have appeared together in several Hollywood films, and has been nominated for a Golden Globe award. He has also won an MTV Movie Award, and a Nickelodeon Kids Choice Award.\n\n​From Wikipedia, the free encyclopedia",
    "birthday": "1969-08-28",
    "deathday": null,
    "gender": 2,
    "homepage": null,
    "id": 70851,
    "imdb_id": "nm0085312",
    "known_for_department": "Acting",
    "name": "Jack Black",
    "place_of_birth": "Santa Monica, California, USA",
    "popularity": 12.933,
    "profile_path": "/rtCx0fiYxJVhzXXdwZE2XRTfIKE.jpg"
  }
];

class MockSearchService {
  personSearch = (query: string) => {
    expect(query).toEqual("test");
    return new Observable(observer => {
      observer.next({
        page: 1,
        result: testData,
        total_pages: 1,
        total_results: 20
      });
      observer.complete();
    })
  }

  personPaginator = (index: number, query: string) => {
    expect(index).toEqual(2);
    expect(query).toEqual("test");
    return new Observable(observer => {
      observer.next({
        page: 2,
        result: testData,
        total_pages: 2,
        total_results: 40
      });
      observer.complete();
    })
  }
}

describe('PersonSearchComponent', () => {
  let component: PersonSearchComponent;
  let fixture: ComponentFixture<PersonSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonSearchComponent ],
      imports: [
        RouterTestingModule.withRoutes([]),
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [{ provide: SearchService, useClass: MockSearchService }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should update the URL with new value", () => {
    const router = TestBed.inject(Router);
    const spy = spyOn(router, "navigate").and.stub();
    component.searchQuery.setValue("test");

    component.setParams();
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy.calls.allArgs()[0][1].queryParams).toEqual({query: "test" });
  });

  it("should set hasData to false", () => {
    expect(component.hasData).toBeFalsy();
  });

  it("should NOT have data in datasource", () => {
    const testDataSource = new MatTableDataSource();
    expect(component.datasource.data).toEqual(testDataSource.data)
  });

  it("should NOT set message", () => {
    expect(component.message).toEqual("");
  });
});

describe('PersonSearchComponent - with query params', () => {
  let component: PersonSearchComponent;
  let fixture: ComponentFixture<PersonSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonSearchComponent ],
      imports: [
        RouterTestingModule.withRoutes([]),
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [
        { provide: SearchService, useClass: MockSearchService },
        { provide: ActivatedRoute, useValue: { queryParams: of({  query: "test" }) }}
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should have a initialized value", () => {
    expect(component.searchQuery.value).toEqual("test");
  });

  it("should update the URL with new value", () => {
    const router = TestBed.inject(Router);
    const spy = spyOn(router, "navigate").and.stub();
    component.searchQuery.setValue("test1");

    component.setParams();
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy.calls.allArgs()[0][1].queryParams).toEqual({query: "test1" });

    fixture.detectChanges();
    expect(component.searchQuery.value).toEqual("test1");
  });
  it("should run paginator", () => {
    component.changePage({pageSize: 20, pageIndex: 1, previousPageIndex: 0, length: 20});
    expect(component.pageIndex).toEqual(2);
  });
  it("should run navigate function", () => {
    const router = TestBed.inject(Router);
    const spy = spyOn(router, "navigate").and.stub();

    component.goToDetails(1);
    expect(spy).toHaveBeenCalledOnceWith(["/details/person/1"]);
  });
});
