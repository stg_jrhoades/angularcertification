import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';
import { SearchFilterComponent } from './search-filter/search-filter.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { RouterModule, Routes } from "@angular/router";
import {MatCardModule} from "@angular/material/card";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatTableModule} from "@angular/material/table";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ApiInterceptor} from "../interceptor/api.interceptor";
import {MatPaginatorModule} from "@angular/material/paginator";
import { PersonSearchComponent } from './person-search/person-search.component';
import {GenderPipe} from "../pipes/gender.pipe";


const routes: Routes = [
  { path: "search",
    children: [
      { path: "movie", component: SearchComponent },
      { path: "person", component: PersonSearchComponent }
    ]}
]


@NgModule({
  declarations: [
    SearchComponent,
    SearchFilterComponent,
    SearchResultsComponent,
    PersonSearchComponent,
    GenderPipe
  ],
    imports: [
        CommonModule,
        RouterModule.forRoot(routes),
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        MatTableModule,
        MatPaginatorModule
    ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: ApiInterceptor,
    multi: true
  }],
  exports: [
    RouterModule
  ]
})
export class SearchModule { }
