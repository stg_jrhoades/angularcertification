import { TestBed } from '@angular/core/testing';

import { SearchService } from './search.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

const moviesData = [
  {
    "adult": false,
    "backdrop_path": "/sXfQGa7juaTpZ5IF17Xa6hI53nx.jpg",
    "belongs_to_collection": {
      "id": 19163,
      "name": "The Land Before Time Collection",
      "poster_path": "/n1bjdBVThBezxR6nEf2dy43sTtV.jpg",
      "backdrop_path": "/alkvR9vTtuZEmd5ygsayOfxYOMa.jpg"
    },
    "budget": 12500000,
    "genres": [
      {
        "id": 10751,
        "name": "Family"
      },
      {
        "id": 16,
        "name": "Animation"
      },
      {
        "id": 12,
        "name": "Adventure"
      }
    ],
    "homepage": "https://www.uphe.com/movies/the-land-before-time",
    "id": 12144,
    "imdb_id": "tt0095489",
    "original_language": "en",
    "original_title": "The Land Before Time",
    "overview": "An orphaned brontosaurus named Littlefoot sets off in search of the legendary Great Valley. A land of lush vegetation where the dinosaurs can thrive and live in peace. Along the way he meets four other young dinosaurs, each one a different species, and they encounter several obstacles as they learn to work together in order to survive.",
    "popularity": 13.547,
    "poster_path": "/wSC2wxFj3ZgrIHlIaePeuHN4igo.jpg",
    "production_companies": [
      {
        "id": 6422,
        "logo_path": null,
        "name": "Sullivan Bluth Studios",
        "origin_country": "US"
      },
      {
        "id": 108270,
        "logo_path": "/eDWGeg5tdVNcI8aBaaJ21bEBtFa.png",
        "name": "Lucasfilm Animation",
        "origin_country": "US"
      },
      {
        "id": 33,
        "logo_path": "/8lvHyhjr8oUKOOy2dKXoALWKdp0.png",
        "name": "Universal Pictures",
        "origin_country": "US"
      },
      {
        "id": 20448,
        "logo_path": null,
        "name": "U-Drive Productions",
        "origin_country": "US"
      },
      {
        "id": 56,
        "logo_path": "/cEaxANEisCqeEoRvODv2dO1I0iI.png",
        "name": "Amblin Entertainment",
        "origin_country": "US"
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "IE",
        "name": "Ireland"
      },
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "1988-11-18",
    "revenue": 84460846,
    "runtime": 69,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      }
    ],
    "status": "Released",
    "tagline": "A new adventure is born.",
    "title": "The Land Before Time",
    "video": false,
    "vote_average": 7.1,
    "vote_count": 1988
  },
  {
    "adult": false,
    "backdrop_path": "/8pJZhr2wZnaMUkISc3agk9rjrKv.jpg",
    "belongs_to_collection": {
      "id": 19163,
      "name": "The Land Before Time Collection",
      "poster_path": "/n1bjdBVThBezxR6nEf2dy43sTtV.jpg",
      "backdrop_path": "/alkvR9vTtuZEmd5ygsayOfxYOMa.jpg"
    },
    "budget": 0,
    "genres": [
      {
        "id": 16,
        "name": "Animation"
      },
      {
        "id": 12,
        "name": "Adventure"
      },
      {
        "id": 10751,
        "name": "Family"
      }
    ],
    "homepage": "",
    "id": 19004,
    "imdb_id": "tt0113596",
    "original_language": "en",
    "original_title": "The Land Before Time III: The Time of the Great Giving",
    "overview": "Littlefoot and his young friends Cera, Ducky and Spike are being harassed by a group of older dinosaurs led by Hyp. When a meteorite strikes nearby, it causes a drought. The resulting scarcity of resources creates animosity among the different species in the Great Valley. Littlefoot and his crew resolve to venture forth to find a solution - as does Hyp and his gang. Will the groups learn to cooperate, or will their rivalry destroy them?",
    "popularity": 12.238,
    "poster_path": "/abcJUBDHi6hxibSgm2bxYK1wAZK.jpg",
    "production_companies": [
      {
        "id": 4285,
        "logo_path": null,
        "name": "Universal Cartoon Studios",
        "origin_country": ""
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "1995-03-16",
    "revenue": 0,
    "runtime": 71,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      }
    ],
    "status": "Released",
    "tagline": "",
    "title": "The Land Before Time III: The Time of the Great Giving",
    "video": false,
    "vote_average": 6.2,
    "vote_count": 301
  },
  {
    "adult": false,
    "backdrop_path": "/jWEYrAfXIPjmprktyKSvrkXVW8.jpg",
    "belongs_to_collection": {
      "id": 19163,
      "name": "The Land Before Time Collection",
      "poster_path": "/n1bjdBVThBezxR6nEf2dy43sTtV.jpg",
      "backdrop_path": "/alkvR9vTtuZEmd5ygsayOfxYOMa.jpg"
    },
    "budget": 0,
    "genres": [
      {
        "id": 10751,
        "name": "Family"
      },
      {
        "id": 16,
        "name": "Animation"
      },
      {
        "id": 12,
        "name": "Adventure"
      }
    ],
    "homepage": "",
    "id": 19164,
    "imdb_id": "tt0116817",
    "original_language": "en",
    "original_title": "The Land Before Time IV: Journey Through the Mists",
    "overview": "Littlefoot and his dinosaur pals go in search of a medicinal flower that will cure his ailing grandfather.",
    "popularity": 12.985,
    "poster_path": "/sqBhPAvSwPCZq4Kg79CvxGrU7nj.jpg",
    "production_companies": [
      {
        "id": 4285,
        "logo_path": null,
        "name": "Universal Cartoon Studios",
        "origin_country": ""
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "1996-11-06",
    "revenue": 0,
    "runtime": 74,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      }
    ],
    "status": "Released",
    "tagline": "",
    "title": "The Land Before Time IV: Journey Through the Mists",
    "video": false,
    "vote_average": 6.2,
    "vote_count": 267
  },
  {
    "adult": false,
    "backdrop_path": "/7RyHsO4yDXtBv1zUU3mTpHeQ0d5.jpg",
    "belongs_to_collection": {
      "id": 86311,
      "name": "The Avengers Collection",
      "poster_path": "/5k6WG11oWrXipYD8BAkmMJU4Men.jpg",
      "backdrop_path": "/zuW6fOiusv4X9nnW3paHGfXcSll.jpg"
    },
    "budget": 356000000,
    "genres": [
      {
        "id": 12,
        "name": "Adventure"
      },
      {
        "id": 878,
        "name": "Science Fiction"
      },
      {
        "id": 28,
        "name": "Action"
      }
    ],
    "homepage": "https://www.marvel.com/movies/avengers-endgame",
    "id": 299534,
    "imdb_id": "tt4154796",
    "original_language": "en",
    "original_title": "Avengers: Endgame",
    "overview": "After the devastating events of Avengers: Infinity War, the universe is in ruins due to the efforts of the Mad Titan, Thanos. With the help of remaining allies, the Avengers must assemble once more in order to undo Thanos' actions and restore order to the universe once and for all, no matter what consequences may be in store.",
    "popularity": 214.037,
    "poster_path": "/or06FN3Dka5tukK1e9sl16pB3iy.jpg",
    "production_companies": [
      {
        "id": 420,
        "logo_path": "/hUzeosd33nzE5MCNsZxCGEKTXaQ.png",
        "name": "Marvel Studios",
        "origin_country": "US"
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "2019-04-24",
    "revenue": 2797800564,
    "runtime": 181,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      },
      {
        "english_name": "Japanese",
        "iso_639_1": "ja",
        "name": "日本語"
      },
      {
        "english_name": "Xhosa",
        "iso_639_1": "xh",
        "name": ""
      }
    ],
    "status": "Released",
    "tagline": "Part of the journey is the end.",
    "title": "Avengers: Endgame",
    "video": false,
    "vote_average": 8.3,
    "vote_count": 18845
  },
  {
    "adult": false,
    "backdrop_path": "/nNmJRkg8wWnRmzQDe2FwKbPIsJV.jpg",
    "belongs_to_collection": {
      "id": 86311,
      "name": "The Avengers Collection",
      "poster_path": "/5k6WG11oWrXipYD8BAkmMJU4Men.jpg",
      "backdrop_path": "/zuW6fOiusv4X9nnW3paHGfXcSll.jpg"
    },
    "budget": 220000000,
    "genres": [
      {
        "id": 878,
        "name": "Science Fiction"
      },
      {
        "id": 28,
        "name": "Action"
      },
      {
        "id": 12,
        "name": "Adventure"
      }
    ],
    "homepage": "http://marvel.com/avengers_movie/",
    "id": 24428,
    "imdb_id": "tt0848228",
    "original_language": "en",
    "original_title": "The Avengers",
    "overview": "When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!",
    "popularity": 132.046,
    "poster_path": "/RYMX2wcKCBAr24UyPD7xwmjaTn.jpg",
    "production_companies": [
      {
        "id": 420,
        "logo_path": "/hUzeosd33nzE5MCNsZxCGEKTXaQ.png",
        "name": "Marvel Studios",
        "origin_country": "US"
      },
      {
        "id": 4,
        "logo_path": "/fycMZt242LVjagMByZOLUGbCvv3.png",
        "name": "Paramount",
        "origin_country": "US"
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "2012-04-25",
    "revenue": 1518815515,
    "runtime": 143,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      },
      {
        "english_name": "Hindi",
        "iso_639_1": "hi",
        "name": "हिन्दी"
      },
      {
        "english_name": "Russian",
        "iso_639_1": "ru",
        "name": "Pусский"
      }
    ],
    "status": "Released",
    "tagline": "Some assembly required.",
    "title": "The Avengers",
    "video": false,
    "vote_average": 7.7,
    "vote_count": 25214
  },
  {
    "adult": false,
    "backdrop_path": "/lmZFxXgJE3vgrciwuDib0N8CfQo.jpg",
    "belongs_to_collection": {
      "id": 86311,
      "name": "The Avengers Collection",
      "poster_path": "/5k6WG11oWrXipYD8BAkmMJU4Men.jpg",
      "backdrop_path": "/zuW6fOiusv4X9nnW3paHGfXcSll.jpg"
    },
    "budget": 300000000,
    "genres": [
      {
        "id": 12,
        "name": "Adventure"
      },
      {
        "id": 28,
        "name": "Action"
      },
      {
        "id": 878,
        "name": "Science Fiction"
      }
    ],
    "homepage": "https://www.marvel.com/movies/avengers-infinity-war",
    "id": 299536,
    "imdb_id": "tt4154756",
    "original_language": "en",
    "original_title": "Avengers: Infinity War",
    "overview": "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.",
    "popularity": 250.329,
    "poster_path": "/7WsyChQLEftFiDOVTGkv3hFpyyt.jpg",
    "production_companies": [
      {
        "id": 420,
        "logo_path": "/hUzeosd33nzE5MCNsZxCGEKTXaQ.png",
        "name": "Marvel Studios",
        "origin_country": "US"
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "2018-04-25",
    "revenue": 2046239637,
    "runtime": 149,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      },
      {
        "english_name": "Xhosa",
        "iso_639_1": "xh",
        "name": ""
      }
    ],
    "status": "Released",
    "tagline": "An entire universe. Once and for all.",
    "title": "Avengers: Infinity War",
    "video": false,
    "vote_average": 8.3,
    "vote_count": 22465
  },
  {
    "adult": false,
    "backdrop_path": "/zqkmTXzjkAgXmEWLRsY4UpTWCeo.jpg",
    "belongs_to_collection": {
      "id": 10,
      "name": "Star Wars Collection",
      "poster_path": "/tdQzRSk4PXX6hzjLcQWHafYtZTI.jpg",
      "backdrop_path": "/d8duYyyC9J5T825Hg7grmaabfxQ.jpg"
    },
    "budget": 11000000,
    "genres": [
      {
        "id": 12,
        "name": "Adventure"
      },
      {
        "id": 28,
        "name": "Action"
      },
      {
        "id": 878,
        "name": "Science Fiction"
      }
    ],
    "homepage": "http://www.starwars.com/films/star-wars-episode-iv-a-new-hope",
    "id": 11,
    "imdb_id": "tt0076759",
    "original_language": "en",
    "original_title": "Star Wars",
    "overview": "Princess Leia is captured and held hostage by the evil Imperial forces in their effort to take over the galactic Empire. Venturesome Luke Skywalker and dashing captain Han Solo team together with the loveable robot duo R2-D2 and C-3PO to rescue the beautiful princess and restore peace and justice in the Empire.",
    "popularity": 57.799,
    "poster_path": "/6FfCtAuVAW8XJjZ7eWeLibRLWTw.jpg",
    "production_companies": [
      {
        "id": 1,
        "logo_path": "/o86DbpburjxrqAzEDhXZcyE8pDb.png",
        "name": "Lucasfilm Ltd.",
        "origin_country": "US"
      },
      {
        "id": 25,
        "logo_path": "/qZCc1lty5FzX30aOCVRBLzaVmcp.png",
        "name": "20th Century Fox",
        "origin_country": "US"
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "1977-05-25",
    "revenue": 775398007,
    "runtime": 121,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      }
    ],
    "status": "Released",
    "tagline": "A long time ago in a galaxy far, far away...",
    "title": "Star Wars",
    "video": false,
    "vote_average": 8.2,
    "vote_count": 15885
  },
  {
    "adult": false,
    "backdrop_path": "/jn52me8AagfNt7r84SgQbV0R9ZG.jpg",
    "belongs_to_collection": {
      "id": 10,
      "name": "Star Wars Collection",
      "poster_path": "/tdQzRSk4PXX6hzjLcQWHafYtZTI.jpg",
      "backdrop_path": "/d8duYyyC9J5T825Hg7grmaabfxQ.jpg"
    },
    "budget": 250000000,
    "genres": [
      {
        "id": 28,
        "name": "Action"
      },
      {
        "id": 12,
        "name": "Adventure"
      },
      {
        "id": 878,
        "name": "Science Fiction"
      }
    ],
    "homepage": "https://www.starwars.com/films/star-wars-episode-ix-the-rise-of-skywalker",
    "id": 181812,
    "imdb_id": "tt2527338",
    "original_language": "en",
    "original_title": "Star Wars: The Rise of Skywalker",
    "overview": "The surviving Resistance faces the First Order once again as the journey of Rey, Finn and Poe Dameron continues. With the power and knowledge of generations behind them, the final battle begins.",
    "popularity": 128.547,
    "poster_path": "/db32LaOibwEliAmSL2jjDF6oDdj.jpg",
    "production_companies": [
      {
        "id": 1,
        "logo_path": "/o86DbpburjxrqAzEDhXZcyE8pDb.png",
        "name": "Lucasfilm Ltd.",
        "origin_country": "US"
      },
      {
        "id": 11461,
        "logo_path": "/p9FoEt5shEKRWRKVIlvFaEmRnun.png",
        "name": "Bad Robot",
        "origin_country": "US"
      }
    ],
    "production_countries": [
      {
        "iso_3166_1": "US",
        "name": "United States of America"
      }
    ],
    "release_date": "2019-12-18",
    "revenue": 1074144248,
    "runtime": 142,
    "spoken_languages": [
      {
        "english_name": "English",
        "iso_639_1": "en",
        "name": "English"
      }
    ],
    "status": "Released",
    "tagline": "Every generation has a legend",
    "title": "Star Wars: The Rise of Skywalker",
    "video": false,
    "vote_average": 6.5,
    "vote_count": 6973
  }
];
const peopleData = [
  {
    "adult": false,
    "also_known_as": [
      "Laura Dawn Bailey",
      "Elle Deets",
      "Laura Bailey Willingham"
    ],
    "biography": "Laura Dawn Bailey is an American voice actress.",
    "birthday": "1981-05-28",
    "deathday": null,
    "gender": 1,
    "homepage": "https://www.laurabaileyvo.com",
    "id": 75037,
    "imdb_id": "nm1154161",
    "known_for_department": "Acting",
    "name": "Laura Bailey",
    "place_of_birth": "Biloxi, Mississippi, USA",
    "popularity": 8.081,
    "profile_path": "/geVQ9DnYTU66xULx4W4jmBh68w3.jpg"
  },
  {
    "adult": false,
    "also_known_as": [],
    "biography": "Marisha Ray is an American voice actress, host, and producer. She is best known for her roles as Keyleth and Beauregard from Critical Role. In video games, Ray has voiced Margaret in Persona 4 Arena Ultimax and Persona Q, Laura Arseid in The Legend of Heroes: Trails of Cold Steel series, and Miranda in Metal Gear: Survive.",
    "birthday": "1989-05-10",
    "deathday": null,
    "gender": 1,
    "homepage": "http://www.marisharay.com/",
    "id": 1485055,
    "imdb_id": null,
    "known_for_department": "Acting",
    "name": "Marisha Ray",
    "place_of_birth": "Louisville, Kentucky, USA",
    "popularity": 2.988,
    "profile_path": "/pLP67Y4iV6vQVaV1O2K7FNmvXJX.jpg"
  },
  {
    "adult": false,
    "also_known_as": [],
    "biography": "",
    "birthday": "1981-08-03",
    "deathday": null,
    "gender": 2,
    "homepage": null,
    "id": 1221888,
    "imdb_id": "nm0992184",
    "known_for_department": "Acting",
    "name": "Travis Willingham",
    "place_of_birth": " Dallas, Texas, USA",
    "popularity": 2.188,
    "profile_path": "/g94CyOOGf8zcRwjtzX2N7csC4D5.jpg"
  },
  {
    "adult": false,
    "also_known_as": [
      "Matthew Christopher Miller",
      "Matt Mercer"
    ],
    "biography": "Matthew Mercer was born on June 29, 1982 as Matthew Christopher Miller. He is an actor and producer, known for Resident Evil: Damnation (2012), Monsters University (2013) and Resident Evil 6 (2012).",
    "birthday": "1982-06-29",
    "deathday": null,
    "gender": 2,
    "homepage": null,
    "id": 556275,
    "imdb_id": "nm2233310",
    "known_for_department": "Acting",
    "name": "Matthew Mercer",
    "place_of_birth": null,
    "popularity": 3.4,
    "profile_path": "/fe1S317253vfMuNuIDjhp9vtNSs.jpg"
  },
  {
    "adult": false,
    "also_known_as": [
      "Дэйзи Ридли",
      "黛西·蕾德莉",
      "เดซี ริดลีย์",
      "デイジー・リドリー",
      "데이지 리들리",
      "ديزي ريدلي",
      "Ντέιζι Ρίντλεϊ",
      "Daisy Jazz Isobel Ridley"
    ],
    "biography": "Daisy Jazz Isobel Ridley (born 10 April 1992) is an English actress. Ridley was born in Westminster, London.\n\nHer great-uncle was actor and playwright Arnold Ridley, of Dad's Army fame. She attended Tring Park School for the Performing Arts in Hertfordshire, graduating in 2010 at the age of 18.\n\nRidley has appeared in the television programmes such as Toast of London, Youngers, Silent Witness, Mr Selfridge and Casualty. She also appeared in the short film Blue Season, which was entered into the Sci-Fi-London 48-Hour Film Challenge. She played the lead in film three of Lifesaver, an interactive film which was nominated for a BAFTA British Academy Award. She has also appeared in the music video for Wiley's song \"Lights On\".\n\nIn April 2014, it was announced that she will play Rey, one of the lead characters in Star Wars: The Force Awakens. She was chosen for the film in February 2014. The casting of Ridley has generally been seen as a deliberate move from director J. J. Abrams, as he wishes to repeat the casting of relatively unknown actors for the lead roles like George Lucas did with the first Star Wars film in 1977.\n\nAfter receiving several competing offers, Ridley switched from her original talent agency Jonathan Arun to the United Talent Agency in August 2014. Following a two-month stint with UTA, Ridley signed on with the Creative Artists Agency in October 2014.",
    "birthday": "1992-04-10",
    "deathday": null,
    "gender": 1,
    "homepage": "http://daisyridley.us/",
    "id": 1315036,
    "imdb_id": "nm5397459",
    "known_for_department": "Acting",
    "name": "Daisy Ridley",
    "place_of_birth": "Westminster, London, England, UK",
    "popularity": 13.661,
    "profile_path": "/n8kBnNOi9VmELHJy3FdZjrSN9zT.jpg"
  },
  {
    "adult": false,
    "also_known_as": [
      "كريس إيفانز",
      "크리스 에반스",
      "Крис Эванс",
      "คริส อีแวนส์",
      "克里斯·埃文斯",
      "クリス・エヴァンス",
      "Кріс Еванс",
      "Christopher Robert \"Chris\" Evans",
      "Christopher Robert Evans",
      "Κρις Έβανς",
      "Κρίστοφερ Ρόμπερτ Έβανς",
      "Cap",
      "کریس اوانز"
    ],
    "biography": "An American actor. Evans is known for his superhero roles as the Marvel Comics characters Steve Rogers in the Marvel Cinematic Universe and the Human Torch in Fantastic Four (2005) and its 2007 sequel. Evans began his career on the 2000 television series Opposite Sex. Besides his superhero films, he has appeared in such films as Not Another Teen Movie (2001), Sunshine (2007), Scott Pilgrim vs. the World (2010), Snowpiercer (2013), and Gifted (2017). In 2014, he made his directorial debut with the drama film Before We Go, in which he also starred. Evans made his Broadway debut in a 2018 production of Lobby Hero.\n\nCourtesy Wikipedia®",
    "birthday": "1981-06-13",
    "deathday": null,
    "gender": 2,
    "homepage": null,
    "id": 16828,
    "imdb_id": "nm0262635",
    "known_for_department": "Acting",
    "name": "Chris Evans",
    "place_of_birth": "Sudbury, Massachusetts, USA",
    "popularity": 13.499,
    "profile_path": "/3bOGNsHlrswhyW79uvIHH1V43JI.jpg"
  },
  {
    "adult": false,
    "also_known_as": [
      "Скарлетт Йоганссон",
      "Скарлетт Йоханссон",
      "스칼릿 조핸슨",
      "سكارليت جوهانسون",
      "史嘉蕾·喬韓森",
      "สการ์เลตต์ โจแฮนส์สัน",
      "スカーレット・ヨハンソン",
      "斯嘉丽·约翰逊",
      "스칼렛 요한슨",
      "Σκάρλετ Τζοχάνσον",
      "اسکارلت جوهانسون"
    ],
    "biography": "Scarlett Johansson, born November 22, 1984, is an American actress, model and singer. She made her film debut in North (1994) and was later nominated for the Independent Spirit Award for Best Female Lead for her performance in Manny & Lo (1996), garnering further acclaim and prominence with roles in The Horse Whisperer (1998) and Ghost World (2001). She shifted to adult roles with her performances in Girl with a Pearl Earring (2003) and Sofia Coppola's Lost in Translation (2003), for which she won a BAFTA award for Best Actress in a Leading Role; both films earned her Golden Globe Award nominations as well.\n\nA role in A Love Song for Bobby Long (2004) earned Johansson her third Golden Globe for Best Actress nomination. Johansson garnered another Golden Globe nomination for Best Supporting Actress with her role in Woody Allen's Match Point (2005). She has played the Marvel comic book character Black Widow/Natasha Romanoff in Iron Man 2 (2010), The Avengers (2012), and Captain America: The Winter Soldier (2014), Avengers: Age of Ultron (2015), Captain America: Civil War (2016), Avengers: Infinity War (2018), Avengers: Endgame (2019), and Black Widow (2020). The 2010 Broadway revival of Arthur Miller's A View From the Bridge won Johansson the Tony Award for Best Performance by a Featured Actress in a Play. As a singer, Johansson has released two albums, Anywhere I Lay My Head and Break Up.\n\nJohansson was nominated for two Academy Awards in 2020 for her work in Marriage Story (2019), and Jojo Rabbit (2019).  Johansson was born in New York City. Her father, Karsten Johansson, is a Danish-born architect, and her paternal grandfather, Ejner Johansson, was a screenwriter and director. Her mother, Melanie Sloan, a producer, comes from an Ashkenazi Jewish family from the Bronx. Johansson has an older sister, Vanessa, who is an actress; an older brother, Adrian; a twin brother, Hunter (who appeared in the film Manny & Lo with Scarlett); and a half-brother, Christian, from her father's re-marriage.",
    "birthday": "1984-11-22",
    "deathday": null,
    "gender": 1,
    "homepage": null,
    "id": 1245,
    "imdb_id": "nm0424060",
    "known_for_department": "Acting",
    "name": "Scarlett Johansson",
    "place_of_birth": "New York City, New York, USA",
    "popularity": 38.072,
    "profile_path": "/6NsMbJXRlDZuDzatN2akFdGuTvx.jpg"
  },
  {
    "adult": false,
    "also_known_as": [
      "Thomas Jacob Black",
      "Τόμας Τζέικομπ \"Τζακ\" Μπλακ",
      "Τόμας Τζέικομπ Μπλακ",
      "Τζακ Μπλακ",
      "잭 블랙",
      "Джек Блек"
    ],
    "biography": "Thomas Jacob \"Jack\" Black (born August 28, 1969) is an American actor, comedian, singer, songwriter, and YouTuber. He makes up one half of the comedy and satirical rock duo Tenacious D. The group has two albums as well as a television series and a film. His acting career is extensive, starring primarily as bumbling, cocky, but internally self-conscious outsiders in comedy films. He was a member of the Frat Pack, a group of comedians who have appeared together in several Hollywood films, and has been nominated for a Golden Globe award. He has also won an MTV Movie Award, and a Nickelodeon Kids Choice Award.\n\n​From Wikipedia, the free encyclopedia",
    "birthday": "1969-08-28",
    "deathday": null,
    "gender": 2,
    "homepage": null,
    "id": 70851,
    "imdb_id": "nm0085312",
    "known_for_department": "Acting",
    "name": "Jack Black",
    "place_of_birth": "Santa Monica, California, USA",
    "popularity": 12.933,
    "profile_path": "/rtCx0fiYxJVhzXXdwZE2XRTfIKE.jpg"
  }
];

describe('SearchService', () => {
  let service: SearchService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(SearchService);
    httpMock = TestBed.inject(HttpTestingController)
  });

  afterEach(() => {
    httpMock.verify();
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it("should do a movie search", () => {
    service.search("test", 0).subscribe(movies => {
      expect(movies).toEqual(moviesData);
    })

    const req = httpMock.expectOne("https://api.themoviedb.org/3/search/movie?query=test&year=0&language=en-US&page=1&include_adult=false&api_key=");
    expect(req.request.method).toBe("GET");
    req.flush(moviesData)
  })

  it("should do a movie paginator", () => {
    service.paginator(2, "test", 0).subscribe(movies => {
      expect(movies).toEqual(moviesData);
    })

    const req = httpMock.expectOne("https://api.themoviedb.org/3/search/movie?query=test&year=0&language=en-US&page=2&include_adult=false&api_key=");
    expect(req.request.method).toBe("GET");
    req.flush(moviesData)
  })

  it("should do a people search", () => {
    service.personSearch("test").subscribe(people => {
      expect(people).toEqual(peopleData);
    })

    const req = httpMock.expectOne("https://api.themoviedb.org/3/search/person?query=test&language=en-US&page=1&include_adult=false&api_key=");
    expect(req.request.method).toBe("GET");
    req.flush(peopleData)
  })

  it("should do a person paginator", () => {
    service.personPaginator(2, "test").subscribe(people => {
      expect(people).toEqual(peopleData);
    })

    const req = httpMock.expectOne("https://api.themoviedb.org/3/search/person?query=test&language=en-US&page=2&include_adult=false&api_key=");
    expect(req.request.method).toBe("GET");
    req.flush(peopleData)
  })
});
