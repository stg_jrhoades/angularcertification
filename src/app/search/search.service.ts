import {Injectable, ViewChild} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {Observable} from "rxjs";

export interface QueryData {
  page: number;
  results: any[];
  total_pages: number;
  total_results: number;
}


@Injectable({
  providedIn: 'root'
})
export class SearchService {
  constructor(private http: HttpClient) { }

  personSearch = (query: string): Observable<any> => {
    return this.http.get(`https://api.themoviedb.org/3/search/person?query=${query}&language=en-US&page=1&include_adult=false&api_key=`);
  }

  search = (query: string, year: number): Observable<any> => {
      return this.http
        .get(`https://api.themoviedb.org/3/search/movie?query=${query}&year=${year}&language=en-US&page=1&include_adult=false&api_key=`)
  }

  paginator = (page:number, query: string, year: number = 0 ): Observable<any> => {
    return this.http
      .get(`https://api.themoviedb.org/3/search/movie?query=${query}&year=${year}&language=en-US&page=${page}&include_adult=false&api_key=`)
  }

  personPaginator = (page:number, query: string): Observable<any> => {
    return this.http.get(`https://api.themoviedb.org/3/search/person?query=${query}&language=en-US&page=${page}&include_adult=false&api_key=`);
  }
}
