import {AfterViewInit, Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Router} from "@angular/router";
import {MovieCollectionService} from "../movie-collection.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  constructor(private router: Router, public movieCollectionService: MovieCollectionService) { }

  // Movie table
  movieDisplayColumns: string[] = [ "title", "release_date", "vote_average", "remove"];
  movieDataSource: MatTableDataSource<any> = new MatTableDataSource<any>()

  goToMovieDetails = (id: number) => {
    this.router.navigate([`/details/movie/${id}`]);
  }

  sortMovies = () => {
    this.movieDataSource.data = this.movieDataSource.data.sort((a, b) => {
      return a.title.localeCompare(b.title);
    })
  }

  private getMovies = () => {
    this.movieCollectionService.getMovies().subscribe(movies => {
      this.movieDataSource.data = movies;
    });
  }

  // Person table
  personDisplayColumns: string[] = [ "name", "birthday", "popularity", "remove"];
  personDataSource: MatTableDataSource<any> = new MatTableDataSource<any>()

  goToPersonDetails = (id: number) => {
    this.router.navigate([`/details/person/${id}`]);
  }

  sortPeople = () => {
    this.personDataSource.data = this.personDataSource.data.sort((a, b) => {
      return a.name.localeCompare(b.name);
    })
  }

  private getPeople = () => {
    this.movieCollectionService.getPeople().subscribe(people => {
      this.personDataSource.data = people;
    });
  }

  ngOnInit(): void {
    this.getMovies();
    this.getPeople();
  }

  ngAfterViewInit() {
    this.getMovies();
    this.getPeople();
  }

}
