import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import {RouterTestingModule} from "@angular/router/testing";
import {Movie, MovieCollectionService, Person} from "../movie-collection.service";
import {Observable} from "rxjs";

const movieMockData = [{
  adult: false,
  backdrop_path: "/jWEYrAfXIPjmprktyKSvrkXVW8.jpg",
  belongs_to_collection: {
    id: 19163,
    name: "The Land Before Time Collection",
    poster_path: "/n1bjdBVThBezxR6nEf2dy43sTtV.jpg",
    backdrop_path: "/alkvR9vTtuZEmd5ygsayOfxYOMa.jpg"
  },
  budget: 0,
  genres: [
    {
      id: 10751,
      name: "Family"
    },
    {
      id: 16,
      name: "Animation"
    },
    {
      id: 12,
      name: "Adventure"
    }
  ],
  homepage: "",
  id: 19164,
  imdb_id: "tt0116817",
  original_language: "en",
  original_title: "The Land Before Time IV: Journey Through the Mists",
  overview: "Littlefoot and his dinosaur pals go in search of a medicinal flower that will cure his ailing grandfather.",
  popularity: 12.985,
  poster_path: "/sqBhPAvSwPCZq4Kg79CvxGrU7nj.jpg",
  production_companies: [
    {
      id: 4285,
      logo_path: null,
      name: "Universal Cartoon Studios",
      origin_country: ""
    }
  ],
  production_countries: [
    {
      iso_3166_1: "US",
      name: "United States of America"
    }
  ],
  release_date: "1996-11-06",
  revenue: 0,
  runtime: 74,
  spoken_languages: [
    {
      english_name: "English",
      iso_639_1: "en",
      name: "English"
    }
  ],
  status: "Released",
  tagline: "",
  title: "The Land Before Time IV: Journey Through the Mists",
  video: false,
  vote_average: 6.2,
  vote_count: 267
}, {
  adult: false,
  backdrop_path: "/sXfQGa7juaTpZ5IF17Xa6hI53nx.jpg",
  belongs_to_collection: {
    id: 19163,
    name: "The Land Before Time Collection",
    poster_path: "/n1bjdBVThBezxR6nEf2dy43sTtV.jpg",
    backdrop_path: "/alkvR9vTtuZEmd5ygsayOfxYOMa.jpg"
  },
  budget: 12500000,
  genres: [
    {
      id: 10751,
      name: "Family"
    },
    {
      id: 16,
      name: "Animation"
    },
    {
      id: 12,
      name: "Adventure"
    }
  ],
  homepage: "https://www.uphe.com/movies/the-land-before-time",
  id: 12144,
  imdb_id: "tt0095489",
  original_language: "en",
  original_title: "The Land Before Time",
  overview: "An orphaned brontosaurus named Littlefoot sets off in search of the legendary Great Valley. A land of lush vegetation where the dinosaurs can thrive and live in peace. Along the way he meets four other young dinosaurs, each one a different species, and they encounter several obstacles as they learn to work together in order to survive.",
  popularity: 13.547,
  poster_path: "/wSC2wxFj3ZgrIHlIaePeuHN4igo.jpg",
  production_companies: [
    {
      id: 6422,
      logo_path: null,
      name: "Sullivan Bluth Studios",
      origin_country: "US"
    },
    {
      id: 108270,
      logo_path: "/eDWGeg5tdVNcI8aBaaJ21bEBtFa.png",
      name: "Lucasfilm Animation",
      origin_country: "US"
    },
    {
      id: 33,
      logo_path: "/8lvHyhjr8oUKOOy2dKXoALWKdp0.png",
      name: "Universal Pictures",
      origin_country: "US"
    },
    {
      id: 20448,
      logo_path: null,
      name: "U-Drive Productions",
      origin_country: "US"
    },
    {
      id: 56,
      logo_path: "/cEaxANEisCqeEoRvODv2dO1I0iI.png",
      name: "Amblin Entertainment",
      origin_country: "US"
    }
  ],
  production_countries: [
    {
      iso_3166_1: "IE",
      name: "Ireland"
    },
    {
      iso_3166_1: "US",
      name: "United States of America"
    }
  ],
  release_date: "1988-11-18",
  revenue: 84460846,
  runtime: 69,
  spoken_languages: [
    {
      english_name: "English",
      iso_639_1: "en",
      name: "English"
    }
  ],
  status: "Released",
  tagline: "A new adventure is born.",
  title: "The Land Before Time",
  video: false,
  vote_average: 7.1,
  vote_count: 1988
},
  {
    adult: false,
    backdrop_path: "/8pJZhr2wZnaMUkISc3agk9rjrKv.jpg",
    belongs_to_collection: {
      id: 19163,
      name: "The Land Before Time Collection",
      poster_path: "/n1bjdBVThBezxR6nEf2dy43sTtV.jpg",
      backdrop_path: "/alkvR9vTtuZEmd5ygsayOfxYOMa.jpg"
    },
    budget: 0,
    genres: [
      {
        id: 16,
        name: "Animation"
      },
      {
        id: 12,
        name: "Adventure"
      },
      {
        id: 10751,
        name: "Family"
      }
    ],
    homepage: "",
    id: 19004,
    imdb_id: "tt0113596",
    original_language: "en",
    original_title: "The Land Before Time III: The Time of the Great Giving",
    overview: "Littlefoot and his young friends Cera, Ducky and Spike are being harassed by a group of older dinosaurs led by Hyp. When a meteorite strikes nearby, it causes a drought. The resulting scarcity of resources creates animosity among the different species in the Great Valley. Littlefoot and his crew resolve to venture forth to find a solution - as does Hyp and his gang. Will the groups learn to cooperate, or will their rivalry destroy them?",
    popularity: 12.238,
    poster_path: "/abcJUBDHi6hxibSgm2bxYK1wAZK.jpg",
    production_companies: [
      {
        id: 4285,
        logo_path: null,
        name: "Universal Cartoon Studios",
        origin_country: ""
      }
    ],
    production_countries: [
      {
        iso_3166_1: "US",
        name: "United States of America"
      }
    ],
    release_date: "1995-03-16",
    revenue: 0,
    runtime: 71,
    spoken_languages: [
      {
        english_name: "English",
        iso_639_1: "en",
        name: "English"
      }
    ],
    status: "Released",
    tagline: "",
    title: "The Land Before Time III: The Time of the Great Giving",
    video: false,
    vote_average: 6.2,
    vote_count: 301
  }];
const peopleMockData = [{
  adult: false,
  also_known_as: [],
  biography: "",
  birthday: "1981-08-03",
  deathday: null,
  gender: 2,
  homepage: null,
  id: 1221888,
  imdb_id: "nm0992184",
  known_for_department: "Acting",
  name: "Travis Willingham",
  place_of_birth: " Dallas, Texas, USA",
  popularity: 2.188,
  profile_path: "/g94CyOOGf8zcRwjtzX2N7csC4D5.jpg"
}, {
  adult: false,
  also_known_as: [
    "Laura Dawn Bailey",
    "Elle Deets",
    "Laura Bailey Willingham"
  ],
  biography: "Laura Dawn Bailey is an American voice actress.",
  birthday: "1981-05-28",
  deathday: null,
  gender: 1,
  homepage: "https://www.laurabaileyvo.com",
  id: 75037,
  imdb_id: "nm1154161",
  known_for_department: "Acting",
  name: "Laura Bailey",
  place_of_birth: "Biloxi, Mississippi, USA",
  popularity: 8.081,
  profile_path: "/geVQ9DnYTU66xULx4W4jmBh68w3.jpg"
},
  {
    adult: false,
    also_known_as: [],
    biography: "Marisha Ray is an American voice actress, host, and producer. She is best known for her roles as Keyleth and Beauregard from Critical Role. In video games, Ray has voiced Margaret in Persona 4 Arena Ultimax and Persona Q, Laura Arseid in The Legend of Heroes: Trails of Cold Steel series, and Miranda in Metal Gear: Survive.",
    birthday: "1989-05-10",
    deathday: null,
    gender: 1,
    homepage: "http://www.marisharay.com/",
    id: 1485055,
    imdb_id: null,
    known_for_department: "Acting",
    name: "Marisha Ray",
    place_of_birth: "Louisville, Kentucky, USA",
    popularity: 2.988,
    profile_path: "/pLP67Y4iV6vQVaV1O2K7FNmvXJX.jpg"
  }];

class MockCollection extends MovieCollectionService{
  getMovies = () => {
    return new Observable<Movie[]>(observer => {
      observer.next(movieMockData);
      observer.complete();
    })
  };
  getPeople = () => {
    return new Observable<Person[]>(observer => {
      observer.next(peopleMockData);
      observer.complete();
    })
  };
}

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardComponent ],
      imports: [ RouterTestingModule.withRoutes([]) ],
      providers: [{provide: MovieCollectionService, useClass: MockCollection}]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("Should initialized with data from mock service", () => {
    expect(component.movieDataSource.data).toEqual(movieMockData);
    expect(component.personDataSource.data).toEqual(peopleMockData);
  })

  it("Should sort movies", () => {
    component.sortMovies();
    fixture.detectChanges();
    expect(component.movieDataSource.data[0].original_title).toEqual("The Land Before Time");
    expect(component.movieDataSource.data[1].original_title).toEqual("The Land Before Time III: The Time of the Great Giving");
  })

  it("Should sort people", () => {
    component.sortPeople();
    fixture.detectChanges();
    expect(component.personDataSource.data[0].name).toEqual("Laura Bailey");
    expect(component.personDataSource.data[1].name).toEqual("Marisha Ray");
  })
});
