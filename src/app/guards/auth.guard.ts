import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from "../auth/auth.service";

// #Guards - are used on routing to help manage access to components
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private route: Router ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const path = state.url || "/login";
    switch (path) {
      case "/login":
        if (this.authService.isLoggedIn) {
          this.route.navigateByUrl("/dashboard");
          return false;
        } else {
          return true;
        }
      default:
        if (this.authService.isLoggedIn) {
          return true;
        } else {
          this.route.navigateByUrl("/login");
          return false;
        }
    }
  }

}
