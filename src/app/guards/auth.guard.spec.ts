import { AuthGuard } from './auth.guard';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from "@angular/router";
import {AuthService} from "../auth/auth.service";

function fakeRouterState(url: string): RouterStateSnapshot {
  return { url } as RouterStateSnapshot;
}

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let routerSpy: jasmine.SpyObj<Router>;
  let serviceStub: Partial<AuthService>;

  const dummyRoute = {} as ActivatedRouteSnapshot;
  const fakeUrls = ['/', '/dashboard', '/login'];

  beforeEach(() => {
    routerSpy = jasmine.createSpyObj<Router>("Router", ["navigate", "navigateByUrl"]);
    serviceStub = {};
    guard = new AuthGuard(serviceStub as AuthService, routerSpy);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  describe("when user is logged in", () => {
    beforeEach(() => {
      serviceStub.isLoggedIn = true;
    })

    it("should redirect each route to dashboard", () => {
      fakeUrls.forEach(url => {
        const canActivate = guard.canActivate(dummyRoute, fakeRouterState(url));
        if (url === "/login") {
            expect(canActivate).toBeFalse()
            expect(routerSpy.navigateByUrl).toHaveBeenCalledOnceWith("/dashboard")
            return
        }

        expect(canActivate).toBeTrue();
      })
    })
  });

  describe("when user is NOT logged in", () => {
    beforeEach(() => {
      serviceStub.isLoggedIn = false;
    });

    it("should reroute to login", () => {
      const canActivate = guard.canActivate(dummyRoute, fakeRouterState("dashboard"));
      expect(canActivate).toBeFalse()
      expect(routerSpy.navigateByUrl).toHaveBeenCalledOnceWith("/login")

    })

    it("should return false all but once", () => {
      fakeUrls.forEach(url => {
        const canActivate = guard.canActivate(dummyRoute, fakeRouterState(url));
        if (url !== "/login") {
          expect(canActivate).toBeFalse()
          return
        }

        expect(canActivate).toBeTrue();
      })
    })
  })
});
