import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it("should update the isLoggedIn variable to true", () => {
    service.login({username: "test", password: "test"});
    setTimeout(() => { expect(service.isLoggedIn).toBeTruthy() }, 1500)
  })

  it("should update the isLoggedIn to false", () => {
    service.login({username: "test", password: "test"});
    setTimeout(() => { expect(service.isLoggedIn).toBeTruthy() }, 1500)
    service.logout();
    expect(service.isLoggedIn).toBeFalsy();
  })
});
