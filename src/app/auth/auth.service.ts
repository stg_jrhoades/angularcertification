import { Injectable } from '@angular/core';
import {of} from "rxjs";
import {delay} from "rxjs/operators";

class LoginCredentials {
  username: string = "";
  password: string = "";
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor() { }

  isLoggedIn = false;

  login(cred: LoginCredentials) {
    return of({}).pipe(delay(1500)).toPromise().then(() => { this.isLoggedIn = true });
  }

  logout() {
    this.isLoggedIn = false;
  }
}
