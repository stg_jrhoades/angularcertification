import { Component, OnInit } from '@angular/core';
import {FormControl} from "@angular/forms";
import {AuthService} from "../auth.service";
import {Router} from "@angular/router";

// #Components - this decorator is what sets this class up to be a component in the angular system
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private authService: AuthService, private route: Router) { }

  // Put values in variable for better code maintenance
  private MASK = "visibility_off";
  private SHOW = "visibility";
  private SHOWPASS = "text";
  private MASKPASS = "password";

  // Form handling
  username = new FormControl("");
  password = new FormControl("");

  // #arrow function - the arrow function inherits the this of the caller
  submit = async () => {
    // #this - "this" refers to the instance of a variable, so if this component created 2 instances the this keyword
    //  is what would specify that we are interacting with one of the instances but not the other.
    await this.authService.login({ username: this.username.value, password: this.password.value })
    this.username.reset();
    this.password.reset();

    await this.route.navigate(["/dashboard"]);
  }

  // Manage masking password
  isVisible = false;
  isVisibleIcon = this.MASK;
  isVisibleType = this.MASKPASS;

  toggleVisibility = () => {
    this.isVisible = !this.isVisible;
    this.isVisibleIcon = this.isVisible ? this.SHOW : this.MASK;
    this.isVisibleType = this.isVisible ? this.SHOWPASS : this.MASKPASS;
  }

  ngOnInit(): void {}
}
