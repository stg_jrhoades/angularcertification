import { ComponentFixture, TestBed } from '@angular/core/testing';
import {By} from "@angular/platform-browser";

import { LoginComponent } from './login.component';
import {RouterTestingModule} from "@angular/router/testing";
import {AuthService} from "../auth.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

class MockAuth extends AuthService{
  async login(cred: { username: string, password: string }): Promise<void> {
    expect(cred.username).toEqual("test");
    expect(cred.password).toEqual("test");
  }
}

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        RouterTestingModule.withRoutes([]),
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [{provide: AuthService, useClass: MockAuth}]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("form data should be initialized", () => {
    expect(component.username.value).toEqual("");
    expect(component.password.value).toEqual("");
  })

  it("should send credentials", () => {
    component.username.setValue("test");
    component.password.setValue("test");
    component.submit();
    // Mock service is where the spec is located;
  });

  it("Password should be type password", () => {
    let passwordInput = fixture.debugElement.query(By.css("#password"));
    expect(passwordInput.nativeElement.type).toEqual("password");
    expect(component.isVisible).toBeFalsy();
    expect(component.isVisibleType).toEqual("password");
    expect(component.isVisibleIcon).toEqual("visibility_off")
  })

  it("Password should be type text", () => {
    component.toggleVisibility();
    fixture.detectChanges();
    console.log(component)
    const passwordInput = fixture.debugElement.query(By.css("#password"));
    expect(passwordInput.nativeElement.type).toEqual("text");
    expect(component.isVisible).toBeTruthy();
    expect(component.isVisibleType).toEqual("text");
    expect(component.isVisibleIcon).toEqual("visibility");
  })
});
