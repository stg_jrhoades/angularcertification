import { HoverExpandDirective } from './hover-expand.directive';
import {Component, DebugElement, OnInit} from "@angular/core";
import {ComponentFixture, TestBed} from "@angular/core/testing";
import {By} from "@angular/platform-browser";

const testString: string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus mauris ante, ornare ac rutrum vitae, pretium a nulla. Duis tempor sodales orci nec tincidunt. Donec lacinia imperdiet porttitor. Donec fringilla quam et nibh congue, at efficitur nisi eleifend. Aliquam erat volutpat. Cras ligula felis, maximus sed enim id, feugiat blandit urna. Nam id nulla nec nisi dapibus rutrum. Ut dignissim eu.";

@Component({
  template: `<p appHoverExpand>{{ testData }}</p>`
})
class TestHoverExpandComponent implements OnInit {
  constructor() {}
  testData = testString;

  ngOnInit() {}
}

describe('HoverExpandDirective', () => {
  let component: TestHoverExpandComponent;
  let fixture: ComponentFixture<TestHoverExpandComponent>;
  let pEl: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HoverExpandDirective, TestHoverExpandComponent]
    })

    fixture = TestBed.createComponent(TestHoverExpandComponent);
    component = fixture.componentInstance;
    pEl = fixture.debugElement.query(By.directive(HoverExpandDirective));
  })


  it("element should be queryable", () => {
    expect(pEl).not.toBeNull()
    expect(pEl.nativeElement.innerText).toBe("")
  });

  it("InnerText should be truncated", () => {
    fixture.detectChanges();
    expect(pEl.nativeElement.innerText).toBe(testString.substr(0, 200));
  })

  it("When hovered should display full text", () => {
    fixture.detectChanges();

    // Trigger event
    pEl.triggerEventHandler("mouseenter", null);
    fixture.detectChanges();
    expect(pEl.nativeElement.innerText).toBe(testString);

    // Trigger leave event
    pEl.triggerEventHandler("mouseleave", null);
    fixture.detectChanges();
    expect(pEl.nativeElement.innerText).toBe(testString.substr(0, 200));
  })
});
