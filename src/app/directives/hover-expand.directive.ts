import {AfterViewInit, Directive, ElementRef, HostListener, OnInit} from '@angular/core';


// #Directive - Directives give you ability to make a constant easy to use feature on elements want to put it on just
// by adding the directive name ot the element
@Directive({
  selector: '[appHoverExpand]'
})
export class HoverExpandDirective implements AfterViewInit {
  constructor(private el: ElementRef) {}

  originalText: string = "";
  truncateText = () => {
    this.el.nativeElement.innerText = this.el.nativeElement.innerText.substr(0, 200);
  }

  @HostListener("mouseenter") onMouseEnter() {
    this.el.nativeElement.innerText = this.originalText;
  }

  @HostListener("mouseleave") onMouseLeave() {
    this.truncateText();
  }

  ngAfterViewInit() {
    this.originalText = this.el.nativeElement.innerText;
    this.truncateText();
  }
}
