import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of} from "rxjs";

// Movie Interfaces
// #interfaces this is a light weight version of a class that helps shape the data for dev tools to help with development
interface MovieCollection {
  id: number,
  name: string,
  poster_path: string,
  backdrop_path: string
}

interface Genre {
  id: number,
  name: string
}

interface Production {
  id: number,
  logo_path: string | null,
  name: string,
  origin_country: string
}

interface Country {
  iso_3166_1: string,
  name: string
}

interface Language {
  english_name: string,
  iso_639_1: string,
  name: string
}

export interface Movie {
  adult: boolean,
  backdrop_path: string,
  belongs_to_collection: MovieCollection,
  budget: number,
  genres: Genre[],
  homepage: string,
  id: number,
  imdb_id: string,
  original_language: string,
  original_title: string,
  overview: string,
  popularity: number,
  poster_path: string,
  production_companies: Production[],
  production_countries: Country[],
  release_date: string,
  revenue: number,
  runtime: number,
  spoken_languages: Language[],
  status: string,
  tagline: string,
  title: string,
  video: boolean,
  vote_average: number,
  vote_count: number
}

// Person Interfaces
export interface Person {
  adult: boolean,
  also_known_as: string[],
  biography: string,
  birthday: string,
  deathday: string | null,
  gender: number,
  homepage: string | null,
  id: number,
  imdb_id: string | null,
  known_for_department: string,
  name: string,
  place_of_birth: string,
  popularity: number,
  profile_path: string
}


// #Service - services are isolated logics that can be called and reused across multiple components and helps centralize
// specific functions or source of data.
@Injectable({
  providedIn: 'root'
})
export class MovieCollectionService {
  constructor() { }

  // Movie Data
  private moviesSource = new BehaviorSubject<Movie[]>([]);
  movies = this.moviesSource.asObservable();

  // #Type definition - the "variable: type" syntax is defining the type. If it's a function it's defining the
  // return type if it's a variable it's defining the type it is.
  getMovies = (): Observable<Movie[]> => {
    return this.movies;
  }

  addMovie = (movie: Movie) => {
    // #const - this varible remains scoped and constant to the block it was defined
    const data: Movie[] = this.moviesSource.getValue()
    data.push(movie);
    this.moviesSource.next(data);
  }

  removeMovie = (id: number) => {
    const data: Movie[] = this.moviesSource.getValue();
    // #filter function - this goes through an array and creates a new array where the criteria provided equals true
    this.moviesSource.next(data.filter(movie => { return movie.id !== id }));
  }

  // People data
  private peopleSource = new BehaviorSubject<Person[]>([]);
  people = this.peopleSource.asObservable()

  // #Observables - observables is a rxjs way to async data and how to manage keeping the data all synced up across
  // the app
  getPeople = (): Observable<Person[]> => {
    return this.people;
  }

  addPerson = (person: Person) => {
    const data: Person[] = this.peopleSource.getValue();
    data.push(person);
    this.peopleSource.next(data);
  }

  removePerson = (id: number) => {
    const data: Person[] = this.peopleSource.getValue();
    this.peopleSource.next(data.filter(person => { return person.id !== id }));
  }
}
