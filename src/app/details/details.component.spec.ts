import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsComponent } from './details.component';
import {DetailsService} from "./details.service";
import {Observable, of} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {AuthService} from "../auth/auth.service";
import {By} from "@angular/platform-browser";

const testParam = "10";
const testMovie = {
  adult: false,
  backdrop_path: "/sXfQGa7juaTpZ5IF17Xa6hI53nx.jpg",
  belongs_to_collection: {
    id: 19163,
    name: "The Land Before Time Collection",
    poster_path: "/n1bjdBVThBezxR6nEf2dy43sTtV.jpg",
    backdrop_path: "/alkvR9vTtuZEmd5ygsayOfxYOMa.jpg"
  },
  budget: 12500000,
  genres: [
    {
      id: 10751,
      name: "Family"
    },
    {
      id: 16,
      name: "Animation"
    },
    {
      id: 12,
      name: "Adventure"
    }
  ],
  homepage: "https://www.uphe.com/movies/the-land-before-time",
  id: 12144,
  imdb_id: "tt0095489",
  original_language: "en",
  original_title: "The Land Before Time",
  overview: "An orphaned brontosaurus named Littlefoot sets off in search of the legendary Great Valley. A land of lush vegetation where the dinosaurs can thrive and live in peace. Along the way he meets four other young dinosaurs, each one a different species, and they encounter several obstacles as they learn to work together in order to survive.",
  popularity: 15.57,
  poster_path: "/wSC2wxFj3ZgrIHlIaePeuHN4igo.jpg",
  production_companies: [
    {
      id: 6422,
      logo_path: null,
      name: "Sullivan Bluth Studios",
      origin_country: "US"
    },
    {
      id: 108270,
      logo_path: "/eDWGeg5tdVNcI8aBaaJ21bEBtFa.png",
      name: "Lucasfilm Animation",
      origin_country: "US"
    },
    {
      id: 33,
      logo_path: "/8lvHyhjr8oUKOOy2dKXoALWKdp0.png",
      name: "Universal Pictures",
      origin_country: "US"
    },
    {
      id: 20448,
      logo_path: null,
      name: "U-Drive Productions",
      origin_country: "US"
    },
    {
      id: 56,
      logo_path: "/cEaxANEisCqeEoRvODv2dO1I0iI.png",
      name: "Amblin Entertainment",
      origin_country: "US"
    }
  ],
  production_countries: [
    {
      iso_3166_1: "IE",
      name: "Ireland"
    },
    {
      iso_3166_1: "US",
      name: "United States of America"
    }
  ],
  release_date: "1988-11-18",
  revenue: 84460846,
  runtime: 69,
  spoken_languages: [
    {
      english_name: "English",
      iso_639_1: "en",
      name: "English"
    }
  ],
  status: "Released",
  tagline: "A new adventure is born.",
  title: "The Land Before Time",
  video: false,
  vote_average: 7.1,
  vote_count: 1989
};
const testMoviePoster = `https://image.tmdb.org/t/p/w500${testMovie.backdrop_path}`;
const testProductionLogos = `https://image.tmdb.org/t/p/w200${testMovie.production_companies[1].logo_path}`;

class MockDetailService {
  getMovieDetails = (id: string) => {
    expect(id).toEqual(testParam);
    return new Observable<Object>(observer => {
      observer.next(testMovie);
      observer.complete();
    })
  }
}

class MockAuthService extends AuthService {
  isLoggedIn = false;
}

describe('DetailsComponent', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsComponent ],
      providers: [
        {
          provide: AuthService,
          useClass: MockAuthService
        },
        {
          provide: DetailsService,
          useClass: MockDetailService
        },
        {
          provide: ActivatedRoute,
          useValue: { params: of({  id: "10" }) }
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("Should get movie details from service", () => {
    expect(component.movie).toEqual(testMovie);
  });

  it("should have both poster and logo images", () => {
    const poster = fixture.debugElement.query(By.css("#movie-poster-image"));
    expect(poster.nativeElement.src).toEqual(testMoviePoster);

    const productionLogos = fixture.debugElement.queryAll(By.css(".movie-production-logo"));
    expect(productionLogos[1].nativeElement.src).toEqual(testProductionLogos);
  });
});
