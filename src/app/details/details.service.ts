import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DetailsService {
  constructor(private http: HttpClient) { }

  getMovieDetails = (id: string) => {
    return this.http.get(`https://api.themoviedb.org/3/movie/${id}?language=en-US&api_key=`);
  }

  getPersonDetails = (id: string) => {
    return this.http.get(`https://api.themoviedb.org/3/person/${id}?language=en-US&api_key=`);
  }

  getPersonCredits = (id: string) => {
    return this.http.get(`https://api.themoviedb.org/3/person/${id}/movie_credits?language=en-US&api_key=`)
  }
}
