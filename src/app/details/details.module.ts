import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsComponent } from './details.component';
import {RouterModule, Routes} from "@angular/router";
import {MatCardModule} from "@angular/material/card";
import {HttpClientModule} from "@angular/common/http";
import {MatButtonModule} from "@angular/material/button";
import {HoverExpandDirective} from "../directives/hover-expand.directive";
import {DatePipe} from "../pipes/date.pipe";
import {AuthModule} from "../auth/auth.module";
import { PersonDetailsComponent } from './person-details/person-details.component';
import {ShareModule} from "../share/share.module";

const routes: Routes = [
  { path: "details",
    children: [
      { path: "movie/:id", component: DetailsComponent },
      { path: "person/:id", component: PersonDetailsComponent }
    ]
  }
]

@NgModule({
  declarations: [
    DetailsComponent,
    HoverExpandDirective,
    DatePipe,
    PersonDetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    ShareModule,
    MatCardModule,
    MatButtonModule
  ],
  exports: [ RouterModule ]
})
export class DetailsModule { }
