import { Component, OnInit } from '@angular/core';
import {DetailsService} from "./details.service";
import {ActivatedRoute} from "@angular/router";
import {AuthService} from "../auth/auth.service";
import {Movie, MovieCollectionService} from "../movie-collection.service";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent implements OnInit {
  constructor(
    private detailsService: DetailsService,
    private route: ActivatedRoute,
    public authService: AuthService,
    public movieCollectionService: MovieCollectionService
  ) { }

  // Movie Data
  movie!: Movie

  getImage = (imageId: string | null, type: "poster" | "logo"): string => {
    if (!imageId) {
      return "";
    }

    switch (type) {
      case "poster":
        return `https://image.tmdb.org/t/p/w500${imageId}`;
      case "logo":
        return `https://image.tmdb.org/t/p/w200${imageId}`;
      default:
        return "";
    }
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
        this.detailsService.getMovieDetails(params.id).subscribe((movie: any) => {
          this.movie = movie
        });
    });
  }
}
