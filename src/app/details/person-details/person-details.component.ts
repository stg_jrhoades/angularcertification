import { Component, OnInit } from '@angular/core';
import {DetailsService} from "../details.service";
import {ActivatedRoute} from "@angular/router";
import {AuthService} from "../../auth/auth.service";
import {MovieCollectionService, Person} from "../../movie-collection.service";

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.css']
})
export class PersonDetailsComponent implements OnInit {
  constructor(
    private detailsService: DetailsService,
    private route: ActivatedRoute,
    public authService: AuthService,
    public movieCollectionService: MovieCollectionService
  ) { }

  // Person Data
  person!: Person;
  credits!: any;


  getImage = (id: string | null, type: "profile" | "poster"): string => {
    if (!id) {
      return "";
    }

    switch (type) {
      case "profile":
        return  `https://image.tmdb.org/t/p/w500/${id}`;
      case "poster":
        return  `https://image.tmdb.org/t/p/w200/${id}`;
      default:
        return "";
    }
  }

  getLatestMovies = (movies: any[]): any[] => {
    return movies.sort((a: any, b: any) => {
      const date1 = new Date(a.release_date);
      const date2 =  new Date(b.release_date);
      // @ts-ignore
      return date2 - date1;
    }).slice(0, 5);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.detailsService.getPersonDetails(params.id).subscribe((person: any) => {
        this.person = person
      });
      this.detailsService.getPersonCredits(params.id).subscribe(credits => {
        this.credits = credits;
      })
    });
  }

}
