import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonDetailsComponent } from './person-details.component';
import {Observable, of} from "rxjs";
import {AuthService} from "../../auth/auth.service";
import {DetailsService} from "../details.service";
import {ActivatedRoute} from "@angular/router";
import {By} from "@angular/platform-browser";

const testParam = "10";
const testPerson = {
  adult: false,
  also_known_as: [
    "Thomas Jacob Black",
    "Τόμας Τζέικομπ \"Τζακ\" Μπλακ",
    "Τόμας Τζέικομπ Μπλακ",
    "Τζακ Μπλακ",
    "잭 블랙",
    "Джек Блек"
  ],
  biography: "Thomas Jacob \"Jack\" Black (born August 28, 1969) is an American actor, comedian, singer, songwriter, and YouTuber. He makes up one half of the comedy and satirical rock duo Tenacious D. The group has two albums as well as a television series and a film. His acting career is extensive, starring primarily as bumbling, cocky, but internally self-conscious outsiders in comedy films. He was a member of the Frat Pack, a group of comedians who have appeared together in several Hollywood films, and has been nominated for a Golden Globe award. He has also won an MTV Movie Award, and a Nickelodeon Kids Choice Award.\n\n​From Wikipedia, the free encyclopedia",
  birthday: "1969-08-28",
  deathday: null,
  gender: 2,
  homepage: null,
  id: 70851,
  imdb_id: "nm0085312",
  known_for_department: "Acting",
  name: "Jack Black",
  place_of_birth: "Santa Monica, California, USA",
  popularity: 19.952,
  profile_path: "/rtCx0fiYxJVhzXXdwZE2XRTfIKE.jpg"
};
const testCredits = {
  cast: [
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        99
      ],
      id: 746391,
      original_language: "en",
      original_title: "Loving Neverland",
      overview: "Documentary that highlights the injustices and mockery that Michael Jackson dealt with post his stardom",
      poster_path: "/2ULNaiFlt10Gq9j57v5mPSJogmS.jpg",
      release_date: "2020-09-14",
      title: "Loving Neverland",
      video: false,
      vote_average: 8.3,
      vote_count: 3,
      popularity: 2.96,
      character: "Himself",
      credit_id: "5f6cb15c688cd0003627d152",
      order: 15
    },
    {
      id: 713500,
      video: true,
      vote_count: 4,
      vote_average: 8,
      title: "Dear Class of 2020",
      release_date: "2020-06-07",
      original_language: "en",
      original_title: "Dear Class of 2020",
      genre_ids: [
        99
      ],
      backdrop_path: "/uawEBM7SlvXnbeuNxokLZGzWlIn.jpg",
      adult: false,
      overview: "A virtual commencement celebration bringing together inspirational leaders, celebrities, and YouTube creators to celebrate graduates, their families, and their communities. Ring the bell to get notified when the stream goes live!",
      poster_path: "/t67VPjhtfjIawqPFlSaot1KrwVp.jpg",
      popularity: 3.351,
      character: "Self",
      credit_id: "5edd5a1602842000200dd858",
      order: 19
    },
    {
      video: false,
      vote_average: 5.9,
      vote_count: 9,
      overview: "Jack Black is the master of ceremonies, leading Courtney Cox, Lisa Kudrow, Adam Scott, and Ben Stiller through a laugh-filled escape room adventure as they solve puzzles, find clues, and crack jokes to laugh their way through a fun maze of rooms.",
      release_date: "2020-05-21",
      adult: false,
      backdrop_path: "/f5eSHfrFAIQrDuMElmwKIdog1nG.jpg",
      genre_ids: [
        10770,
        35,
        9648
      ],
      title: "Celebrity Escape Room",
      original_language: "en",
      original_title: "Celebrity Escape Room",
      poster_path: "/z21oelicEnMXZcoztMMxyOTnGvY.jpg",
      id: 707054,
      popularity: 6.697,
      character: "Self - Game Master",
      credit_id: "5ec73e17d2147c0023be9147",
      order: 1
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 702942,
      original_language: "en",
      original_title: "Feeding America Comedy Festival",
      overview: "All-star comics perform in support of Feeding America.",
      poster_path: "/mE6PuUktz0zAfI5PvsNXDdDZEzu.jpg",
      release_date: "2020-05-10",
      title: "Feeding America Comedy Festival",
      video: false,
      vote_average: 5.7,
      vote_count: 3,
      popularity: 1.677,
      character: "Himself",
      credit_id: "5eb9b1a0511d090020d4ef78",
      order: 3
    },
    {
      overview: "A global broadcast & digital special organized by Global Citizen and the World Health Organization featuring comedians, musicians, and actors to raise funds in support of front line health workers in the global response to COVID-19.",
      release_date: "2020-04-18",
      adult: false,
      backdrop_path: "/vGYlunWBrMUsPAhjkRpy0GCnIEL.jpg",
      genre_ids: [
        10402
      ],
      vote_count: 22,
      original_language: "en",
      original_title: "One World: Together at Home",
      poster_path: "/tOvv28PSM04AerehwigqdeSdlmC.jpg",
      vote_average: 7.4,
      id: 694527,
      title: "One World: Together at Home",
      video: false,
      popularity: 6.555,
      character: "Self",
      credit_id: "60d638ad66a7c300468c5899",
      order: 71
    },
    {
      release_date: "2020-01-28",
      adult: false,
      backdrop_path: "/3Ncm6vACPmuFq0WxFWppZXMbHk.jpg",
      genre_ids: [
        99
      ],
      title: "Happy Happy Joy Joy: The Ren & Stimpy Story​",
      original_language: "en",
      original_title: "Happy Happy Joy Joy: The Ren & Stimpy Story​",
      poster_path: "/9Giq5I39EF2V4mT7FYgRt1wEZnQ.jpg",
      vote_count: 7,
      video: false,
      vote_average: 5.9,
      id: 653707,
      overview: "Exploring the rise and fall of the groundbreaking animated series ​Ren & Stimpy​ and its controversial creator, John Kricfalusi, through archival footage, show artwork and interviews with the artists, actors and executives behind the show.",
      popularity: 5.698,
      character: "Himself",
      credit_id: "5ff13b4b2495ab003eac69fe",
      order: 8
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        10402
      ],
      id: 636383,
      original_language: "en",
      original_title: "Tenacious D: Rock In Rio 2019",
      overview: "“The Greatest Band on Earth,” Tenacious D will be on September 28th at Rock City World Stage. Versatile musicians, Jack Black and Kyle Gass are also actors. In 2006, they released their first feature film, Tenacious D in The Pick of Destiny. Shortly thereafter, “Rize of the Fenix” took Grammy for Best Comedy Album. From Los Angeles, the duo, formed in 1994, even recorded a series for HBO in 1999. Two years later, they released their maiden album, Tenacious D, certified platinum. The metal will strike you down in Rock City.",
      poster_path: "/2ySxcUGsJgxKqbTX8FkcRyFG5qi.jpg",
      release_date: "2019-09-28",
      title: "Tenacious D: Rock In Rio 2019",
      video: true,
      vote_average: 10,
      vote_count: 2,
      popularity: 1.646,
      character: "Himself",
      credit_id: "5d9620d635818f0174d70d52",
      order: 1
    },
    {
      genre_ids: [
        12,
        35,
        14,
        10751,
        27
      ],
      original_language: "en",
      original_title: "Goosebumps 2: Haunted Halloween",
      poster_path: "/t2wy38iiMpB8WsgJi3lYeDnGh2H.jpg",
      video: false,
      title: "Goosebumps 2: Haunted Halloween",
      vote_count: 1050,
      overview: "Two boys face an onslaught from witches, monsters, ghouls and a talking dummy after they discover a mysterious book by author R. L. Stine.",
      release_date: "2018-10-11",
      vote_average: 6.1,
      id: 442062,
      adult: false,
      backdrop_path: "/h5BvesqaxL7V3vl1CmaR8waGyiM.jpg",
      popularity: 43.887,
      character: "R. L. Stine (uncredited)",
      credit_id: "5a7b6ec49251414b0c000ebb",
      order: 44
    },
    {
      adult: false,
      backdrop_path: "/gwdiLo4SDNS6MXJQCKfGlP4ttQu.jpg",
      genre_ids: [
        35,
        10402
      ],
      id: 547025,
      original_language: "en",
      original_title: "Tenacious D in Post-Apocalypto",
      overview: "Tenacious D's animated magnum opus. Our heroes JB und KG survive a nuclear fallout by hiding in a fridge and embark on an adventure in a post-apocalyptic world.",
      poster_path: "/o1tagFS2mE3rFeA0f28Gccmo5fC.jpg",
      release_date: "2018-09-28",
      title: "Tenacious D in Post-Apocalypto",
      video: false,
      vote_average: 7.1,
      vote_count: 4,
      popularity: 2.208,
      character: "Jack Black / Jables / JB (voice), various (voices)",
      credit_id: "5b92a7ddc3a36836e20013bb",
      order: 1
    },
    {
      overview: "When ten-year-old Lewis is suddenly orphaned, he is sent to live with his Uncle Jonathan in a creaky (and creepy) old mansion with a mysterious ticking noise that emanates from the walls. Upon discovering that his uncle is a warlock, Lewis begins learning magic, but when he rebelliously resurrects an evil warlock he must find the secret of the house and save the world from destruction.",
      release_date: "2018-09-15",
      adult: false,
      backdrop_path: "/LC9g7oNJc3jZNXUU8CkwSzVK2w.jpg",
      vote_count: 1649,
      genre_ids: [
        14,
        10751,
        35
      ],
      id: 463821,
      original_language: "en",
      original_title: "The House with a Clock in Its Walls",
      poster_path: "/qM66Hv4ByAxnilr0jaqCA9uOD4Y.jpg",
      title: "The House with a Clock in Its Walls",
      video: false,
      vote_average: 6.3,
      popularity: 22.109,
      character: "Jonathan Barnavelt",
      credit_id: "59545355c3a36814ae046f50",
      order: 0
    },
    {
      id: 443009,
      adult: false,
      backdrop_path: "/aZYjP0cS1zsDHgyvDo6ymxF5JEY.jpg",
      genre_ids: [
        35,
        18
      ],
      vote_count: 479,
      original_language: "en",
      original_title: "Don't Worry, He Won't Get Far on Foot",
      poster_path: "/rKsiN37qMt8jad5GikZzSeevyI9.jpg",
      title: "Don't Worry, He Won't Get Far on Foot",
      video: false,
      vote_average: 6.7,
      release_date: "2018-04-04",
      overview: "On the rocky path to sobriety after a life-changing accident, John Callahan discovers the healing power of art, willing his injured hands into drawing hilarious, often controversial cartoons, which bring him a new lease on life.",
      popularity: 10.806,
      character: "Dexter",
      credit_id: "58ac512392514158bc01521c",
      order: 3
    },
    {
      adult: false,
      backdrop_path: "/90147e6MgbxP5pJQHi48IgQ3CN2.jpg",
      genre_ids: [
        35
      ],
      id: 419472,
      original_language: "en",
      original_title: "The Polka King",
      overview: "Local Pennsylvania polka legend Jan Lewan develops a plan to get rich that shocks his fans and lands him in jail.",
      poster_path: "/dU1XG1DI6lkZ567vmtM9ifQJ38F.jpg",
      release_date: "2017-01-22",
      title: "The Polka King",
      video: false,
      vote_average: 5.6,
      vote_count: 204,
      popularity: 8.135,
      character: "Jan Lewan",
      credit_id: "57f48634c3a368053d0016da",
      order: 0
    },
    {
      adult: false,
      backdrop_path: "/m1bZEuSnhF0apMReCgetmwXyDCV.jpg",
      genre_ids: [
        99
      ],
      id: 376528,
      original_language: "en",
      original_title: "Richard Linklater: Dream Is Destiny",
      overview: "Highlighting one of the most innovative American directors, this film reveals the path traveled by the auteur from his small-town Texas roots to his warm reception on the awards circuit. Long before he directed Boyhood, Richard Linklater’s intense desire to create fueled his work outside the Hollywood system. Rather than leave Texas, he chose to collaborate with like-minded artists crafting modest, low-budget films in a DIY style. His ability to showcase realistic characters and tell honest stories was evident from his films, and others soon took notice of his raw talent.",
      poster_path: "/xRXSiTKPSyk5LsYeXSjzJXVpDZq.jpg",
      release_date: "2016-08-05",
      title: "Richard Linklater: Dream Is Destiny",
      video: false,
      vote_average: 7.2,
      vote_count: 13,
      popularity: 4.066,
      character: "Himself",
      credit_id: "5ed140f7528b2e002069c66f",
      order: 5
    },
    {
      genre_ids: [
        28,
        12,
        16,
        35,
        10751
      ],
      original_language: "en",
      original_title: "Kung Fu Panda 3",
      poster_path: "/nlr2oxuYsHXt0wdtmzaOuVBoNC0.jpg",
      video: false,
      vote_average: 6.8,
      overview: "Continuing his \"legendary adventures of awesomeness\", Po must face two hugely epic, but different threats: one supernatural and the other a little closer to his home.",
      release_date: "2016-01-23",
      vote_count: 4302,
      id: 140300,
      adult: false,
      backdrop_path: "/kHgWFKSa0NUItppt6uW2JT1ezuQ.jpg",
      title: "Kung Fu Panda 3",
      popularity: 53.443,
      character: "Po (voice)",
      credit_id: "52fe4a8b9251416c750e5a31",
      order: 0
    },
    {
      overview: "After moving to a small town, Zach Cooper finds a silver lining when he meets next door neighbor Hannah, the daughter of bestselling Goosebumps series author R.L. Stine. When Zach unintentionally unleashes real monsters from their manuscripts and they begin to terrorize the town, it’s suddenly up to Stine, Zach and Hannah to get all of them back in the books where they belong.",
      release_date: "2015-08-05",
      adult: false,
      backdrop_path: "/6mPe4rBHzDMal2oL6hVgOBkaHz9.jpg",
      genre_ids: [
        12,
        27,
        35
      ],
      vote_count: 3009,
      original_language: "en",
      original_title: "Goosebumps",
      id: 257445,
      title: "Goosebumps",
      video: false,
      vote_average: 6.3,
      poster_path: "/aeZT9pqEgrmXOGdESFkAuVjX6kw.jpg",
      popularity: 36.356,
      character: "R.L. Stine",
      credit_id: "5310c68bc3a368299b0015fe",
      order: 0
    },
    {
      overview: "With his 20th reunion looming, Dan can’t shake his high school insecurities. In a misguided mission to prove he’s changed, Dan rekindles a friendship with the popular guy from his class and is left scrambling to protect more than just his reputation when a wild night takes an unexpected turn.",
      release_date: "2015-05-08",
      adult: false,
      backdrop_path: "/qKR3GvLXbWbLCz98TKEPzJU1xv7.jpg",
      title: "The D Train",
      genre_ids: [
        18,
        35
      ],
      vote_count: 170,
      original_language: "en",
      original_title: "The D Train",
      id: 308638,
      poster_path: "/p3X4ea0xirJRiHGRtk47fSrmH9I.jpg",
      video: false,
      vote_average: 4.7,
      popularity: 8.798,
      character: "Dan Landsman",
      credit_id: "54828108c3a36829ae006ec8",
      order: 0
    },
    {
      id: 315085,
      video: false,
      vote_count: 4,
      vote_average: 9.8,
      title: "The Concert for Valor",
      release_date: "2014-11-11",
      original_language: "en",
      original_title: "The Concert for Valor",
      genre_ids: [
        10402,
        10770
      ],
      backdrop_path: null,
      adult: false,
      overview: "A live HBO concert honoring United States veterans in Washington DC.",
      poster_path: "/q7nBd0KGa9efwk5EpcCvQgWcT7r.jpg",
      popularity: 1.595,
      character: "Himself",
      credit_id: "54a564dec3a368764f0117f0",
      order: 3
    },
    {
      adult: false,
      backdrop_path: "/jZKE0NhLuxoXLYpLm02slNESEz.jpg",
      genre_ids: [
        99
      ],
      original_language: "en",
      original_title: "21 Years: Richard Linklater",
      poster_path: "/hlgWdZlnQcA6cpYwdGvFdIHsCaV.jpg",
      id: 290304,
      video: false,
      title: "21 Years: Richard Linklater",
      vote_count: 28,
      overview: "A journey through the professional life of innovative film director Richard Linklater: 21 years creating films, carving his signature in pop culture; an analysis of his style and motivations, through the funny and moving testimonies of close friends and collaborators, actors and other filmmakers.",
      release_date: "2014-11-07",
      vote_average: 6.2,
      popularity: 3.346,
      character: "Self - Actor",
      credit_id: "569a3994c3a3686fa200121f",
      order: 0
    },
    {
      id: 225886,
      video: false,
      vote_average: 5.4,
      overview: "When Jay and Annie first got together, their romantic connection was intense – but ten years and two kids later, the flame of their love needs a spark.  To kick things up a notch, they decide – why not? – to make a video of themselves trying out every position in The Joy of Sex in one marathon three-hour session.  It seems like a great idea – until they discover that their most private video is no longer private.  With their reputations on the line, they know they’re just one click away from being laid bare to the world... but as their race to reclaim their video leads to a night they'll never forget, they'll find that their video will expose even more than they bargained for.",
      release_date: "2014-07-17",
      vote_count: 3473,
      adult: false,
      backdrop_path: "/nFCUBRDF4C6RWopNo0nAfvMA6mm.jpg",
      title: "Sex Tape",
      genre_ids: [
        35
      ],
      poster_path: "/An0sAEBH7BuXlnGPIISnqLBvjNh.jpg",
      original_language: "en",
      original_title: "Sex Tape",
      popularity: 66.673,
      character: "YouPorn Owner",
      credit_id: "54a22f58c3a3684d60000bf1",
      order: 16
    },
    {
      id: 261906,
      video: false,
      vote_count: 16,
      vote_average: 7.6,
      title: "The Sheik",
      release_date: "2014-04-26",
      original_language: "en",
      original_title: "The Sheik",
      genre_ids: [
        99
      ],
      backdrop_path: "/5AdR3tvoxMrfBstWzGpQ4Jrvj6I.jpg",
      adult: false,
      overview: "When Khosrow Vaziri became the World Wrestling Federations Iron Sheik and camel-clutched his way to fame in the 1980s, he achieved the American Dream by personifying a foreign villain. Losing his world championship belt to Hulk Hogan became a defining moment in professional wrestling. These days, the Sheiks smackdowns are on Twitter, where hes gained a new following.  Once an Olympic hopeful, bodyguard to Irans Shah and pop culture icon, we witness Vaziri struggling with addiction and despair as a family man. But with the help of Torontos Magen brothers, the Sheik begins a road to redemption and renewed status as a public figure.  Showcasing his powerful past and at times painful present, this is an insightful look at one of wrestlings biggest stars, but also a powerful story of personal sacrifice that, in the Sheiks own words, will make you humble.",
      poster_path: "/yBqXqf8K12nwm8MrntQpbJMW90v.jpg",
      popularity: 3.091,
      character: "",
      credit_id: "5f0749f11f98d1003816a395",
      order: 6
    },
    {
      id: 328407,
      video: false,
      vote_count: 34,
      vote_average: 7.6,
      title: "Metalocalypse: The Doomstar Requiem",
      release_date: "2013-10-27",
      original_language: "en",
      original_title: "Metalocalypse: The Doomstar Requiem",
      genre_ids: [
        16,
        35,
        18,
        10402
      ],
      backdrop_path: "/xRw4cBKGQ1LTftGjav34RoKrLuL.jpg",
      adult: false,
      overview: "While Toki and Abigail remain in the clutches of Magnus Hammersmith and the Metal Masked Assassin, the remaining members of Dethklok carry on with their lives while pretending not to care. But as their guilt mounts and their fans become increasingly restless with Toki's absence, they realize that they must find a way to rescue their brother.",
      poster_path: "/AjZcj7wB6GRpKeKEqeqgcU1r03Y.jpg",
      popularity: 4.763,
      character: "Dethklok's Original Manager / Fat Fan (voice)",
      credit_id: "54f54b179251412ba7000a04",
      order: 1
    },
    {
      id: 144680,
      video: false,
      vote_count: 13,
      vote_average: 8.3,
      title: "Pretty Sweet",
      release_date: "2012-11-16",
      original_language: "en",
      original_title: "Pretty Sweet",
      genre_ids: [
        99
      ],
      backdrop_path: null,
      adult: false,
      overview: "This is an epic tale of two gangs, like The Jets and The Sharks. But Girl and Chocolate aren't even gangs. Some of them act tough and some of them act like babies. But they are even more unlike the Jets and The Sharks in that they aren't even battling each other for territory. They really don't know what the hell they are doing. They don't have a feud, most of them really like each other so that is another thing they don't have in common with the Jets and the Sharks. What they do have in common with The Jets and The Sharks is they love to dance. And when I say dance, I mean SKATE. And when I say SKATE, I mean really good. From the directors that brought you Mouse, Yeah Right and Fully Flared, another chapter in this tale with no plot, no ending but beautiful inner battles acted out on a little board with wheels.",
      poster_path: "/vJbW6q6lPub2UZllwytoWIDGBh7.jpg",
      popularity: 2.429,
      character: "Himself",
      credit_id: "59621a76c3a3680dec0ad6f2",
      order: 7
    },
    {
      release_date: "2011-12-12",
      adult: false,
      backdrop_path: "/lm1F7Z2jPjLFsf0VCZMrjemPxxI.jpg",
      genre_ids: [
        16,
        28
      ],
      vote_count: 172,
      original_language: "en",
      original_title: "Kung Fu Panda: Secrets of the Masters",
      poster_path: "/4DlsFecqDZPEgDPWIW45DiFIOit.jpg",
      title: "Kung Fu Panda: Secrets of the Masters",
      video: false,
      vote_average: 6.4,
      id: 81003,
      overview: "Po and the Furious Five uncover the legend of three of kung fu's greatest heroes: Master Thundering Rhino, Master Storming Ox, and Master Croc.",
      popularity: 14.826,
      character: "Po (voice)",
      credit_id: "52fe47ef9251416c9107a949",
      order: 0
    },
    {
      overview: "When Kermit the Frog and the Muppets learn that their beloved theater is slated for demolition, a sympathetic human, Gary, and his puppet brother, Walter, swoop in to help the gang put on a show and raise the $10 million they need to save the day.",
      release_date: "2011-11-22",
      poster_path: "/nLXqBPKErQqbumuonEW6E1oLyW3.jpg",
      id: 64328,
      adult: false,
      backdrop_path: "/60PR4UC3D20dycVAoWadUzFio5R.jpg",
      genre_ids: [
        10751,
        35,
        10402
      ],
      vote_count: 1197,
      original_language: "en",
      original_title: "The Muppets",
      vote_average: 6.6,
      video: false,
      title: "The Muppets",
      popularity: 25.046,
      character: "Self (uncredited)",
      credit_id: "5fb9a763dd83fa0044d4fa8f",
      order: 73
    },
    {
      overview: "Three fanatical bird-watchers spend an entire year competing to spot the highest number of species as El Nino sends an extraordinary variety of rare breeds flying up into the U.S., but they quickly discover that there are more important things than coming out on top of the competition.",
      release_date: "2011-10-13",
      adult: false,
      backdrop_path: "/sBOTeV38Xf3xyvzEq8tpHor6pCJ.jpg",
      vote_count: 588,
      genre_ids: [
        35
      ],
      id: 73937,
      original_language: "en",
      original_title: "The Big Year",
      poster_path: "/q6Pr8fWKh664seri2MaQeIJ6eSI.jpg",
      title: "The Big Year",
      video: false,
      vote_average: 5.9,
      popularity: 12.96,
      character: "Brad Harris",
      credit_id: "52fe48b3c3a368484e106683",
      order: 0
    },
    {
      original_language: "en",
      original_title: "Kung Fu Panda 2",
      poster_path: "/mtqqD00vB4PGRt20gWtGqFhrkd0.jpg",
      video: false,
      title: "Kung Fu Panda 2",
      overview: "Po is now living his dream as The Dragon Warrior, protecting the Valley of Peace alongside his friends and fellow kung fu masters, The Furious Five - Tigress, Crane, Mantis, Viper and Monkey. But Po’s new life of awesomeness is threatened by the emergence of a formidable villain, who plans to use a secret, unstoppable weapon to conquer China and destroy kung fu. It is up to Po and The Furious Five to journey across China to face this threat and vanquish it. But how can Po stop a weapon that can stop kung fu? He must look to his past and uncover the secrets of his mysterious origins; only then will he be able to unlock the strength he needs to succeed.",
      release_date: "2011-05-25",
      vote_count: 5150,
      vote_average: 6.9,
      adult: false,
      backdrop_path: "/iKJNE3QV0wEc9VMFuHYSZVhmSN9.jpg",
      id: 49444,
      genre_ids: [
        16,
        10751
      ],
      popularity: 51.538,
      character: "Po (voice)",
      credit_id: "52fe4795c3a36847f813d913",
      order: 0
    },
    {
      adult: false,
      backdrop_path: "/xod6vFO6mimq4FHKMF61eR7nEBQ.jpg",
      genre_ids: [
        35,
        80,
        18
      ],
      id: 92591,
      original_language: "en",
      original_title: "Bernie",
      overview: "In small-town Texas, affable and popular mortician Bernie Tiede strikes up a friendship with Marjorie Nugent, a wealthy widow well known for her sour attitude. When she becomes controlling and abusive, Bernie goes to great lengths to remove himself from her grasp.",
      poster_path: "/rW5CetooG545jpkPNpD4FjFAXfe.jpg",
      release_date: "2011-04-27",
      title: "Bernie",
      video: false,
      vote_average: 6.6,
      vote_count: 676,
      popularity: 10.42,
      character: "Bernie Tiede",
      credit_id: "52fe49039251416c750babe9",
      order: 0
    },
    {
      genre_ids: [
        10402,
        35
      ],
      id: 62934,
      original_language: "en",
      original_title: "Fight for Your Right Revisited",
      poster_path: "/txbWkBtDc5fXqOaUdDgty4ouzfJ.jpg",
      video: false,
      vote_average: 6.4,
      title: "Fight for Your Right Revisited",
      overview: "Fight for Your Right Revisited stars Danny McBride, Seth Rogen, and Elijah Wood as the \"young\" Beastie Boys from the past and Jack Black, John C. Reilly, and Will Ferrell as the \"old\" Beastie Boys from the future. The story begins where the video for \"Fight for Your Right (1987)\" ended. It features music from the band's album Hot Sauce Committee Part Two.",
      release_date: "2011-04-22",
      vote_count: 55,
      adult: false,
      backdrop_path: null,
      popularity: 5.601,
      character: "MCA (B-Boys 2)",
      credit_id: "59e392f89251410b670002f4",
      order: 28
    },
    {
      poster_path: "/tazLyiF4q2w07jXjg4z4CQZgvY.jpg",
      video: false,
      vote_average: 5.2,
      id: 38745,
      overview: "Travel writer Lemuel Gulliver takes an assignment in Bermuda, but ends up on the island of Liliput, where he towers over its tiny citizens.",
      release_date: "2010-12-25",
      adult: false,
      backdrop_path: "/1EnX8qIJUYv6mG0Asub6Om8158z.jpg",
      vote_count: 1829,
      genre_ids: [
        10751,
        35,
        12,
        14
      ],
      title: "Gulliver's Travels",
      original_language: "en",
      original_title: "Gulliver's Travels",
      popularity: 34.458,
      character: "Lemuel Gulliver",
      credit_id: "52fe46d99251416c91061c71",
      order: 0
    },
    {
      release_date: "2010-11-26",
      adult: false,
      backdrop_path: "/rXkvj9mInzC6sHYycBXfqEknLCG.jpg",
      genre_ids: [
        16,
        10751,
        10770,
        35,
        14
      ],
      vote_count: 228,
      original_language: "en",
      original_title: "Kung Fu Panda Holiday",
      poster_path: "/rV77WxY35LuYLOuQvBeD1nyWMuI.jpg",
      title: "Kung Fu Panda Holiday",
      video: false,
      vote_average: 6.9,
      id: 50393,
      overview: "The Winter Feast is Po's favorite holiday. Every year he and his father hang decorations, cook together, and serve noodle soup to the villagers. But this year Shifu informs Po that as Dragon Warrior, it is his duty to host the formal Winter Feast at the Jade Palace. Po is caught between his obligations as the Dragon Warrior and his family traditions: between Shifu and Mr. Ping.",
      popularity: 18.345,
      character: "Po (voice)",
      credit_id: "52fe47c6c3a36847f81478fd",
      order: 0
    },
    {
      id: 110278,
      video: false,
      vote_count: 3,
      vote_average: 6,
      title: "Tenacious D Live at BlizzCon 2010",
      release_date: "2010-10-23",
      original_language: "en",
      original_title: "Tenacious D Live at BlizzCon 2010",
      genre_ids: [],
      backdrop_path: null,
      adult: false,
      overview: "With all the powers of face-melting at their disposal, the rocking force of Tenacious D has gone down a storm at BlizzCon 2010.  With tasty riffs, shredding solos and 15 buckets of rock-sweat, Jack Black and the “Rage Kage” Kyle Gass managed to tear of the roof of the Anaheim Convention Center which housed this 2010’s BlizzCon.",
      poster_path: null,
      popularity: 1.251,
      character: "as JB",
      credit_id: "52fe4acfc3a36847f81e2ed5",
      order: 0
    },
    {
      release_date: "2009-06-18",
      adult: false,
      backdrop_path: "/21RU5YYiLk3UVF9lRhf2fs1tRUK.jpg",
      id: 17610,
      genre_ids: [
        35,
        12
      ],
      original_language: "en",
      original_title: "Year One",
      poster_path: "/thbOE3y9cXqRIeGYSyPP0nYFGz5.jpg",
      vote_count: 1396,
      video: false,
      vote_average: 4.9,
      title: "Year One",
      overview: "When a couple of lazy hunter-gatherers are banished from their primitive village, they set off on an epic journey through the ancient world.",
      popularity: 15.94,
      character: "Zed",
      credit_id: "52fe47379251416c75091ad3",
      order: 0
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [],
      id: 279699,
      original_language: "en",
      original_title: "Prop 8: The Musical",
      overview: "The conflict of the aftermath of Proposition 8 (banning gay marriage in California) is solved by Jesus.",
      poster_path: "/mgY6VGRlEKPgoFcKkFI8XMxWsUV.jpg",
      release_date: "2008-12-03",
      title: "Prop 8: The Musical",
      video: true,
      vote_average: 7.4,
      vote_count: 18,
      popularity: 2.517,
      character: "Jesus Christ",
      credit_id: "5b68974f0e0a267ef10d98a9",
      order: 7
    },
    {
      adult: false,
      backdrop_path: "/osRhBUpGIFjwfVfHuIYVG18T9QA.jpg",
      genre_ids: [
        16,
        10751,
        28
      ],
      id: 15854,
      original_language: "en",
      original_title: "Kung Fu Panda: Secrets of the Furious Five",
      overview: "Ordered to teach a martial arts class of rambunctious bunny kittens, Po tells stories of each of the Furious Five's pasts.",
      poster_path: "/tv52mGWtf13T3XEQj3Heow1whuc.jpg",
      release_date: "2008-11-08",
      title: "Kung Fu Panda: Secrets of the Furious Five",
      video: false,
      vote_average: 6.7,
      vote_count: 295,
      popularity: 18.325,
      character: "Po (voice)",
      credit_id: "52fe46859251416c7507aee1",
      order: 0
    },
    {
      id: 133753,
      video: true,
      vote_count: 7,
      vote_average: 5.6,
      title: "Tenacious D: The Complete Masterworks 2",
      release_date: "2008-11-04",
      original_language: "en",
      original_title: "Tenacious D: The Complete Masterworks 2",
      genre_ids: [
        10402
      ],
      backdrop_path: "/sqNFv3ppiOqh6hm7pmFk13zneir.jpg",
      adult: false,
      overview: "Features footage from the band's performances in Seattle, Washington on February 16 and 17, 2007, in addition to numerous bonus videos and D Tour: A Tenacious Documentary. It is the successor to the band's previous collection The Complete Master Works.",
      poster_path: "/1x0GbfRyobOj1RZ0j6a2SsCCHMP.jpg",
      popularity: 2.661,
      character: "Himself",
      credit_id: "52fe4bbbc3a368484e196f93",
      order: 0
    },
    {
      id: 36131,
      video: false,
      vote_count: 44,
      vote_average: 6.6,
      title: "Tropic Thunder: Rain of Madness",
      release_date: "2008-08-26",
      original_language: "en",
      original_title: "Tropic Thunder: Rain of Madness",
      genre_ids: [
        28,
        35
      ],
      backdrop_path: null,
      adult: false,
      overview: "A behind-the-scenes mockumentary of Tropic Thunder.",
      poster_path: null,
      popularity: 4.207,
      character: "Jeff Portnoy",
      credit_id: "52fe45ce9251416c9103f0ff",
      order: 1
    },
    {
      backdrop_path: "/1riTqK5ouEgjTs69MVm0F8dXilZ.jpg",
      genre_ids: [
        28,
        35,
        12,
        10752
      ],
      original_language: "en",
      original_title: "Tropic Thunder",
      poster_path: "/zAurB9mNxfYRoVrVjAJJwGV3sPg.jpg",
      video: false,
      vote_average: 6.6,
      vote_count: 4491,
      overview: "Ben Stiller, Jack Black and Robert Downey Jr. lead an ensemble cast in 'Tropic Thunder,' an action comedy about a group of self-absorbed actors who set out to make the most expensive war film. After ballooning costs force the studio to cancel the movie, the frustrated director refuses to stop shooting, leading his cast into the jungles of Southeast Asia, where they encounter real bad guys.",
      id: 7446,
      title: "Tropic Thunder",
      release_date: "2008-08-09",
      adult: false,
      popularity: 19.618,
      character: "Jeff Portnoy",
      credit_id: "52fe447dc3a36847f80992e5",
      order: 1
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [],
      id: 784886,
      original_language: "en",
      original_title: "VH1 Rock Honors: The Who",
      overview: "The Who were honored at the 2008 ceremony from the Pauley Pavilion at UCLA instead of the usual Las Vegas Mandalay Bay Events Center. The concert consisted of an hour-long performance by the band as well as tributes by Incubus, Pearl Jam, Foo Fighters, Flaming Lips, Adam Sandler and Tenacious D.",
      poster_path: "/9K4ISpCXGhXwR2Nelpmw0Au4lqJ.jpg",
      release_date: "2008-07-17",
      title: "VH1 Rock Honors: The Who",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 1.342,
      character: "",
      credit_id: "5ffbd291957e6d003d14447f",
      order: 5
    },
    {
      genre_ids: [
        28,
        12,
        16,
        10751,
        35
      ],
      original_language: "en",
      original_title: "Kung Fu Panda",
      poster_path: "/wWt4JYXTg5Wr3xBW2phBrMKgp3x.jpg",
      title: "Kung Fu Panda",
      video: false,
      id: 9502,
      overview: "When the Valley of Peace is threatened, lazy Po the panda discovers his destiny as the \"chosen one\" and trains to become a kung fu hero, but transforming the unsleek slacker into a brave warrior won't be easy. It's up to Master Shifu and the Furious Five -- Tigress, Crane, Mantis, Viper and Monkey -- to give it a try.",
      release_date: "2008-06-04",
      vote_count: 8666,
      adult: false,
      backdrop_path: "/qdthf9WrRDSaIkGVQGhhJ9pz1hn.jpg",
      vote_average: 7.2,
      popularity: 66.911,
      character: "Po (voice)",
      credit_id: "52fe4500c3a36847f80b6ca3",
      order: 0
    },
    {
      id: 127655,
      video: false,
      vote_count: 3,
      vote_average: 7,
      title: "D Tour: A Tenacious Documentary",
      release_date: "2008-04-24",
      original_language: "en",
      original_title: "D Tour: A Tenacious Documentary",
      genre_ids: [
        99,
        10402
      ],
      backdrop_path: "/2YY2ZEa0Vjdg3hh9Hp6DCpodYH2.jpg",
      adult: false,
      overview: "A documentary focusing on Tenacious D's world tour in support of their film and soundtrack to 'Tenacious D in The Pick of Destiny'.",
      poster_path: "/kc3mRv6zDRBDr6l7hVOx7tP4L39.jpg",
      popularity: 1.245,
      character: "",
      credit_id: "52fe4b02c3a368484e1729ad",
      order: 0
    },
    {
      adult: false,
      backdrop_path: "/nbmPpumlhuR8jcmW4buPQzliKU3.jpg",
      genre_ids: [
        18,
        35
      ],
      id: 4953,
      original_language: "en",
      original_title: "Be Kind Rewind",
      overview: "A man whose brain becomes magnetized unintentionally destroys every tape in his friend's video store. In order to satisfy the store's most loyal renter, an aging woman with signs of dementia, the two men set out to remake the lost films.",
      poster_path: "/f0oX20YrQEiVPDH9InCQ1d3Cm66.jpg",
      release_date: "2008-01-20",
      title: "Be Kind Rewind",
      video: false,
      vote_average: 6.3,
      vote_count: 1010,
      popularity: 8.31,
      character: "Jerry",
      credit_id: "52fe43e7c3a36847f807740f",
      order: 0
    },
    {
      genre_ids: [
        35,
        10402,
        18
      ],
      original_language: "en",
      original_title: "Walk Hard: The Dewey Cox Story",
      poster_path: "/Aa1IQ4Cuin3d7qIahvPheMmR4E5.jpg",
      title: "Walk Hard: The Dewey Cox Story",
      vote_average: 6.6,
      overview: "Following a childhood tragedy, Dewey Cox follows a long and winding road to music stardom. Dewey perseveres through changing musical styles, an addiction to nearly every drug known and bouts of uncontrollable rage.",
      release_date: "2007-12-21",
      vote_count: 483,
      video: false,
      adult: false,
      backdrop_path: "/axg73mVxiZcE9Pzynu2uj1TYIgj.jpg",
      id: 6575,
      popularity: 10.947,
      character: "Paul McCartney (uncredited)",
      credit_id: "52fe445bc3a36847f80913b5",
      order: 69
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 231165,
      original_language: "en",
      original_title: "Tenacious D: For the Ladies",
      overview: "A short improvised featurette that accompanies the feature film TENACIOUS D: THE PICK OF DESTINY.",
      poster_path: "/dB4hwRY2Vxu31VKqOkCS94FPer3.jpg",
      release_date: "2007-02-27",
      title: "Tenacious D: For the Ladies",
      video: false,
      vote_average: 7,
      vote_count: 1,
      popularity: 0.6,
      character: "",
      credit_id: "52fe4de7c3a36847f8273be3",
      order: 0
    },
    {
      release_date: "2006-12-08",
      adult: false,
      backdrop_path: "/nbxDCGO60Pbh20AH2YXkXrGVQoT.jpg",
      genre_ids: [
        35,
        10749
      ],
      vote_count: 3771,
      original_language: "en",
      original_title: "The Holiday",
      poster_path: "/6fbqG49Q7IWBWdyJ7asNTcNbnG6.jpg",
      title: "The Holiday",
      video: false,
      vote_average: 7.1,
      id: 1581,
      overview: "Two women, one from the United States and one from the United Kingdom, swap homes at Christmastime after bad breakups with their boyfriends. Each woman finds romance with a local man but realizes that the imminent return home may end the relationship.",
      popularity: 17.627,
      character: "Miles Dumont",
      credit_id: "52fe4300c3a36847f803325d",
      order: 3
    },
    {
      vote_average: 6.7,
      overview: "In Venice Beach, naive Midwesterner JB bonds with local slacker KG and they form the rock band Tenacious D. Setting out to become the world's greatest band is no easy feat, so they set out to steal what could be the answer to their prayers... a magical guitar pick housed in a rock-and-roll museum some 300 miles away.",
      release_date: "2006-11-22",
      adult: false,
      backdrop_path: "/frWnQLBGB6ROzdK2aPCCLRnz24J.jpg",
      id: 2179,
      genre_ids: [
        35,
        10402
      ],
      vote_count: 1165,
      original_language: "en",
      original_title: "Tenacious D in The Pick of Destiny",
      poster_path: "/8baHzEIfrBvBy5b9XKe87Skl1tf.jpg",
      title: "Tenacious D in The Pick of Destiny",
      video: false,
      popularity: 14.081,
      character: "JB",
      credit_id: "52fe433ec3a36847f8045463",
      order: 0
    },
    {
      id: 683614,
      video: true,
      vote_count: 0,
      vote_average: 0,
      title: "The Eighth Blunder of the World",
      release_date: "2006-11-14",
      original_language: "en",
      original_title: "The Eighth Blunder of the World",
      genre_ids: [
        35
      ],
      backdrop_path: null,
      adult: false,
      overview: "Blooper reel for Peter Jackson's King Kong (2005)",
      poster_path: null,
      popularity: 1.092,
      character: "Himself",
      credit_id: "5e6f2e474f9a990011513d1b",
      order: 1
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        99
      ],
      id: 121251,
      original_language: "en",
      original_title: "Recreating the Eighth Wonder: The Making of 'King Kong'",
      overview: "Following the immense success of The Lord of the Rings trilogy, Peter Jackson directs King Kong (2005). This documentary follows him, and the immense project from start to finish. It turns out that this one film may actually be a larger task to complete than all 3 of the Lord of the Rings films together.",
      poster_path: null,
      release_date: "2006-11-14",
      title: "Recreating the Eighth Wonder: The Making of 'King Kong'",
      video: false,
      vote_average: 5.5,
      vote_count: 2,
      popularity: 1.257,
      character: "Himself",
      credit_id: "52fe4a5ec3a368484e14f679",
      order: 5
    },
    {
      video: false,
      vote_average: 6.2,
      overview: "Nacho Libre is loosely based on the story of Fray Tormenta (\"Friar Storm\"), aka Rev. Sergio Gutierrez Benitez, a real-life Mexican Catholic priest who had a 23-year career as a masked luchador. He competed in order to support the orphanage he directed.",
      release_date: "2006-06-16",
      id: 9353,
      adult: false,
      backdrop_path: "/nNCleqiADKVMKWDC5G7hjYcVCmv.jpg",
      genre_ids: [
        35,
        10751
      ],
      vote_count: 1106,
      original_language: "en",
      original_title: "Nacho Libre",
      poster_path: "/kh7B91bMl2lZ0mH9WhPfaNUIEQH.jpg",
      title: "Nacho Libre",
      popularity: 47.975,
      character: "Nacho",
      credit_id: "52fe44ecc3a36847f80b1fe7",
      order: 0
    },
    {
      adult: false,
      backdrop_path: "/xvdiZO5bEHLcnx4JUiImGQ5pckg.jpg",
      genre_ids: [
        99
      ],
      id: 82864,
      original_language: "en",
      original_title: "King Kong: Peter Jackson's Production Diaries",
      overview: "Academy Award - winning filmmaker Peter Jackson invites you behind the scenes of his latest movie to witness the birth of King Kong.",
      poster_path: "/tnj9XYsA0iBh2kgQgARiQ0sEY8I.jpg",
      release_date: "2005-12-13",
      title: "King Kong: Peter Jackson's Production Diaries",
      video: false,
      vote_average: 7.5,
      vote_count: 35,
      popularity: 3.975,
      character: "Himself",
      credit_id: "52fe48799251416c9108db65",
      order: 1
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        16,
        35,
        10751
      ],
      id: 118254,
      original_language: "en",
      original_title: "Club Oscar",
      overview: "Picking up where Shark Tale ends, all the characters of the film dance at the whale wash in a spoof of Saturday Night Fever.",
      poster_path: "/o8o3lBY3oiI8NwpzZw8nLMUH3FA.jpg",
      release_date: "2005-02-08",
      title: "Club Oscar",
      video: false,
      vote_average: 5.1,
      vote_count: 8,
      popularity: 1.685,
      character: "Lenny (voice)",
      credit_id: "5ebac53ac3514c002061615b",
      order: 5
    },
    {
      video: false,
      vote_average: 6,
      overview: "Oscar is a small fish whose big aspirations often get him into trouble. Meanwhile, Lenny is a great white shark with a surprising secret that no sea creature would guess: He's a vegetarian. When a lie turns Oscar into an improbable hero and Lenny becomes an outcast, the two form an unlikely friendship.",
      release_date: "2004-09-20",
      adult: false,
      backdrop_path: "/z1oPj03g6b9F5VyUTyQMp7IJ7Y9.jpg",
      vote_count: 5121,
      genre_ids: [
        16,
        28,
        35,
        10751
      ],
      id: 10555,
      original_language: "en",
      original_title: "Shark Tale",
      poster_path: "/r08DpyPyhXcJTfNZAICNGMzcQ8l.jpg",
      title: "Shark Tale",
      popularity: 65.176,
      character: "Lenny (voice)",
      credit_id: "52fe43879251416c75013e5d",
      order: 3
    },
    {
      genre_ids: [
        35
      ],
      original_language: "en",
      original_title: "Anchorman: The Legend of Ron Burgundy",
      poster_path: "/gGi19Jl0x1gDQv43TNfAc3wYeeu.jpg",
      video: false,
      title: "Anchorman: The Legend of Ron Burgundy",
      vote_count: 3189,
      overview: "It's the 1970s and San Diego anchorman Ron Burgundy is the top dog in local TV, but that's all about to change when ambitious reporter Veronica Corningstone arrives as a new employee at his station.",
      release_date: "2004-07-09",
      vote_average: 6.7,
      id: 8699,
      adult: false,
      backdrop_path: "/5iHPV6M6aPKxXv3w8LTxsD6Zeot.jpg",
      popularity: 16.496,
      character: "Motorcyclist",
      credit_id: "52fe44b5c3a36847f80a5a0b",
      order: 41
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [],
      id: 687709,
      original_language: "en",
      original_title: "Stuck on You: It's Funny - The Farrelly Formula",
      overview: "Features snippets from a lot of the big names who have worked with the brothers in the past. Gwyneth Paltrow, Jack Black, Jason Alexander, Matt Damon, Chris Elliott, Ben Stiller and Greg Kinnear are all on hand to give their thoughts on what makes their comedy films work so well. The brothers themselves are also interviewed and they talk in a bit of detail about how they stick to their own creative process to get their movies made.",
      poster_path: "/4R9iul2tTNLlOBk0iW4hThta7bj.jpg",
      release_date: "2004-04-20",
      title: "Stuck on You: It's Funny - The Farrelly Formula",
      video: true,
      vote_average: 2,
      vote_count: 1,
      popularity: 1.4,
      character: "Himself",
      credit_id: "5e80f18d89b5610017400923",
      order: 9
    },
    {
      adult: false,
      backdrop_path: "/uefhJ3kVYrU5oW0VBJMuCAXlTzH.jpg",
      genre_ids: [
        35,
        18,
        10749
      ],
      id: 34681,
      original_language: "en",
      original_title: "Melvin Goes to Dinner",
      overview: "Marital infidelity, religion, a guy in heaven wearing a Wizards jersey, anal fetishes, cigarettes and schizophrenia, ghosts, and how it’s going to get worse before it gets better.",
      poster_path: "/zRaeIMhn6yKzzk5PEqji5UpJZoM.jpg",
      release_date: "2003-12-04",
      title: "Melvin Goes to Dinner",
      video: false,
      vote_average: 5.5,
      vote_count: 15,
      popularity: 1.83,
      character: "Mental Patient (uncredited)",
      credit_id: "607c026766f2d200291e7450",
      order: 34
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        10402,
        35
      ],
      id: 111160,
      original_language: "en",
      original_title: "Tenacious D: The Complete Masterworks",
      overview: "Documenting their gig at the Brixton Academy on November 3, 2002. Also contains the band's short lived HBO TV series, a collection of short films, two documentaries on the band, music videos, and live TV appearances. The video was a major success, going 6x platinum in the US in the Video Longform category.",
      poster_path: "/2QKGvggkbstazDBsab8r5E5g73x.jpg",
      release_date: "2003-11-04",
      title: "Tenacious D: The Complete Masterworks",
      video: true,
      vote_average: 7.2,
      vote_count: 17,
      popularity: 2.609,
      character: "Himself",
      credit_id: "52fe4af2c3a36847f81e9e03",
      order: 0
    },
    {
      backdrop_path: "/pM1CnP0ZN2VxmrTAbBZOS3PNoNE.jpg",
      genre_ids: [
        35,
        10402,
        10751
      ],
      original_language: "en",
      original_title: "School of Rock",
      poster_path: "/zXLXaepIBvFVLU25DH3wv4IPSbe.jpg",
      video: false,
      title: "School of Rock",
      vote_count: 4305,
      overview: "Fired from his band and hard up for cash, guitarist and vocalist Dewey Finn finagles his way into a job as a fifth-grade substitute teacher at a private school, where he secretly begins teaching his students the finer points of rock 'n' roll. The school's hard-nosed principal is rightly suspicious of Finn's activities. But Finn's roommate remains in the dark about what he's doing.",
      release_date: "2003-10-03",
      vote_average: 7.1,
      id: 1584,
      adult: false,
      popularity: 19.799,
      character: "Dewey Finn",
      credit_id: "5ac4e0a69251412720050c05",
      order: 0
    },
    {
      id: 687788,
      video: true,
      vote_count: 4,
      vote_average: 6.5,
      title: "Shallow Hal: Seeing Through the Make-up",
      release_date: "2002-07-02",
      original_language: "en",
      original_title: "Shallow Hal: Seeing Through the Make-up",
      genre_ids: [
        35,
        99
      ],
      backdrop_path: null,
      adult: false,
      overview: "This short documentary provides discussion of how the fat suits and other make-up effects were done for the film as well as considerations of exactly the look. There's also some test footage of the make-up effects and interviews with Paltrow, Black, the Farrellys, Paltrow and Paltrow's body double, Ivy.",
      poster_path: "/6wHWKqySY5sfS3MJq3gID2zLjUi.jpg",
      popularity: 1.4,
      character: "Himself",
      credit_id: "5e812ef20bc5290016efd169",
      order: 7
    },
    {
      original_language: "en",
      original_title: "Ice Age",
      poster_path: "/gLhHHZUzeseRXShoDyC4VqLgsNv.jpg",
      video: false,
      vote_average: 7.3,
      overview: "With the impending ice age almost upon them, a mismatched trio of prehistoric critters – Manny the woolly mammoth, Diego the saber-toothed tiger and Sid the giant sloth – find an orphaned infant and decide to return it to its human parents. Along the way, the unlikely allies become friends but, when enemies attack, their quest takes on far nobler aims.",
      release_date: "2002-03-10",
      vote_count: 10305,
      adult: false,
      backdrop_path: "/mCVQ2cZmGkAHG2Q3fDZTQA1YzeI.jpg",
      title: "Ice Age",
      genre_ids: [
        16,
        35,
        10751,
        12
      ],
      id: 425,
      popularity: 116.611,
      character: "Zeke (voice)",
      credit_id: "52fe4242c3a36847f801037f",
      order: 7
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 14923,
      original_language: "en",
      original_title: "Run Ronnie Run",
      overview: "A redneck with an uncanny knack for getting arrested becomes the star of his own reality program.",
      poster_path: "/3AYJFRGu3rEGfFurxlc53RWiSWQ.jpg",
      release_date: "2002-01-20",
      title: "Run Ronnie Run",
      video: false,
      vote_average: 6.2,
      vote_count: 45,
      popularity: 4.564,
      character: "Lead Chimney Sweep",
      credit_id: "546a1d5aeaeb816b4b00370b",
      order: 66
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        99,
        35
      ],
      id: 687782,
      original_language: "en",
      original_title: "Reel Comedy: Shallow Hal",
      overview: "The Comedy Central \"Reel Comedy\" special provides a stage for Black, who starts riffing in the interview and coming up with some funny bits. Paltrow, Robbins and the Farrelly Brothers also are interviewed.",
      poster_path: "/6wHWKqySY5sfS3MJq3gID2zLjUi.jpg",
      release_date: "2001-11-06",
      title: "Reel Comedy: Shallow Hal",
      video: true,
      vote_average: 0,
      vote_count: 0,
      popularity: 0.665,
      character: "Himself",
      credit_id: "5e8129c2ce4ddc00137367ef",
      order: 3
    },
    {
      adult: false,
      backdrop_path: "/9cKpfklF4CgMiNMOk53yDzfzF3q.jpg",
      genre_ids: [
        18
      ],
      id: 32274,
      original_language: "en",
      original_title: "Cradle Will Rock",
      overview: "A true story of politics and art in the 1930s USA, centered around a leftist musical drama and attempts to stop its production.",
      poster_path: "/d8LJWY8kaAIlfMiQLOGglL8VqVT.jpg",
      release_date: "1999-12-10",
      title: "Cradle Will Rock",
      video: false,
      vote_average: 6.7,
      vote_count: 57,
      popularity: 6.325,
      character: "Sid",
      credit_id: "56820e7b92514131df01043d",
      order: 15
    },
    {
      video: false,
      vote_average: 5.5,
      overview: "A romantic comedy about a mysterious love letter that turns a sleepy New England town upside down.",
      release_date: "1999-05-21",
      adult: false,
      backdrop_path: "/oYEp07KhxUuPYJHrLqq7S4FHcaA.jpg",
      vote_count: 44,
      genre_ids: [
        35,
        10749
      ],
      title: "The Love Letter",
      original_language: "en",
      original_title: "The Love Letter",
      poster_path: "/7w8QCvKDFn1cJ3sW3RGWcwlitgX.jpg",
      id: 31342,
      popularity: 2.592,
      character: "Fisherman (uncredited)",
      credit_id: "5e6ba7a2b04228001a4993bb",
      order: 12
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [],
      id: 184500,
      original_language: "en",
      original_title: "Heat Vision and Jack",
      overview: "As the result of a NASA miscalculation, Astronaut Jack Austin flew too close to the sun, the rays expanding his mind and making him the smartest man on the planet. Now he and his talking motorcycle (who contains the mind of his former roommate) are on the run from NASA and their assassin, actor Ron Silver who want to take his brain. They travel from place to place solving paranormal mysteries with the help of Jack's newfound ability to become a super genius when his brain absorbs sunlight.",
      poster_path: "/hhy3UIunQbUlpSZQXSbDUc9ca8U.jpg",
      release_date: "1999-01-10",
      title: "Heat Vision and Jack",
      video: false,
      vote_average: 5.8,
      vote_count: 15,
      popularity: 3.706,
      character: "Jack",
      credit_id: "52fe4cc19251416c751249f1",
      order: 0
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        10751,
        12,
        16
      ],
      id: 664996,
      original_language: "en",
      original_title: "Apollo 10½: A Space Age Adventure",
      overview: "A coming-of-age story set in the suburbs of Houston, Texas in the summer of 1969, against the backdrop of the historic Apollo 11 moon landing.  The story will be told from two interwoven perspectives, capturing the astronaut and mission control view of the triumphant moment, alongside the lesser-seen, bottom-up perspective of what it was like from an excited kid's perspective, living near NASA but mostly watching it on TV like hundreds of millions of others.",
      poster_path: null,
      release_date: "",
      title: "Apollo 10½: A Space Age Adventure",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 1.374,
      character: "",
      credit_id: "5f108c3b688cd0003848dd14",
      order: 0
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35,
        10402
      ],
      id: 821480,
      original_language: "en",
      original_title: "Tenacious D: Time Fixers",
      overview: "After an open mic night, JB and KG get a message from Michael Keaton who tells them that the time-space continuum has been disrupted at the Ford's Theater in 1850 and only Tenacious D can fix it.",
      poster_path: "/mTqQxXRPUkq4ZOPPldQtrmzfFfH.jpg",
      release_date: "2006-11-14",
      title: "Tenacious D: Time Fixers",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 1.212,
      character: "JB",
      credit_id: "6080cb5ef6787a004044d7f6",
      order: 0
    },
    {
      genre_ids: [
        12,
        18,
        28
      ],
      original_language: "en",
      original_title: "King Kong",
      poster_path: "/lBtlVlECMW98tz3a3O1C2s1vric.jpg",
      video: false,
      title: "King Kong",
      vote_count: 6146,
      overview: "In 1933 New York, an overly ambitious movie producer coerces his cast and hired ship crew to travel to mysterious Skull Island, where they encounter Kong, a giant ape who is immediately smitten with the leading lady.",
      release_date: "2005-12-12",
      vote_average: 6.8,
      id: 254,
      adult: false,
      backdrop_path: "/mRM2NB0i3wv4HqxXvwIjEVi4Qqq.jpg",
      popularity: 37.962,
      character: "Carl Denham",
      credit_id: "52fe422ec3a36847f800a1a3",
      order: 1
    },
    {
      id: 10710,
      vote_average: 4.9,
      overview: "A man becomes increasingly jealous of his friend's newfound success.",
      release_date: "2004-04-30",
      adult: false,
      backdrop_path: "/6p6iDDKyFbPlezsmdILJmrx3Amg.jpg",
      vote_count: 390,
      genre_ids: [
        35
      ],
      video: false,
      original_language: "en",
      original_title: "Envy",
      poster_path: "/RMaKg5mVnGVI0z3SvIgS7hYPUt.jpg",
      title: "Envy",
      popularity: 9.891,
      character: "Nick Vanderpark",
      credit_id: "52fe43a69251416c75018c21",
      order: 1
    },
    {
      adult: false,
      backdrop_path: "/5DgySIGlGpt0YN34TZ6be5a87cF.jpg",
      genre_ids: [
        35,
        18
      ],
      id: 11857,
      original_language: "en",
      original_title: "Orange County",
      overview: "Shaun Brumder is a local surfer kid from Orange County who dreams of going to Stanford to become a writer and to get away from his dysfunctional family household. Except Shaun runs into one complication after another, starting when his application is rejected after his dim-witted guidance counselor sends in the wrong form.",
      poster_path: "/efmTNIyPbKXDWYSNkC7FsFFeVJj.jpg",
      release_date: "2002-01-10",
      title: "Orange County",
      video: false,
      vote_average: 6.1,
      vote_count: 325,
      popularity: 11.066,
      character: "Lance Brumder",
      credit_id: "52fe44949251416c75039c8f",
      order: 1
    },
    {
      adult: false,
      backdrop_path: "/9TbKP25IA3UiWkkzq390ChrkdVB.jpg",
      genre_ids: [
        35,
        10749
      ],
      id: 9889,
      original_language: "en",
      original_title: "Shallow Hal",
      overview: "A shallow man falls in love with a 300 pound woman because of her \"inner beauty\".",
      poster_path: "/q4lZrHWTWuybb6pzMucj1c0ngCW.jpg",
      release_date: "2001-11-01",
      title: "Shallow Hal",
      video: false,
      vote_average: 6.1,
      vote_count: 2222,
      popularity: 23.557,
      character: "Hal Larson",
      credit_id: "52fe4541c3a36847f80c3e51",
      order: 1
    },
    {
      adult: false,
      backdrop_path: "/scrH5AMMD2o73vwgCnhcYXx8zrt.jpg",
      genre_ids: [
        35,
        80,
        10749
      ],
      id: 10878,
      original_language: "en",
      original_title: "Saving Silverman",
      overview: "A pair of buddies conspire to save their best friend from marrying the wrong woman, a cold-hearted beauty who snatches him from them and breaks up their Neil Diamond cover band.",
      poster_path: "/5mq8J11266ZL7HCOwDGXoaU6eIO.jpg",
      release_date: "2001-02-09",
      title: "Saving Silverman",
      video: false,
      vote_average: 5.5,
      vote_count: 380,
      popularity: 11.445,
      character: "J.D. McNugent",
      credit_id: "52fe43c89251416c7501e009",
      order: 1
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 341004,
      original_language: "en",
      original_title: "Micronations",
      overview: "A misfit living amongst neighborhoods and homes that have declared themselves sovereign nations, is recruited to wage a battle against the nation of Wayne County, Nevada.",
      poster_path: null,
      release_date: "",
      title: "Micronations",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 0.6,
      character: "",
      credit_id: "555d9965c3a3686957000fe0",
      order: 1
    },
    {
      video: false,
      vote_average: 6.9,
      overview: "As Po looks for his lost action figures, the story of how the panda inadvertently helped create the Furious Five is told.",
      release_date: "2012-01-31",
      adult: false,
      backdrop_path: "/qt2KqQatKL52f7ApYnpfsjQAP13.jpg",
      vote_count: 142,
      genre_ids: [
        16,
        10751,
        35
      ],
      id: 381693,
      original_language: "en",
      original_title: "Kung Fu Panda: Secrets of the Scroll",
      poster_path: "/giZ4VFIG1DVeHUcJKCbtcRLM43b.jpg",
      title: "Kung Fu Panda: Secrets of the Scroll",
      popularity: 13.778,
      character: "Po (voice)",
      credit_id: "56ed8acec3a3682253007a1b",
      order: 1
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        99
      ],
      id: 413509,
      original_language: "en",
      original_title: "Tenacious D: In the Studio",
      overview: "Tenacious D documenting their time in the studio with Dave Grohl and Steve McDonald.",
      poster_path: "/u9gkTbZOQgXx2GWb1MNcr1tR6Hd.jpg",
      release_date: "2003-11-04",
      title: "Tenacious D: In the Studio",
      video: false,
      vote_average: 10,
      vote_count: 2,
      popularity: 0.84,
      character: "Himself",
      credit_id: "57c675c19251412f67000c34",
      order: 1
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        99
      ],
      id: 416801,
      original_language: "en",
      original_title: "Under the Smogberry Trees",
      overview: "A documentary about the legendary radio show \"The Dr. Demento Show\", as well as its brilliant host, Barry Hansen.",
      poster_path: "/e0ycqCQzBdHwqkDwbIEkMhuTeuM.jpg",
      release_date: "",
      title: "Under the Smogberry Trees",
      video: false,
      vote_average: 10,
      vote_count: 2,
      popularity: 1.4,
      character: "Himself",
      credit_id: "57e00d359251413d740121bc",
      order: 1
    },
    {
      release_date: "2017-12-09",
      adult: false,
      backdrop_path: "/zJDMuXQDraHjtF53wikmyBQIcYe.jpg",
      genre_ids: [
        12,
        28,
        35,
        14
      ],
      vote_count: 10944,
      original_language: "en",
      original_title: "Jumanji: Welcome to the Jungle",
      poster_path: "/pSgXKPU5h6U89ipF7HBYajvYt7j.jpg",
      title: "Jumanji: Welcome to the Jungle",
      video: false,
      vote_average: 6.8,
      id: 353486,
      overview: "The tables are turned as four teenagers are sucked into Jumanji's world - pitted against rhinos, black mambas and an endless variety of jungle traps and puzzles. To survive, they'll play as characters from the game.",
      popularity: 78.034,
      character: "Professor Sheldon 'Shelly' Oberon",
      credit_id: "57436ac6c3a3686ffd002c6c",
      order: 1
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        10402,
        35
      ],
      id: 635972,
      original_language: "en",
      original_title: "Tenacious D: Live in London",
      overview: "This live concert, filmed at London's Brixton Academy on November 3, 2002 where the band performed Flash, Wonderboy, Explosivo, Medley, Karate, Kyle Quit the Band, Friendship, Kielbasa, Dio, The Road, The Cosmis Shame, FHG, Tribute, Rock Your Socks, and Double Team. Your ass will be ROCKED.",
      poster_path: "/4fi79jKWKI70Ign8zpyY5mTTZVc.jpg",
      release_date: "2002-11-03",
      title: "Tenacious D: Live in London",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 0.6,
      character: "",
      credit_id: "5d940b1021c4ca00307ee276",
      order: 1
    },
    {
      id: 467037,
      video: false,
      vote_count: 17,
      vote_average: 7.4,
      title: "Jack Black: Spider-Man",
      release_date: "2002-06-06",
      original_language: "en",
      original_title: "Jack Black: Spider-Man",
      genre_ids: [
        35
      ],
      backdrop_path: null,
      adult: false,
      overview: "Peter Parker is no ordinary kid, but what if he was played by Jack Black? That's right, you got yourself some pretty irresponsible hero!",
      poster_path: null,
      popularity: 4.312,
      character: "Spider-Man",
      credit_id: "5daa517dce6c4c0018b8f90a",
      order: 1
    },
    {
      id: 69723,
      video: false,
      vote_count: 13,
      vote_average: 7.1,
      title: "Lord of the Piercing",
      release_date: "2002-01-01",
      original_language: "en",
      original_title: "Lord of the Piercing",
      genre_ids: [],
      backdrop_path: null,
      adult: false,
      overview: "A spoof of The Fellowship of the Ring (specifically, the Council of Elrond scene) from the 2002 MTV Movie Awards.",
      poster_path: null,
      popularity: 1.879,
      character: "Jack",
      credit_id: "5b4c780dc3a36823ed03f958",
      order: 1
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [],
      id: 769762,
      original_language: "en",
      original_title: "everything must go",
      overview: "Jack Black cuts all his head and beard off while saying nothing... that is until his son shows up to offer a hand.",
      poster_path: null,
      release_date: "",
      title: "everything must go",
      video: true,
      vote_average: 0,
      vote_count: 0,
      popularity: 0.6,
      character: "Himself",
      credit_id: "5fc3219c4d6791003cd895b5",
      order: 1
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 843371,
      original_language: "en",
      original_title: "Oh Hell No",
      overview: "",
      poster_path: null,
      release_date: "2022-06-17",
      title: "Oh Hell No",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 3.22,
      character: "Sherman",
      credit_id: "60d99b760ff15a00488dd4e1",
      order: 1
    },
    {
      id: 512200,
      adult: false,
      backdrop_path: "/zTxHf9iIOCqRbxvl8W5QYKrsMLq.jpg",
      genre_ids: [
        12,
        35,
        14
      ],
      original_language: "en",
      original_title: "Jumanji: The Next Level",
      poster_path: "/jyw8VKYEiM1UDzPB7NsisUgBeJ8.jpg",
      video: false,
      vote_average: 7,
      vote_count: 6099,
      overview: "As the gang return to Jumanji to rescue one of their own, they discover that nothing is as they expect. The players will have to brave parts unknown and unexplored in order to escape the world’s most dangerous game.",
      release_date: "2019-12-04",
      title: "Jumanji: The Next Level",
      popularity: 257.087,
      character: "Professor Sheldon 'Shelly' Oberon",
      credit_id: "5aac39700e0a267821004f36",
      order: 2
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        99
      ],
      id: 530964,
      original_language: "en",
      original_title: "The Insufferable Groo",
      overview: "Having made nearly 200 low-budget movies in just two decades, Utah writer-director Stephen Groo is now seeking production funds and the involvement of celebrity fan Jack Black for a remake of his 2004 human/elf fantasy drama ‘The Unexpected Race’. Scott Christopherson’s hilarious yet sincere portrait depicts this uphill battle, while examining the unusual methods of a determined DIY auteur.",
      poster_path: "/6DQMMkCXu79wMW7vLxzXZJ7fXMN.jpg",
      release_date: "2018-06-07",
      title: "The Insufferable Groo",
      video: false,
      vote_average: 5.7,
      vote_count: 3,
      popularity: 0.974,
      character: "Himself",
      credit_id: "5b2866f69251412c54000fca",
      order: 2
    },
    {
      adult: false,
      backdrop_path: "/jQTYh5kX3WtC45Zjbey1T5aMYsC.jpg",
      genre_ids: [
        35
      ],
      id: 478846,
      original_language: "en",
      original_title: "Ricky Dicks' Rock Docs: \"I Feel Love\"",
      overview: "Ricky Dicks relays the history of iconic disco track 'I Feel Love'.",
      poster_path: "/svkB4Hja7PugF8UaelDFV8azs31.jpg",
      release_date: "2017-09-13",
      title: "Ricky Dicks' Rock Docs: \"I Feel Love\"",
      video: true,
      vote_average: 10,
      vote_count: 2,
      popularity: 1.4,
      character: "Giorgio Moroder",
      credit_id: "59cfb2e79251412ef302113d",
      order: 2
    },
    {
      original_language: "en",
      original_title: "The Words That Built America",
      poster_path: "/pk7TYOGbtIHeVLSmmsfSgAKp75Z.jpg",
      video: false,
      vote_average: 7,
      overview: "In recognition of the 4th of July, several celebrities and politicians of differing ideologies join to read the historic documents which laid the foundation for the United States of America.",
      release_date: "2017-07-04",
      vote_count: 7,
      title: "The Words That Built America",
      adult: false,
      backdrop_path: "/oeFNndgqfIB9uRUkROzbyLvfLR8.jpg",
      id: 464655,
      genre_ids: [
        99
      ],
      popularity: 4.213,
      character: "Reader - Declaration of Independence",
      credit_id: "59e392e99251410b67000039",
      order: 2
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        27
      ],
      id: 463683,
      original_language: "en",
      original_title: "Tommy Can't Sleep",
      overview: "Tommy can't sleep because there's rats in his room. A short film by Die Antwoord.",
      poster_path: "/4cNN9DZeNa9huHH8ouYoAjWpDdB.jpg",
      release_date: "2017-06-20",
      title: "Tommy Can't Sleep",
      video: false,
      vote_average: 5.2,
      vote_count: 5,
      popularity: 0.864,
      character: "Big Tommy",
      credit_id: "5952fab49251411dbd00c1ad",
      order: 2
    },
    {
      adult: false,
      backdrop_path: "/leUVJPZbA8rZIkcn33y46LGMPv2.jpg",
      genre_ids: [
        99,
        10402,
        10770
      ],
      id: 705899,
      original_language: "en",
      original_title: "We Are One With President-Elect Barack Obama",
      overview: "A public celebration of the inauguration of Barack Obama as the 44th President of the United States of America at the Lincoln Memorial and the National Mall in Washington DC, on January 18, 2009.",
      poster_path: "/aqFHftRn2gRe5psczpv37J0Aqsi.jpg",
      release_date: "2009-01-18",
      title: "We Are One With President-Elect Barack Obama",
      video: false,
      vote_average: 5,
      vote_count: 1,
      popularity: 4.899,
      character: "Self",
      credit_id: "607185c4dd4716006e5350af",
      order: 2
    },
    {
      adult: false,
      backdrop_path: "/RnOmbeR3OVesxnVlfTBvmISp6K.jpg",
      genre_ids: [
        35,
        18
      ],
      original_language: "en",
      original_title: "Margot at the Wedding",
      poster_path: "/kIT4H088qeCzlGaLRVx5CGIIp30.jpg",
      video: false,
      title: "Margot at the Wedding",
      vote_count: 182,
      overview: "Margot Zeller is a short story writer with a sharp wit and an even sharper tongue. On the eve of her estranged sister Pauline's wedding to unemployed musician/artist/depressive Malcolm at the family seaside home, Margot shows up unexpectedly to rekindle the sisterly bond and offer her own brand of support. What ensues is a nakedly honest and subversively funny look at family dynamics.",
      release_date: "2007-09-11",
      vote_average: 5.9,
      id: 13998,
      popularity: 10.991,
      character: "Malcolm",
      credit_id: "52fe45c19251416c750614e9",
      order: 2
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 821464,
      original_language: "en",
      original_title: "Tenacious D: Rock Star Sperm for Sale",
      overview: "JB and KG decide the best way to make money is to sell their sperm, so they go into the restroom and do their business into a huge glass. When they have enough, they begin to sell it in the street. They meet some pretty weird people.",
      poster_path: "/aQ4h3ZPMossWVbmzarhKWkpGLUT.jpg",
      release_date: "2002-10-20",
      title: "Tenacious D: Rock Star Sperm for Sale",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 0.6,
      character: "JB",
      credit_id: "6080bf94726fb10059b50a80",
      order: 1
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 821430,
      original_language: "en",
      original_title: "Tenacious D: Butt Baby",
      overview: "JB and KG head out to the desert to expand their minds with LSD.",
      poster_path: "/4pwiJCKqe1hAQgGB9RxWjYnzJAl.jpg",
      release_date: "2002-04-05",
      title: "Tenacious D: Butt Baby",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 0.6,
      character: "JB",
      credit_id: "6080ad1c01b1ca002a6092fa",
      order: 1
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        99,
        35
      ],
      id: 687777,
      original_language: "en",
      original_title: "Being 'Shallow Hal'",
      overview: "The HBO special is hosted by star Brooke Burns, who spends more time talking to people on the street about what it means to be shallow than discussing the film itself.",
      poster_path: "/6wHWKqySY5sfS3MJq3gID2zLjUi.jpg",
      release_date: "2001-11-06",
      title: "Being 'Shallow Hal'",
      video: true,
      vote_average: 0,
      vote_count: 0,
      popularity: 0.841,
      character: "Self",
      credit_id: "5e8126aa2a210c00152aa051",
      order: 2
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 821379,
      original_language: "en",
      original_title: "Tenacious D: JB's BJ",
      overview: "JB goes out on the streets looking to prostitute himself. A Tenacious D short created for their American 2001 tour.",
      poster_path: "/r24DpnxQZfk6dHNJoT21Ck6IMaW.jpg",
      release_date: "2001-09-20",
      title: "Tenacious D: JB's BJ",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 0.6,
      character: "JB",
      credit_id: "60808d2d01b1ca00586a021b",
      order: 1
    },
    {
      id: 243,
      overview: "When record store owner Rob Gordon gets dumped by his girlfriend, Laura, because he hasn't changed since they met, he revisits his top five breakups of all time in an attempt to figure out what went wrong. As Rob seeks out his former lovers to find out why they left, he keeps up his efforts to win Laura back.",
      release_date: "2000-03-17",
      adult: false,
      backdrop_path: "/tmP4COlAaowO0FniW30aatOcmMq.jpg",
      genre_ids: [
        35,
        18,
        10749,
        10402
      ],
      vote_count: 1444,
      original_language: "en",
      original_title: "High Fidelity",
      poster_path: "/e2LZGB62GMhv3Fo8tDZjY87I81a.jpg",
      title: "High Fidelity",
      video: false,
      vote_average: 7.2,
      popularity: 12.562,
      character: "Barry",
      credit_id: "52fe422cc3a36847f80098f3",
      order: 3
    },
    {
      backdrop_path: null,
      genre_ids: [
        35,
        18,
        10749
      ],
      original_language: "en",
      original_title: "Jesus' Son",
      poster_path: "/e9bjD4ljTkakrcsPYtrEHmHjivt.jpg",
      video: false,
      vote_average: 6.3,
      id: 25636,
      overview: "A young man turns from drug addiction and petty crime to a life redeemed by a discovery of compassion.",
      release_date: "1999-09-05",
      vote_count: 44,
      title: "Jesus' Son",
      adult: false,
      popularity: 4.28,
      character: "Georgie",
      credit_id: "52fe44d3c3a368484e039097",
      order: 2
    },
    {
      adult: false,
      backdrop_path: "/lPLTLCBn39Wiy1RPQLB9xOKyF2v.jpg",
      genre_ids: [
        28,
        18,
        53
      ],
      id: 9798,
      original_language: "en",
      original_title: "Enemy of the State",
      overview: "A hotshot Washington criminal lawyer becomes the target of a rogue security executive videotaped in the act of murdering a congressman when the incriminating tape is surreptitiously slipped into his shopping bag by the videographer, who is fleeing the executive's assassins.",
      poster_path: "/x9pXrMKLsBGGOFyyZ0Gwt9YpVub.jpg",
      release_date: "1998-11-20",
      title: "Enemy of the State",
      video: false,
      vote_average: 7,
      vote_count: 2832,
      popularity: 16.261,
      character: "Fiedler",
      credit_id: "52fe452fc3a36847f80c0fc7",
      order: 8
    },
    {
      genre_ids: [
        27,
        9648,
        53
      ],
      original_language: "en",
      original_title: "I Still Know What You Did Last Summer",
      poster_path: "/motBJIO921LFZkoGKCSbmSCVHBc.jpg",
      id: 3600,
      title: "I Still Know What You Did Last Summer",
      vote_average: 5.3,
      overview: "Unfinished business with coed Julie James brings the murderer to the Bahamas to terrorize her and her friends, Karla, Tyrell and Will, during a vacation. Can Ray Bronson who survived a bloody attack alongside Julie two summers ago, get to the island in time to save everyone?",
      release_date: "1998-11-13",
      vote_count: 1073,
      adult: false,
      backdrop_path: "/iwMaQZz4hRiEoZAqq1rvcYnYokj.jpg",
      video: false,
      popularity: 25.599,
      character: "Titus Telesco",
      credit_id: "56ba1d2892514106af002d7b",
      order: 10
    },
    {
      adult: false,
      backdrop_path: "/wOmMJqsUhPDaxtgP5vBIVxvZSui.jpg",
      genre_ids: [
        53,
        80,
        18,
        9648
      ],
      id: 68744,
      original_language: "en",
      original_title: "Johnny Skidmarks",
      overview: "Johnny Scardino is working for blackmailers, photographing wealthy guys in seedy motels. One such assignment turns the wrong way and blackmailers die one by one. Is Johnny the next on the list?",
      poster_path: "/5wDgsmiYA9S7Zf9zieW20Ukg4x3.jpg",
      release_date: "1998-01-18",
      title: "Johnny Skidmarks",
      video: false,
      vote_average: 6.8,
      vote_count: 6,
      popularity: 1.924,
      character: "Jerry",
      credit_id: "54b2575ac3a3682313001423",
      order: 5
    },
    {
      genre_ids: [
        28,
        53,
        12,
        80
      ],
      original_language: "en",
      original_title: "The Jackal",
      poster_path: "/wkLF73oenC1n1DDKKU7oyLKVcMa.jpg",
      video: false,
      vote_average: 6.3,
      overview: "Hired by a powerful member of the Russian mafia to avenge an FBI sting that left his brother dead, a psychopathic hitman known only as The Jackal proves an elusive target for the people charged with the task of bringing him down: a deputy FBI director, a Russian MVK Major,  and a jailed IRA terrorist who can recognize him.",
      release_date: "1997-11-14",
      vote_count: 1359,
      id: 4824,
      adult: false,
      backdrop_path: "/p8vlNmqPbptMSy5mn8FFJ05SG7P.jpg",
      title: "The Jackal",
      popularity: 18.262,
      character: "Ian Lamont",
      credit_id: "52fe43dcc3a36847f8074b5f",
      order: 7
    },
    {
      adult: false,
      backdrop_path: "/ELsTifJ2lu4vsMhoHeZ5EnncHw.jpg",
      genre_ids: [
        35,
        14,
        878
      ],
      id: 75,
      original_language: "en",
      original_title: "Mars Attacks!",
      overview: "'We come in peace' is not what those green men from Mars mean when they invade our planet, armed with irresistible weapons and a cruel sense of humor.  This star studded cast must play victim to the alien’s fun and games in this comedy homage to science fiction films of the '50s and '60s.",
      poster_path: "/hll4O5vSAfnZDb6JbnP06GPtz7b.jpg",
      release_date: "1996-12-12",
      title: "Mars Attacks!",
      video: false,
      vote_average: 6.4,
      vote_count: 4141,
      popularity: 15.598,
      character: "Billy Glenn Norris",
      credit_id: "564626ec9251413ade0001d2",
      order: 15
    },
    {
      genre_ids: [
        28,
        878,
        35
      ],
      original_language: "en",
      original_title: "Crossworlds",
      poster_path: "/A2aqbMX7EoYg23NfD6lCusZrZyR.jpg",
      video: false,
      vote_average: 5,
      overview: "College good guy Joe is drawn into a battle to save the world from arch-enemy Ferris. Joe's heirloom pendant just happens to be the key to the staff that opens doors to the Crossworlds. When Laura shows up to check on the key and Ferris' goons begin their assaults, they run to semi-retired adventurer A.T. for help and guidance.",
      id: 17821,
      vote_count: 34,
      title: "Crossworlds",
      adult: false,
      backdrop_path: "/qaHHOvacg90lJ2V2pvl9pfNULSR.jpg",
      release_date: "1996-10-30",
      popularity: 3.464,
      character: "Steve",
      credit_id: "52fe474b9251416c750943c5",
      order: 5
    },
    {
      genre_ids: [
        53,
        28
      ],
      original_language: "en",
      original_title: "The Fan",
      poster_path: "/lu7CjP8YES5dJMCFg5O9o9jCkjl.jpg",
      video: false,
      title: "The Fan",
      vote_count: 557,
      overview: "When the San Francisco Giants pay center-fielder, Bobby Rayburn $40 million to lead their team to the World Series, no one is happier or more supportive than #1 fan, Gil Renard.  When Rayburn becomes mired in the worst slump of his career, the obsessed Renard decides to stop at nothing to help his idol regain his former glory—not even murder.",
      release_date: "1996-08-15",
      vote_average: 5.9,
      id: 9566,
      adult: false,
      backdrop_path: "/dISWllRkiQ1l5Nbn5JMZbz6RHoh.jpg",
      popularity: 11.879,
      character: "Broadcast Technician",
      credit_id: "5a8206d2c3a3684d07004cd2",
      order: 24
    },
    {
      adult: false,
      backdrop_path: "/2gA2qjVprFMH3JKmKnxyOjOaBP.jpg",
      genre_ids: [
        35,
        18,
        53
      ],
      id: 9894,
      original_language: "en",
      original_title: "The Cable Guy",
      overview: "When recently single Steven moves into his new apartment, cable guy Chip comes to hook him up—and doesn't let go. Initially, Chip is just overzealous in his desire to be Steven's pal, but when Steven tries to end the 'friendship', Chip shows his dark side. He begins stalking Steven, who's left to fend for himself because no one else can believe Chip's capable of such behaviour.",
      poster_path: "/5cZySBvy41eHTD5LyQn48aP444k.jpg",
      release_date: "1996-06-10",
      title: "The Cable Guy",
      video: false,
      vote_average: 5.9,
      vote_count: 1720,
      popularity: 15.411,
      character: "Rick",
      credit_id: "52fe4542c3a36847f80c41c9",
      order: 3
    },
    {
      adult: false,
      backdrop_path: "/4ODWHlHUeWz94rA0n4bwNOtMVJA.jpg",
      genre_ids: [
        14,
        10751
      ],
      original_language: "en",
      original_title: "The NeverEnding Story III",
      poster_path: "/o75HHfr23sgeqIeUPwjKY0V9Ppi.jpg",
      id: 27793,
      video: false,
      title: "The NeverEnding Story III",
      overview: "A young boy must restore order when a group of bullies steal the magical book that acts as a portal between Earth and the imaginary world of Fantasia.",
      release_date: "1994-10-26",
      vote_count: 285,
      vote_average: 4.4,
      popularity: 16.755,
      character: "Slip",
      credit_id: "52fe4566c3a368484e0584fb",
      order: 2
    },
    {
      adult: false,
      backdrop_path: "/xPGnubqfNPTAdNrr447lX9nUTSl.jpg",
      genre_ids: [
        28,
        878,
        12,
        35,
        53
      ],
      original_language: "en",
      original_title: "Borderlands",
      poster_path: "/wxWqjPkPWUOqktEtU3BCKxRfTA6.jpg",
      vote_count: 0,
      video: false,
      vote_average: 0,
      title: "Borderlands",
      overview: "In the distant future, four \"vault hunters\" travel to the planet Pandora to hunt down an alien vault rumored to contain advanced technology.",
      id: 365177,
      popularity: 1.4,
      character: "Claptrap (voice)",
      credit_id: "602575ffaf58cb003ea4dee2",
      order: 3
    },
    {
      video: false,
      vote_average: 0,
      overview: "A young woman named Amber is sent to live with her father who she hasn't seen in over 10 years after suffering a car accident that takes the life of her mother.  As Amber is healing she discovers a man hiding in the woods that is an elf.  She finds out Elves have always lived, but he is the last being a FBI agent was sent out to destroy them.  As they fall in love, Lythorin must make a decision if he wants to stay with Amber or stay alone.",
      release_date: "2018-10-14",
      title: "The Unexpected Race",
      adult: false,
      backdrop_path: null,
      id: 558910,
      genre_ids: [
        18
      ],
      vote_count: 0,
      original_language: "en",
      original_title: "The Unexpected Race",
      poster_path: "/mZNK0cqLJDp1GSwWihmLnkRZbVd.jpg",
      popularity: 0.64,
      character: "Sheriff",
      credit_id: "5bde3b3dc3a3682b29018bdc",
      order: 4
    },
    {
      adult: false,
      backdrop_path: "/rJMzSM1cga0NWwwh67EUFE3IJ8E.jpg",
      genre_ids: [
        99
      ],
      id: 253292,
      original_language: "en",
      original_title: "Harmontown",
      overview: "A comedic, brutally honest documentary following self-destructive TV writer Dan Harmon (NBC's Community) as he takes his live podcast on a national tour.",
      poster_path: "/5HlxDzy3LZ26r5as5pLX7jYVcyC.jpg",
      release_date: "2014-03-08",
      title: "Harmontown",
      video: false,
      vote_average: 6.8,
      vote_count: 58,
      popularity: 5.53,
      character: "Himself",
      credit_id: "5302197c925141218c2eed0c",
      order: 4
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        16,
        10751,
        35,
        28
      ],
      original_language: "en",
      original_title: "DreamWorks Holiday Classics (Merry Madagascar / Shrek the Halls / Gift of the Night Fury / Kung Fu Panda Holiday)",
      poster_path: "/liaVMXtLLuOPF9TwMEH5kJYLIFc.jpg",
      id: 792098,
      video: true,
      vote_average: 6,
      vote_count: 2,
      overview: "Join your favorite DreamWorks friends for these four holiday specials. Watch as those zany zoosters from Madagascar save Christmas, Donkey puts on a caroling Christmas Shrek-tacular, and Po prepares for his favorite holiday, the Winter Feast.",
      release_date: "2012-12-25",
      title: "DreamWorks Holiday Classics (Merry Madagascar / Shrek the Halls / Gift of the Night Fury / Kung Fu Panda Holiday)",
      popularity: 8.187,
      character: "",
      credit_id: "6016f194d96c3c004011431d",
      order: 4
    },
    {
      adult: false,
      backdrop_path: "/xWZqP9Q0dOYnRMpj6qNcnfOC9hu.jpg",
      genre_ids: [
        10751,
        16,
        35
      ],
      id: 286488,
      original_language: "en",
      original_title: "Dreamworks Holiday Classics (Merry Madagascar / Shrek the Halls / Gift of the Night Fury)",
      overview: "Join your favourite DreamWorks friends for these four holiday specials. Watch as those zany zoosters from Madagascar save Christmas, Donkey puts on a carolling Christmas Shrek-tacular, and Po prepares for his favourite holiday, the Winter Feast.",
      poster_path: "/yKfYz4orhr868isBVUgElckuGJV.jpg",
      release_date: "2012-10-30",
      title: "Dreamworks Holiday Classics (Merry Madagascar / Shrek the Halls / Gift of the Night Fury)",
      video: true,
      vote_average: 7.4,
      vote_count: 55,
      popularity: 7.349,
      character: "",
      credit_id: "601ad2a81b70ae003c6b0d94",
      order: 4
    },
    {
      adult: false,
      backdrop_path: "/eUZHhgwv0zwR3LPadxeBNfh7VCw.jpg",
      genre_ids: [
        99,
        10402
      ],
      id: 41120,
      original_language: "en",
      original_title: "Rush: Beyond The Lighted Stage",
      overview: "An in-depth look at the Canadian rock band Rush, chronicling the band's musical evolution from their progressive rock sound of the '70s to their current heavy rock style.",
      poster_path: "/5su3xR3k0doJmUYIVk8O5QnXnxn.jpg",
      release_date: "2010-06-29",
      title: "Rush: Beyond The Lighted Stage",
      video: false,
      vote_average: 7.8,
      vote_count: 56,
      popularity: 6.678,
      character: "Himself",
      credit_id: "52fe45b6c3a36847f80d634f",
      order: 4
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 216364,
      original_language: "en",
      original_title: "Awesometown",
      overview: "In May, 2005, The Dudes were hired to create a pilot for Fox.",
      poster_path: null,
      release_date: "2005-01-01",
      title: "Awesometown",
      video: false,
      vote_average: 7.3,
      vote_count: 11,
      popularity: 2.091,
      character: "George Washington",
      credit_id: "55c26ca9c3a36842ec000643",
      order: 4
    },
    {
      id: 14887,
      video: false,
      vote_count: 24,
      vote_average: 5.1,
      title: "Bongwater",
      release_date: "1998-04-18",
      original_language: "en",
      original_title: "Bongwater",
      genre_ids: [
        35,
        10749
      ],
      backdrop_path: null,
      adult: false,
      overview: "David is an artist and a pothead. He's fallen in love with the beautiful and sexy Serena, and things are going simply splendidly until poor David's house burns down. Serena doesn't need the bad vibes, so she splits the scene and runs off to New York with rocker and junkie Tommy. Lonely David finally turns to the sweet, sweet comfort of marijuana and his strange menagerie of friends to forget about his lost home and love",
      poster_path: "/3gW8VZHOvvS1KnyH3u0svl7yCU8.jpg",
      popularity: 1.877,
      character: "Devlin",
      credit_id: "52fe46259251416c7506ea2f",
      order: 4
    },
    {
      adult: false,
      backdrop_path: "/quc6Smoaha9ZrFlu9gtSlJP66iF.jpg",
      genre_ids: [
        35
      ],
      id: 9536,
      original_language: "en",
      original_title: "Bio-Dome",
      overview: "Bud and Doyle are two losers who are doing nothing with their lives. Both of their girlfriends are actively involved in saving the environment, but the two friends couldn't care less about saving the Earth. One day, when a group of scientists begin a mission to live inside a \"Bio-Dome\" for a year without outside contact, Bud and Doyle mistakenly become part of the project themselves.",
      poster_path: "/pjSkGZAvksrbO6tGain8PJ3rfC7.jpg",
      release_date: "1996-01-12",
      title: "Bio-Dome",
      video: false,
      vote_average: 4.6,
      vote_count: 263,
      popularity: 12.971,
      character: "Tenacious D",
      credit_id: "52fe4506c3a36847f80b7d33",
      order: 17
    },
    {
      adult: false,
      backdrop_path: "/8xNSEa0gFana45wOT1WhmyWzcBx.jpg",
      genre_ids: [
        18
      ],
      id: 687,
      original_language: "en",
      original_title: "Dead Man Walking",
      overview: "A justice drama based on a true story about a man on death row who, in his last days, forms a strong relationship with a nun who teaches him forgiveness and gives him spirituality, as she accompanies him to his execution.",
      poster_path: "/wQmmJi5ypfHH2boXrQBmsep7qb2.jpg",
      release_date: "1995-12-29",
      title: "Dead Man Walking",
      video: false,
      vote_average: 7.4,
      vote_count: 966,
      popularity: 11.546,
      character: "Craig Poncelet",
      credit_id: "52fe426ac3a36847f801cf4f",
      order: 10
    },
    {
      backdrop_path: "/hJleEkILmKzJsvzrU8o5d66jbPa.jpg",
      genre_ids: [
        12,
        28,
        878
      ],
      original_language: "en",
      original_title: "Waterworld",
      poster_path: "/f4Q6BKm1lv9u5xoffbIIwrOYf6z.jpg",
      video: false,
      title: "Waterworld",
      vote_count: 2708,
      overview: "In a futuristic world where the polar ice caps have melted and made Earth a liquid planet, a beautiful barmaid rescues a mutant seafarer from a floating island prison. They escape, along with her young charge, Enola, and sail off aboard his ship.",
      release_date: "1995-07-28",
      vote_average: 6.1,
      id: 9804,
      adult: false,
      popularity: 22.794,
      character: "Pilot",
      credit_id: "52fe4530c3a36847f80c1491",
      order: 13
    },
    {
      adult: false,
      backdrop_path: "/ugDa1khwE3yDFBQ7KXnvCnO93Ot.jpg",
      genre_ids: [
        35,
        10749
      ],
      id: 30528,
      original_language: "en",
      original_title: "Bye Bye Love",
      overview: "With varying degrees of success, recently divorced friends Dave, Vic and Donny are trying to move on with their lives. Vic feels vilified by his ex-wife's parents, while Donny has a shaky bond with his teen daughter, Emma. Dave, meanwhile, has an enviable problem -- he has more dates than he can handle. As they confront their post-marital challenges, the men take solace in one another's plights.",
      poster_path: "/qMAvEXDLLhIKJzCSiMUgn1ypsO4.jpg",
      release_date: "1995-03-16",
      title: "Bye Bye Love",
      video: false,
      vote_average: 6,
      vote_count: 46,
      popularity: 5.127,
      character: "DJ at Party",
      credit_id: "5a91402fc3a3682502008291",
      order: 16
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [],
      id: 402838,
      original_language: "en",
      original_title: "The Innocent",
      overview: "The owner of a drugstore is killed in a hold-up. The only witness who saw one of the murderers without his mask is Gregory, a nine year old autistic boy. Cop Barlow is sure they'll try to silence him. He tries to get him to draw a picture of the man he saw.",
      poster_path: "/yP7P64lvg4do8Kro5N9yHUkCNqn.jpg",
      release_date: "1994-09-25",
      title: "The Innocent",
      video: false,
      vote_average: 6,
      vote_count: 3,
      popularity: 1.4,
      character: "Marty Prago",
      credit_id: "5d69db5d0929f6001444e5d8",
      order: 4
    },
    {
      video: false,
      vote_average: 6.6,
      overview: "Simon Phoenix, a violent criminal cryogenically frozen in 1996, escapes during a parole hearing in 2032 in the utopia of San Angeles. Police are incapable of dealing with his violent ways and turn to his captor, who had also been cryogenically frozen after being wrongfully accused of killing 30 innocent people while apprehending Phoenix.",
      release_date: "1993-10-08",
      title: "Demolition Man",
      adult: false,
      backdrop_path: "/4pl165EUdvveHCr4J4YcHVA1MnD.jpg",
      genre_ids: [
        80,
        28,
        878
      ],
      vote_count: 2618,
      original_language: "en",
      original_title: "Demolition Man",
      poster_path: "/49GjSZn9FnSnrRFmsA4x5WAj8oB.jpg",
      id: 9739,
      popularity: 24.84,
      character: "Wasteland Scrap",
      credit_id: "561b3582c3a3683c1b000136",
      order: 36
    },
    {
      genre_ids: [
        28,
        12,
        35
      ],
      original_language: "en",
      original_title: "Airborne",
      poster_path: "/sZk2JtFyP2zbWIP140ZttqM2IkA.jpg",
      video: false,
      title: "Airborne",
      vote_count: 52,
      overview: "Mitchell Goosen is sixteen/seventeen year old kid from California who loves to surf and roller blade. Yet, his parents, who are two zoologists were given a grant to work in Australia. The only problem was: Mitchell couldn't go with them. So, he gets sent to stay with his aunt, uncle, and cousin in Cincinnati, Ohio. When he arrives, he meets his cousin who is also his new roommate for the next six months: Wiley. Mitchell then goes to school and gets on the bad side of the high school hockey players. Mitchell and Wiley end up enduring weeks of torture from the guys. Then, the big guys and Mitchell and Wiley have to learn to get along to try to beat the Central High School rivals in a competition down Devil's Backbone",
      release_date: "1993-09-17",
      vote_average: 6.2,
      id: 13064,
      adult: false,
      backdrop_path: "/kvmeihN8VNABa0Lk19mEqAwtkMC.jpg",
      popularity: 5.681,
      character: "Augie",
      credit_id: "52fe45359251416c7504f379",
      order: 12
    },
    {
      adult: false,
      backdrop_path: "/lyTh10IP4bNVYp3G7cWBac76OYb.jpg",
      genre_ids: [
        35,
        18
      ],
      id: 10608,
      original_language: "en",
      original_title: "Bob Roberts",
      overview: "Mock documentary about an upstart candidate for the U.S. Senate written and directed by actor Tim Robbins. Bob Roberts is a folksinger with a difference: He offers tunes that protest welfare chiselers, liberal whining, and the like. As the filmmakers follow his campaign, Robbins gives needle-sharp insight into the way candidates manipulate the media.",
      poster_path: "/j4PokuSIrmbN5oCR54D8kiI0U6a.jpg",
      release_date: "1992-09-04",
      title: "Bob Roberts",
      video: false,
      vote_average: 6.5,
      vote_count: 119,
      popularity: 9.42,
      character: "Roger Davis",
      credit_id: "52fe43929251416c75015a8f",
      order: 17
    }
  ],
  crew: [
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 843371,
      original_language: "en",
      original_title: "Oh Hell No",
      overview: "",
      poster_path: null,
      release_date: "2022-06-17",
      title: "Oh Hell No",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 3.22,
      credit_id: "60d99c3dcc277c0028ea026b",
      department: "Production",
      job: "Producer"
    },
    {
      genre_ids: [
        35,
        10749,
        53
      ],
      original_language: "en",
      original_title: "Happily",
      poster_path: "/uCV98jOIxwd0SMICfHFNcuo5CHS.jpg",
      title: "Happily",
      vote_average: 6,
      overview: "Tom and Janet have been happily married for years. But a visit from a mysterious stranger leads to a dead body, a lot of questions, and a tense couples' trip with friends who may not actually be friends at all.",
      release_date: "2021-03-19",
      vote_count: 43,
      video: false,
      adult: false,
      backdrop_path: "/kRtlcDGTowJGcKKX3F0u1C0vObD.jpg",
      id: 673319,
      popularity: 46.099,
      credit_id: "5e696fc3151c5c001905239b",
      department: "Production",
      job: "Producer"
    },
    {
      video: false,
      vote_average: 5.9,
      vote_count: 9,
      overview: "Jack Black is the master of ceremonies, leading Courtney Cox, Lisa Kudrow, Adam Scott, and Ben Stiller through a laugh-filled escape room adventure as they solve puzzles, find clues, and crack jokes to laugh their way through a fun maze of rooms.",
      release_date: "2020-05-21",
      adult: false,
      backdrop_path: "/f5eSHfrFAIQrDuMElmwKIdog1nG.jpg",
      genre_ids: [
        10770,
        35,
        9648
      ],
      title: "Celebrity Escape Room",
      original_language: "en",
      original_title: "Celebrity Escape Room",
      poster_path: "/z21oelicEnMXZcoztMMxyOTnGvY.jpg",
      id: 707054,
      popularity: 6.697,
      credit_id: "5ec740cc140bad001d1a22a7",
      department: "Production",
      job: "Executive Producer"
    },
    {
      adult: false,
      backdrop_path: "/gwdiLo4SDNS6MXJQCKfGlP4ttQu.jpg",
      genre_ids: [
        35,
        10402
      ],
      id: 547025,
      original_language: "en",
      original_title: "Tenacious D in Post-Apocalypto",
      overview: "Tenacious D's animated magnum opus. Our heroes JB und KG survive a nuclear fallout by hiding in a fridge and embark on an adventure in a post-apocalyptic world.",
      poster_path: "/o1tagFS2mE3rFeA0f28Gccmo5fC.jpg",
      release_date: "2018-09-28",
      title: "Tenacious D in Post-Apocalypto",
      video: false,
      vote_average: 7.1,
      vote_count: 4,
      popularity: 2.208,
      credit_id: "5b92a814925141655f0012ab",
      department: "Directing",
      job: "Director"
    },
    {
      adult: false,
      backdrop_path: "/gwdiLo4SDNS6MXJQCKfGlP4ttQu.jpg",
      genre_ids: [
        35,
        10402
      ],
      id: 547025,
      original_language: "en",
      original_title: "Tenacious D in Post-Apocalypto",
      overview: "Tenacious D's animated magnum opus. Our heroes JB und KG survive a nuclear fallout by hiding in a fridge and embark on an adventure in a post-apocalyptic world.",
      poster_path: "/o1tagFS2mE3rFeA0f28Gccmo5fC.jpg",
      release_date: "2018-09-28",
      title: "Tenacious D in Post-Apocalypto",
      video: false,
      vote_average: 7.1,
      vote_count: 4,
      popularity: 2.208,
      credit_id: "5b92a8489251416568001b1b",
      department: "Production",
      job: "Producer"
    },
    {
      adult: false,
      backdrop_path: "/gwdiLo4SDNS6MXJQCKfGlP4ttQu.jpg",
      genre_ids: [
        35,
        10402
      ],
      id: 547025,
      original_language: "en",
      original_title: "Tenacious D in Post-Apocalypto",
      overview: "Tenacious D's animated magnum opus. Our heroes JB und KG survive a nuclear fallout by hiding in a fridge and embark on an adventure in a post-apocalyptic world.",
      poster_path: "/o1tagFS2mE3rFeA0f28Gccmo5fC.jpg",
      release_date: "2018-09-28",
      title: "Tenacious D in Post-Apocalypto",
      video: false,
      vote_average: 7.1,
      vote_count: 4,
      popularity: 2.208,
      credit_id: "5b92a871925141656c001d84",
      department: "Production",
      job: "Executive Producer"
    },
    {
      adult: false,
      backdrop_path: "/gwdiLo4SDNS6MXJQCKfGlP4ttQu.jpg",
      genre_ids: [
        35,
        10402
      ],
      id: 547025,
      original_language: "en",
      original_title: "Tenacious D in Post-Apocalypto",
      overview: "Tenacious D's animated magnum opus. Our heroes JB und KG survive a nuclear fallout by hiding in a fridge and embark on an adventure in a post-apocalyptic world.",
      poster_path: "/o1tagFS2mE3rFeA0f28Gccmo5fC.jpg",
      release_date: "2018-09-28",
      title: "Tenacious D in Post-Apocalypto",
      video: false,
      vote_average: 7.1,
      vote_count: 4,
      popularity: 2.208,
      credit_id: "5b92a8250e0a2679ab0019ce",
      department: "Writing",
      job: "Writer"
    },
    {
      overview: "With his 20th reunion looming, Dan can’t shake his high school insecurities. In a misguided mission to prove he’s changed, Dan rekindles a friendship with the popular guy from his class and is left scrambling to protect more than just his reputation when a wild night takes an unexpected turn.",
      release_date: "2015-05-08",
      adult: false,
      backdrop_path: "/qKR3GvLXbWbLCz98TKEPzJU1xv7.jpg",
      title: "The D Train",
      genre_ids: [
        18,
        35
      ],
      vote_count: 170,
      original_language: "en",
      original_title: "The D Train",
      id: 308638,
      poster_path: "/p3X4ea0xirJRiHGRtk47fSrmH9I.jpg",
      video: false,
      vote_average: 4.7,
      popularity: 8.798,
      credit_id: "54de7c3bc3a368537a001b7a",
      department: "Production",
      job: "Producer"
    },
    {
      poster_path: "/tazLyiF4q2w07jXjg4z4CQZgvY.jpg",
      video: false,
      vote_average: 5.2,
      id: 38745,
      overview: "Travel writer Lemuel Gulliver takes an assignment in Bermuda, but ends up on the island of Liliput, where he towers over its tiny citizens.",
      release_date: "2010-12-25",
      adult: false,
      backdrop_path: "/1EnX8qIJUYv6mG0Asub6Om8158z.jpg",
      vote_count: 1829,
      genre_ids: [
        10751,
        35,
        12,
        14
      ],
      title: "Gulliver's Travels",
      original_language: "en",
      original_title: "Gulliver's Travels",
      popularity: 34.458,
      credit_id: "5a32d5a392514103241a0821",
      department: "Production",
      job: "Executive Producer"
    },
    {
      adult: false,
      backdrop_path: "/iSACjRcXDE3AjrS6CW6rbsH9VLI.jpg",
      genre_ids: [
        35,
        18,
        10749
      ],
      id: 13574,
      original_language: "en",
      original_title: "Year of the Dog",
      overview: "A secretary's life changes in unexpected ways after her dog dies.",
      poster_path: "/6K9ZWVMOpgQeYlq8mYGnreOViPW.jpg",
      release_date: "2007-04-13",
      title: "Year of the Dog",
      video: false,
      vote_average: 5.4,
      vote_count: 49,
      popularity: 4.819,
      credit_id: "58d06ce7c3a368674f00126b",
      department: "Production",
      job: "Producer"
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        99
      ],
      id: 397646,
      original_language: "en",
      original_title: "The Making of 'The Pick of Destiny'",
      overview: "A behind the scenes look at the making of The Pick of Destiny.",
      poster_path: null,
      release_date: "2007-02-27",
      title: "The Making of 'The Pick of Destiny'",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 1.462,
      credit_id: "5738b20692514166ac00083b",
      department: "Production",
      job: "Producer"
    },
    {
      vote_average: 6.7,
      overview: "In Venice Beach, naive Midwesterner JB bonds with local slacker KG and they form the rock band Tenacious D. Setting out to become the world's greatest band is no easy feat, so they set out to steal what could be the answer to their prayers... a magical guitar pick housed in a rock-and-roll museum some 300 miles away.",
      release_date: "2006-11-22",
      adult: false,
      backdrop_path: "/frWnQLBGB6ROzdK2aPCCLRnz24J.jpg",
      id: 2179,
      genre_ids: [
        35,
        10402
      ],
      vote_count: 1165,
      original_language: "en",
      original_title: "Tenacious D in The Pick of Destiny",
      poster_path: "/8baHzEIfrBvBy5b9XKe87Skl1tf.jpg",
      title: "Tenacious D in The Pick of Destiny",
      video: false,
      popularity: 14.081,
      credit_id: "52fe433ec3a36847f8045473",
      department: "Production",
      job: "Producer"
    },
    {
      vote_average: 6.7,
      overview: "In Venice Beach, naive Midwesterner JB bonds with local slacker KG and they form the rock band Tenacious D. Setting out to become the world's greatest band is no easy feat, so they set out to steal what could be the answer to their prayers... a magical guitar pick housed in a rock-and-roll museum some 300 miles away.",
      release_date: "2006-11-22",
      adult: false,
      backdrop_path: "/frWnQLBGB6ROzdK2aPCCLRnz24J.jpg",
      id: 2179,
      genre_ids: [
        35,
        10402
      ],
      vote_count: 1165,
      original_language: "en",
      original_title: "Tenacious D in The Pick of Destiny",
      poster_path: "/8baHzEIfrBvBy5b9XKe87Skl1tf.jpg",
      title: "Tenacious D in The Pick of Destiny",
      video: false,
      popularity: 14.081,
      credit_id: "5f489d3b2da84600343f1646",
      department: "Sound",
      job: "Music"
    },
    {
      vote_average: 6.7,
      overview: "In Venice Beach, naive Midwesterner JB bonds with local slacker KG and they form the rock band Tenacious D. Setting out to become the world's greatest band is no easy feat, so they set out to steal what could be the answer to their prayers... a magical guitar pick housed in a rock-and-roll museum some 300 miles away.",
      release_date: "2006-11-22",
      adult: false,
      backdrop_path: "/frWnQLBGB6ROzdK2aPCCLRnz24J.jpg",
      id: 2179,
      genre_ids: [
        35,
        10402
      ],
      vote_count: 1165,
      original_language: "en",
      original_title: "Tenacious D in The Pick of Destiny",
      poster_path: "/8baHzEIfrBvBy5b9XKe87Skl1tf.jpg",
      title: "Tenacious D in The Pick of Destiny",
      video: false,
      popularity: 14.081,
      credit_id: "5d45ced62d1e40349abfc446",
      department: "Writing",
      job: "Writer"
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35,
        10402
      ],
      id: 821480,
      original_language: "en",
      original_title: "Tenacious D: Time Fixers",
      overview: "After an open mic night, JB and KG get a message from Michael Keaton who tells them that the time-space continuum has been disrupted at the Ford's Theater in 1850 and only Tenacious D can fix it.",
      poster_path: "/mTqQxXRPUkq4ZOPPldQtrmzfFfH.jpg",
      release_date: "2006-11-14",
      title: "Tenacious D: Time Fixers",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 1.212,
      credit_id: "6080ceb20f0da500587d2771",
      department: "Sound",
      job: "Music"
    },
    {
      video: false,
      vote_average: 6.2,
      overview: "Nacho Libre is loosely based on the story of Fray Tormenta (\"Friar Storm\"), aka Rev. Sergio Gutierrez Benitez, a real-life Mexican Catholic priest who had a 23-year career as a masked luchador. He competed in order to support the orphanage he directed.",
      release_date: "2006-06-16",
      id: 9353,
      adult: false,
      backdrop_path: "/nNCleqiADKVMKWDC5G7hjYcVCmv.jpg",
      genre_ids: [
        35,
        10751
      ],
      vote_count: 1106,
      original_language: "en",
      original_title: "Nacho Libre",
      poster_path: "/kh7B91bMl2lZ0mH9WhPfaNUIEQH.jpg",
      title: "Nacho Libre",
      popularity: 47.975,
      credit_id: "52fe44ecc3a36847f80b2041",
      department: "Production",
      job: "Producer"
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        99,
        35
      ],
      id: 109012,
      original_language: "en",
      original_title: "60 Spins Around the Sun",
      overview: "Helmed by \"Saturday Night Live\" alumna Laura Kightlinger, this hourlong exposé chronicles -- warts and all -- the life of comedian turned activist Randy Credico, an up-and-coming funnyman whose candor tanked his career. But the end of his showbiz days didn't stop him: He switched gears and became a mouthpiece for various causes, including the fight against New York's draconian drug laws. Credico's peers and ex-girlfriends weigh in with insights.",
      poster_path: "/tJBrQokjBvHYHhcrWEf9IasRrrh.jpg",
      release_date: "2003-01-01",
      title: "60 Spins Around the Sun",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 1.127,
      credit_id: "5fe79b22c9dbf9003e4716c3",
      department: "Production",
      job: "Executive Producer"
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 821464,
      original_language: "en",
      original_title: "Tenacious D: Rock Star Sperm for Sale",
      overview: "JB and KG decide the best way to make money is to sell their sperm, so they go into the restroom and do their business into a huge glass. When they have enough, they begin to sell it in the street. They meet some pretty weird people.",
      poster_path: "/aQ4h3ZPMossWVbmzarhKWkpGLUT.jpg",
      release_date: "2002-10-20",
      title: "Tenacious D: Rock Star Sperm for Sale",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 0.6,
      credit_id: "6080bf1143d9b1003fe52675",
      department: "Writing",
      job: "Writer"
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 821430,
      original_language: "en",
      original_title: "Tenacious D: Butt Baby",
      overview: "JB and KG head out to the desert to expand their minds with LSD.",
      poster_path: "/4pwiJCKqe1hAQgGB9RxWjYnzJAl.jpg",
      release_date: "2002-04-05",
      title: "Tenacious D: Butt Baby",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 0.6,
      credit_id: "6080acfeaa659e002aeea842",
      department: "Writing",
      job: "Writer"
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        35
      ],
      id: 821379,
      original_language: "en",
      original_title: "Tenacious D: JB's BJ",
      overview: "JB goes out on the streets looking to prostitute himself. A Tenacious D short created for their American 2001 tour.",
      poster_path: "/r24DpnxQZfk6dHNJoT21Ck6IMaW.jpg",
      release_date: "2001-09-20",
      title: "Tenacious D: JB's BJ",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 0.6,
      credit_id: "60808c24160e73002998c0ca",
      department: "Writing",
      job: "Writer"
    },
    {
      adult: false,
      backdrop_path: null,
      genre_ids: [
        18
      ],
      id: 669356,
      original_language: "en",
      original_title: "You Can't Win",
      overview: "A drama set in the 1920s hobo underworld and centered on the unusual friendship between an adventurer and a young prostitute alongside Black's life on the road, in prison and his various criminal capers in the American and Canadian west from the late 1880s to early 20th century.",
      poster_path: "/g8iEQsImmyH1Nur2SBWPbO4npsr.jpg",
      release_date: "",
      title: "You Can't Win",
      video: false,
      vote_average: 0,
      vote_count: 0,
      popularity: 0.6,
      credit_id: "60f810fc39a1a600617f76ff",
      department: "Writing",
      job: "Novel"
    }
  ],
  id: 70851
};
const testPersonProfileImage = `https://image.tmdb.org/t/p/w500/${testPerson.profile_path}`;
const testPersonCastImage = `https://image.tmdb.org/t/p/w200/${testCredits.cast[0].poster_path}`;
const testPersonCrewImage = `https://image.tmdb.org/t/p/w200/${testCredits.crew[1].poster_path}`;

class MockDetailService {
  getPersonDetails = (id: string) => {
    expect(id).toEqual(testParam);
    return new Observable<Object>(observer => {
      observer.next(testPerson);
      observer.complete();
    })
  }

  getPersonCredits = (id: string) => {
    expect(id).toEqual(testParam);
    return new Observable<Object>(observer => {
      observer.next(testCredits);
      observer.complete();
    })
  }
}

class MockAuthService extends AuthService {
  isLoggedIn = false;
}

describe('PersonDetailsComponent', () => {
  let component: PersonDetailsComponent;
  let fixture: ComponentFixture<PersonDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonDetailsComponent ],
      providers: [
        {
          provide: AuthService,
          useClass: MockAuthService
        },
        {
          provide: DetailsService,
          useClass: MockDetailService
        },
        {
          provide: ActivatedRoute,
          useValue: { params: of({  id: "10" }) }
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should get person details and credits from service", () => {
    expect(component.person).toEqual(testPerson);
    expect(component.credits).toEqual(testCredits);
  })

  it("should have images", () => {
    const poster = fixture.debugElement.query(By.css("#person-profile-image"));
    expect(poster.nativeElement.src).toEqual(testPersonProfileImage);

    const castMoviesImages = fixture.debugElement.queryAll(By.css(".credits-cast-image"));
    expect(castMoviesImages[0].nativeElement.src).toEqual(testPersonCastImage);

    const crewMovieImages = fixture.debugElement.queryAll(By.css(".credits-crew-image"));
    expect(crewMovieImages[1].nativeElement.src).toEqual(testPersonCrewImage);
  })

  it("should sort movies to pull the latest 5", () => {
    const castMovies = component.getLatestMovies(testCredits.cast);
    expect(castMovies.length).toBe(5);
    expect(castMovies[0].id).toEqual(746391);

    const crewMovies = component.getLatestMovies(testCredits.cast);
    expect(crewMovies.length).toBe(5);
    expect(crewMovies[1].id).toEqual(713500);
  });
});
