import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddRemoveButtonComponent} from "./add-remove-button/add-remove-button.component";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [
    AddRemoveButtonComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule
  ],
  exports: [
    AddRemoveButtonComponent
  ]
})
export class ShareModule { }
