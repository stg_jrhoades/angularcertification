import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AuthService} from "../../auth/auth.service";
import {MovieCollectionService} from "../../movie-collection.service";

@Component({
  selector: 'app-add-remove-button',
  templateUrl: './add-remove-button.component.html',
  styleUrls: ['./add-remove-button.component.css']
})
export class AddRemoveButtonComponent implements OnInit {
  constructor(public authService: AuthService, private movieCollectionService: MovieCollectionService) { }

  // #Inputs - these allow you to provide inputs on component when called
  @Input() type: "add" | "remove" = "add";
  @Input() dataType: "movie" | "person" = "movie";
  @Input() value: string | any;

  // #Output - this decorator lets you emit stuff so the calling component can react to the events.
  @Output() afterProcess: EventEmitter<any> = new EventEmitter<any>();

  add = () => {
    switch (this.dataType) {
      case "movie":
        this.movieCollectionService.addMovie(this.value);
        this.afterProcess.emit();
        break;
      case "person":
        this.movieCollectionService.addPerson(this.value);
        this.afterProcess.emit();
        break;
    }
  }

  remove = () => {
    switch (this.dataType) {
      case "movie":
        this.movieCollectionService.removeMovie(this.value);
        this.afterProcess.emit();
        break;
      case "person":
        this.movieCollectionService.removePerson(this.value);
        this.afterProcess.emit();
        break;
    }
  }

  ngOnInit(): void { }
}
