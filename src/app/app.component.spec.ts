import {ComponentFixture, TestBed} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {AuthService} from "./auth/auth.service";
import {LoginComponent} from "./auth/login/login.component";
import {Router} from "@angular/router";

class MockAuth {
  logout = () => {};
}

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([])
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: AuthService, useClass: MockAuth }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  })

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'movie-app'`, () => {
    expect(component.title).toEqual('movie-app');
  });

  it("should logout", () => {
    const router = TestBed.inject(Router);
    const spy = spyOn(router, "navigateByUrl").and.stub();
    const auth = TestBed.inject(AuthService);
    const authSpy = spyOn(auth, "logout").and.stub();

    component.logout();
    expect(authSpy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledOnceWith("/login");
  })
});
