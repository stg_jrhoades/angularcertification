import { TestBed } from '@angular/core/testing';

import { MovieCollectionService } from './movie-collection.service';

const testMovie = {
  "adult": false,
  "backdrop_path": "/sXfQGa7juaTpZ5IF17Xa6hI53nx.jpg",
  "belongs_to_collection": {
    "id": 19163,
    "name": "The Land Before Time Collection",
    "poster_path": "/n1bjdBVThBezxR6nEf2dy43sTtV.jpg",
    "backdrop_path": "/alkvR9vTtuZEmd5ygsayOfxYOMa.jpg"
  },
  "budget": 12500000,
  "genres": [
    {
      "id": 10751,
      "name": "Family"
    },
    {
      "id": 16,
      "name": "Animation"
    },
    {
      "id": 12,
      "name": "Adventure"
    }
  ],
  "homepage": "https://www.uphe.com/movies/the-land-before-time",
  "id": 12144,
  "imdb_id": "tt0095489",
  "original_language": "en",
  "original_title": "The Land Before Time",
  "overview": "An orphaned brontosaurus named Littlefoot sets off in search of the legendary Great Valley. A land of lush vegetation where the dinosaurs can thrive and live in peace. Along the way he meets four other young dinosaurs, each one a different species, and they encounter several obstacles as they learn to work together in order to survive.",
  "popularity": 15.57,
  "poster_path": "/wSC2wxFj3ZgrIHlIaePeuHN4igo.jpg",
  "production_companies": [
    {
      "id": 6422,
      "logo_path": null,
      "name": "Sullivan Bluth Studios",
      "origin_country": "US"
    },
    {
      "id": 108270,
      "logo_path": "/eDWGeg5tdVNcI8aBaaJ21bEBtFa.png",
      "name": "Lucasfilm Animation",
      "origin_country": "US"
    },
    {
      "id": 33,
      "logo_path": "/8lvHyhjr8oUKOOy2dKXoALWKdp0.png",
      "name": "Universal Pictures",
      "origin_country": "US"
    },
    {
      "id": 20448,
      "logo_path": null,
      "name": "U-Drive Productions",
      "origin_country": "US"
    },
    {
      "id": 56,
      "logo_path": "/cEaxANEisCqeEoRvODv2dO1I0iI.png",
      "name": "Amblin Entertainment",
      "origin_country": "US"
    }
  ],
  "production_countries": [
    {
      "iso_3166_1": "IE",
      "name": "Ireland"
    },
    {
      "iso_3166_1": "US",
      "name": "United States of America"
    }
  ],
  "release_date": "1988-11-18",
  "revenue": 84460846,
  "runtime": 69,
  "spoken_languages": [
    {
      "english_name": "English",
      "iso_639_1": "en",
      "name": "English"
    }
  ],
  "status": "Released",
  "tagline": "A new adventure is born.",
  "title": "The Land Before Time",
  "video": false,
  "vote_average": 7.1,
  "vote_count": 1989
};
const testPerson = {
  adult: false,
  also_known_as: [
    "Thomas Jacob Black",
    "Τόμας Τζέικομπ \"Τζακ\" Μπλακ",
    "Τόμας Τζέικομπ Μπλακ",
    "Τζακ Μπλακ",
    "잭 블랙",
    "Джек Блек"
  ],
  biography: "Thomas Jacob \"Jack\" Black (born August 28, 1969) is an American actor, comedian, singer, songwriter, and YouTuber. He makes up one half of the comedy and satirical rock duo Tenacious D. The group has two albums as well as a television series and a film. His acting career is extensive, starring primarily as bumbling, cocky, but internally self-conscious outsiders in comedy films. He was a member of the Frat Pack, a group of comedians who have appeared together in several Hollywood films, and has been nominated for a Golden Globe award. He has also won an MTV Movie Award, and a Nickelodeon Kids Choice Award.\n\n​From Wikipedia, the free encyclopedia",
  birthday: "1969-08-28",
  deathday: null,
  gender: 2,
  homepage: null,
  id: 70851,
  imdb_id: "nm0085312",
  known_for_department: "Acting",
  name: "Jack Black",
  place_of_birth: "Santa Monica, California, USA",
  popularity: 19.952,
  profile_path: "/rtCx0fiYxJVhzXXdwZE2XRTfIKE.jpg"
};

describe('MovieCollectionService', () => {
  let service: MovieCollectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MovieCollectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it("should be empty arrays", () => {
    service.getPeople().subscribe(people => {
      expect(people).toEqual([])
    });
    service.getMovies().subscribe(movies => {
      expect(movies).toEqual([])
    });
  });
  it("should be able to add movie", () => {
    service.addMovie(testMovie);
    service.getMovies().subscribe(movies => {
      expect(movies.length).toEqual(1);
      expect(movies[0]).toEqual(testMovie);
    })
  });
  it("should be able to remove movie", () => {
    service.addMovie(testMovie);
    service.removeMovie(testMovie.id);
    service.getMovies().subscribe(movies => {
      expect(movies.length).toEqual(0);
    })
  });
  it("should be able to add person", () => {
    service.addPerson(testPerson);
    service.getPeople().subscribe(people => {
      expect(people.length).toEqual(1);
      expect(people[0]).toEqual(testPerson);
    })
  });
  it("should be able to remove person", () => {
    service.addPerson(testPerson);
    service.removePerson(testPerson.id);
    service.getPeople().subscribe(people => {
      expect(people.length).toEqual(0);
    })
  });
});
