import { TestBed } from '@angular/core/testing';

import { ApiInterceptor } from './api.interceptor';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {HTTP_INTERCEPTORS, HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

const ROOT_URL = "http://localhost:8080?api_key=";
const API_KEY = "2d792a964ef8d51c43de253fa14e1821";

@Injectable()
class MockService {
  constructor(private http: HttpClient) {}

  getPosts() {
    return this.http.get<any>(ROOT_URL);
  }
}

describe('ApiInterceptor', () => {
  let service: MockService;
  let httpMock: HttpTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientTestingModule ],
    providers: [
      MockService,
      {
        provide: HTTP_INTERCEPTORS,
        useClass: ApiInterceptor,
        multi: true
      }
      ]
  }));

  beforeEach(() => {
    service = TestBed.inject(MockService);
    httpMock = TestBed.inject(HttpTestingController);
  })

  it("should apply API key to the end of the URL providedf", () => {
    service.getPosts().subscribe(resp => {
      expect(resp).toBeTruthy();
    });

    const httpRequest = httpMock.expectOne(ROOT_URL+API_KEY);
    expect(httpRequest.request.url).toEqual(ROOT_URL+API_KEY);
  })
});
