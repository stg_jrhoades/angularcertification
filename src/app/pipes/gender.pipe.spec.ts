import { GenderPipe } from './gender.pipe';

describe('GenderPipe', () => {
  let pipe: GenderPipe;

  beforeEach(() => {
    pipe = new GenderPipe();
  })

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it("should return male", () => {
    expect(pipe.transform(2)).toEqual("Male");
  })

  it("should return female", () => {
    expect(pipe.transform(1)).toEqual("Female");
  })

  it("should return anything else", () => {
    expect(pipe.transform(22)).toEqual("-");
    expect(pipe.transform(0)).toEqual("-");
    expect(pipe.transform(5)).toEqual("-");
    expect(pipe.transform(100)).toEqual("-");
  })
});
