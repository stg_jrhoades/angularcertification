import { DatePipe } from './date.pipe';

describe('DatePipe', () => {
  let pipe: DatePipe;

  beforeEach(() => {
    pipe = new DatePipe();
  })

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it("should transform date provided into MM/0D/YYYY", () => {
      const date = pipe.transform("1996-11-07")
      expect(date).toEqual("11/07/1996");
  })

  it("should transform date provided into 0M/DD/YYYY", () => {
    const date = pipe.transform("1996-01-07")
    expect(date).toEqual("01/07/1996");
  })
});
