import { Pipe, PipeTransform } from '@angular/core';

// #Pipes - are useful for changing data into formats you need/want it in, in a nice functional way that makes it
// consistent upon use
@Pipe({
  name: 'date'
})
export class DatePipe implements PipeTransform {
  transform(value: string): string {
    const date = new Date(value);
    if (!isNaN(date.getTime())) {
      const month = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
      const day = date.getDate() + 1 < 10 ? `0${date.getDate() + 1}` : date.getDate() + 1;

      return month + '/' + day + '/' + date.getFullYear();
    }

    return "Bad date provided"
  }
}
